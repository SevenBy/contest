(function() {
	'use strict';

	angular.module('app.questions', ['ngFileUpload', 'ngDragDrop', 'app.service'])
		.controller('QuestionCtrl', ['$scope', '$http', '$filter', '$sce', '$location', '$uibModal', '$rootScope', 'appConfig', 'logger', '$window', QuestionCtrl])
		.controller('QuestionAddCtrl', ['$scope', '$http', '$timeout', '$location', 'appConfig', 'Upload', 'logger', QuestionAddCtrl])
		.controller('QuestionEditCtrl', ['$scope', '$http', '$timeout', '$location', 'appConfig', 'Upload', 'logger', QuestionEditCtrl])
		.controller('QuestionPublishCtrl', ['$scope', '$filter', '$timeout', '$location', '$rootScope', 'tool', 'link', 'appConfig', 'logger', QuestionPublishCtrl])
		.controller('QuestionTopoCtrl', ['$scope', '$http', '$uibModal', 'appConfig', '$location', 'logger', '$window', '$interval', QuestionTopoCtrl])
		.controller('AddNetworkCtrl', ['network', '$scope', '$http', '$uibModalInstance', 'appConfig', 'logger', '$window', AddNetworkCtrl])
		.controller('EditNetworkCtrl', ['network', '$scope', '$http', '$uibModalInstance', 'appConfig', 'logger', '$window', EditNetworkCtrl])
		.controller('DeleteNetworkCtrl', ['network', '$scope', '$http', '$window', '$uibModalInstance', 'appConfig', 'logger', DeleteNetworkCtrl])
		.controller('AddServerCtrl', ['server', 'datas', '$scope', '$http', '$uibModalInstance', 'appConfig', 'logger', '$window', '$filter', AddServerCtrl])
		.controller('EditServerCtrl', ['server', 'datas', '$scope', '$http', '$uibModalInstance', 'appConfig', 'logger', '$window', '$filter', EditServerCtrl])
		.controller('AddRouterCtrl', ['router', 'datas', '$scope', '$http', '$uibModalInstance', '$window', 'logger', 'appConfig', AddRouterCtrl])
		.controller('EditRouterCtrl', ['router', 'datas', '$scope', '$http', '$uibModalInstance', '$window', 'logger', 'appConfig', EditRouterCtrl])
		.controller('QuestionUserCtrl', ['datas', '$scope', '$http', '$uibModalInstance', '$window', '$filter', 'logger', 'appConfig', QuestionUserCtrl])
		.controller('LogDetailCtrl', ['$scope', '$sce', 'datas', '$uibModalInstance', 'appConfig', LogDetailCtrl])
		.controller('CommitFlagCtrl', ['$scope', '$http', 'datas', '$uibModalInstance', 'appConfig', 'logger', CommitFlagCtrl])
		.controller('ChooseImageCtrl', ['$scope', '$http', 'datas', '$uibModalInstance', 'appConfig', 'logger', '$window', ChooseImageCtrl])
		.controller('VNCCtrl', ['$scope', '$http', '$uibModal', 'appConfig', '$location', 'logger', '$window', '$interval', '$sce', VNCCtrl])
		.controller('StopAllQuestionsCtrl', ['$scope', '$http', '$uibModalInstance', 'appConfig', 'logger', StopAllQuestionsCtrl])
		.filter('fmtTime', fmtTime)
		.filter('fmtUuid', fmtUuid)
		.directive("slideFollow", slideFollow).directive('mySelect', mySelect);
	
	function QuestionCtrl($scope, $http, $filter, $sce, $location, $uibModal, $rootScope, appConfig, logger, $window) {
		layer.closeAll();
		$scope.addQuestion = function() {
			$location.path("/questions/add");
		}
		$scope.editQuestion = function(que) {
			sessionStorage.editQue = JSON.stringify(que);
			$location.path("/questions/edit");
		}
		$scope.labelList = [];
		$scope.categoryList = [];
		
		var formatCateroty = function(labellist){
			var list = [];
			list.push({name: '全部', val:undefined});
			for (var i in labellist){
				list.push({name: labellist[i], val:labellist[i]});
			}
			return list;
		}
		
		$http.get(appConfig.main.server + "/services/questions/category/list").success(function(data) {
			$scope.labelList = data.question_categories;
			$scope.categoryList = formatCateroty($scope.labelList);
		});
		$scope.difficultList = [{
			name: '全部',
			val: undefined
		}, {
			name: '低',
			val: 1
		}, {
			name: '中',
			val: 2
		}, {
			name: '高',
			val: 3
		}];

		$scope.ischoose = function(lb) {
			return $scope.label == lb.val ? 'label-primary' : 'label-default';
		}
		$scope.ischoose2 = function(df) {
			return $scope.difficult == df.val ? 'label-primary' : 'label-default';
		};
		$scope.ischoose3 = function() {
			return $scope.oneline == 'overflow-x: auto;' ? 'label-primary' : 'label-default';
		}
		$scope.more = function () {
			$scope.oneline = $scope.oneline == 'white-space:nowrap; overflow:hidden; text-overflow:ellipsis;' ? 'overflow-x: auto;' : 'white-space:nowrap; overflow:hidden; text-overflow:ellipsis;'
		}
		$scope.filterLabel = function(lb) {
			if($scope.labeledit) {
				if (lb.val == undefined) 
					return;
				// 删除操作
				$http.post(appConfig.main.server + '/services/questions/category/delete', {
					label: lb.name
				}).success(function(data) {
					if (data.exception) {
						logger.logError(data.exception);
					} else {
						$http.get(appConfig.main.server + "/services/questions/category/list").success(function(data) {
							$scope.labelList = data.question_categories;
							$scope.categoryList = formatCateroty($scope.labelList);
						});
					}
				})
			} else {
				if($scope.label == lb.val) {
					$scope.label = undefined;
				} else {
					$scope.label = lb.val;
				}
			
				$rootScope.$broadcast('preloader:active');
				$http.post(appConfig.main.server + "/services/questions/list2all", {
					category: lb.val == undefined ? "" :  lb.val
				}).success(function(data) {
					data.questions.reverse();
					$rootScope.$broadcast('preloader:hide');
					$scope.stores = data.questions;
					$scope.search();
				}).error(function(err) {
					$rootScope.$broadcast('preloader:hide');
				});;
			}
		}

		$scope.filterDifficult = function(df) {
			if($scope.difficult == df.val) {
				$scope.difficult = undefined;
			} else {
				$scope.difficult = df.val;
			}
			return $scope.search();
		}

		$scope.labels = function(labs) {
			var ht = [];
			for(var i in labs) {
				ht.push('<span class="label label-primary">');
				ht.push(labs[i]);
				ht.push('</span> ');
			}
			return $sce.trustAsHtml(ht.join(''));
		}

		$scope.diffs = function(d) {
			var ht = [];
			for(var i in $scope.difficultList) {
				if(i == 0) continue;
				if($scope.difficultList[i].val == d) {
					ht.push('<span class="label label-primary">');
					ht.push($scope.difficultList[i].name);
					ht.push('</span> ');
				}
			}
			return $sce.trustAsHtml(ht.join(''));
		}
		
		$scope.select = function(page) {
			var end, start;
			start = (page - 1) * $scope.numPerPage;
			end = start + $scope.numPerPage;
			$scope.currentPage = window.localStorage['currPage'] = page;
			return $scope.currentPageStores = $scope.filteredStores.slice(start, end);
		};

		$scope.onFilterChange = function() {
			$scope.select($scope.currentPage);
			return $scope.row = '';
		};

		$scope.onNumPerPageChange = function() {
			window.localStorage['numPerPage'] = $scope.numPerPage;
			$scope.select(1);
			return $scope.currentPage = 1;
		};

		$scope.onOrderChange = function() {
			$scope.select(1);
			return $scope.currentPage = 1;
		};

		$scope.search = function() {
			window.localStorage['question_questionNameKey'] = $scope.searchKeywords;
			$scope.filteredStores = $filter('filter')($scope.stores, {
				"summary": $scope.searchKeywords,
				'categories': $scope.label,
				'difficult': $scope.difficult
			});
			return $scope.onFilterChange();
		};

		$scope.order = function(rowName) {
			if($scope.row === rowName) {
				return;
			}
			$scope.row = rowName;
			$scope.filteredStores = $filter('orderBy')($scope.stores, rowName);
			return $scope.onOrderChange();
		};
		
		$scope.stopAllQuestions = function(ques) {
			var modalInstance = $uibModal.open({
				animation: true, 
				templateUrl: 'strictconfirm.html',
				controller: 'StopAllQuestionsCtrl'
			});
		}
		
		$scope.delQuestion = function(ques) {
			var modalInstance = $uibModal.open({
				animation: true, //$scope.animationsEnabled,
				templateUrl: 'confirm.html',
				controller: 'ConfirmCtrl'
			});
			modalInstance.result.then(function() {
				$http.post(appConfig.main.server + "/services/questions/delete", {
					id: ques.id
				}).success(function(data) {
					if (data.exception) {
						logger.logError(data.exception);
					} else {
						logger.logSuccess("删除成功");
						var idx = -1;
						for(var i = 0, len = $scope.stores.length; i < len; i++) {
							if($scope.stores[i].id == ques.id) {
								idx = i;
								break;
							}
						}
						$scope.stores.splice(idx, 1);
						$scope.search();
					}
				}).error(function(err) {
					logger.logError("删除失败");
				})
			})
		}
		$scope.downloadAnswer = function(ques) {
			if(ques.answerfile) {
				$window.open(ques.answerfile);
				logger.logSuccess("答案下载成功");
			} else {
				logger.logError("答案文件未上传");
			}
		}
		$scope.labeledit = false;
		$scope.modlabel = function() {
			$scope.labeledit = !$scope.labeledit;
		}
		$scope.canedit = (window.localStorage['uname'] == appConfig.main.adminuser);

		$scope.publishQues = function(ques) {
			sessionStorage.question = JSON.stringify(ques);
			$location.path("/questions/publish");
		}

		$scope.questionTopo = function(ques) {
			sessionStorage.questionSummary = ques.summary;
			sessionStorage.questionId = ques.id;
			$location.path("/questions/topo");
		}

		$scope.users = function(ques) {
			var modalInstance = $uibModal.open({
				animation: true,
				templateUrl: 'questionUser.html',
				controller: 'QuestionUserCtrl',
				resolve: {
					datas: function() {
						return {
							ques: ques
						}
					}
				}
			});
		}

		var init = function() {
			$scope.oneline = 'white-space:nowrap; overflow:hidden; text-overflow:ellipsis;';
			$scope.searchKeywords = window.localStorage['question_questionNameKey'] == undefined ? '' : window.localStorage['question_questionNameKey'];
			$scope.filteredStores = [];
			$scope.row = '';
			$scope.numPerPageOpt = [3, 5, 10, 20];
			var numPerPage = window.localStorage['numPerPage'];
        	$scope.numPerPage = numPerPage == undefined ? $scope.numPerPageOpt[2] : parseInt(numPerPage);
			$scope.currentPage = 1;
			
			$scope.currentPageStores = [];
			$rootScope.$broadcast('preloader:active');
			$http.get(appConfig.main.server + '/services/questions/listall').success(function(data) {
				var currentPage = window.localStorage['currPage'];
                if(currentPage != undefined) $scope.currentPage = currentPage;
                data.questions.reverse();
				$rootScope.$broadcast('preloader:hide');
				$scope.stores = data.questions;
				$scope.search();
			}).error(function(err) {
				$rootScope.$broadcast('preloader:hide');
			});
		};
		init();
	}

	function QuestionAddCtrl($scope, $http, $timeout, $location, appConfig, Upload, logger) {
		var init = function() {
			$scope.question = {};
			$http.get(appConfig.main.server + '/services/questions/category/list').success(function(data) {
				$scope.categoryList = data.question_categories;
				$("#categories").select2({
					minimumResultsForSearch: Infinity,
					placeholder: "请选择标签",
					tags: true,
					tokenSeparators: [',', ' ']
				})
			})
			$scope.myFile = "";
			$scope.date = new Date();
			$scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
			$scope.format = $scope.formats[1];
			$scope.altInputFormats = ['M!/d!/yyyy'];
			$scope.isOpenDate = false;
			$scope.uname = window.localStorage['uname'];
			$scope.dateOptions = {
				formatYear: 'yy',
				startingDay: 1
			};

		};
		init();
		$scope.back = function() {
			$location.path("/questions");
		}
		var checkForm = function() {
			$scope.question.categories = [];
			for(var i in $("#categories").val()) {
				if($("#categories").val()[i]) {
					$scope.question.categories.push($("#categories").val()[i].replace("string:", ""));
				}
			}
			if(!$scope.question.summary) {
				logger.logError("题目名称不能为空");
				return false;
			} else if(!$scope.question.introduction) {
				logger.logError("内容简介不能为空");
				return false;
			} else if(!$scope.question.guide) {
				logger.logError("作业指导书不能为空");
				return false;
			} else if(!$scope.question.answerfile) {
				logger.logError("请选择参考答案");
				return false;
			} else if(!$scope.question.timeout || $scope.question.timeout <= 0) {
				logger.logError("超时时间必须为大于零的有效数");
				return false;
			} 
			return true;
		}

		$scope.save = function() {
			if(checkForm()) {
				$scope.question.timeout = $scope.question.timeout;
				$http.post(appConfig.main.server + '/services/questions/save', $scope.question)
					.success(function(data) {
						if(data.question) {
							logger.logSuccess("提交成功");
							$timeout(function() {
								$scope.back();
							}, 2000);
						}
					})
			}
		}
		$scope.calFileSize = function(size) {
			var sizeLabel = ["B", "KB", "MB", "GB"];
			for(var index = 0; index < sizeLabel.length; index++) {
				if(size < 1024) {
					return round(size, 2) + sizeLabel[index];
				}
				size = size / 1024;
			}
			return round(size, 2) + sizeLabel[index];
		}
		var round = function(number, count) {
			return Math.round(number * Math.pow(10, count)) / Math.pow(10, count);
		};
		var getObjectURL = function(file) {
			var url = null;
			if(window.createObjectURL != undefined) { // basic
				url = window.createObjectURL(file);
			} else if(window.URL != undefined) { // mozilla(firefox)
				url = window.URL.createObjectURL(file);
			} else if(window.webkitURL != undefined) { // webkit or chrome
				url = window.webkitURL.createObjectURL(file);
			}
			return url;
		}
		$scope.uploadFile = function(file) {
			//return;
			if(!!file.name) {
				$scope.fileName = file.name;
			} else {
				logger.logError("文件错误");
				return;
			}
			var objUrl = getObjectURL(file);
			Upload.upload({
				url: appConfig.main.server + '/services/fileupload',
				data: {
					upfile: file,
					uploadFileName: file.name,
					uploadContentType: file.type
				},
			}).then(function(response) {
				$timeout(function() {
					$scope.question.answerfile = response.data.url;
					$scope.question.originalName = response.data.originalName;
					logger.logSuccess("文件上传成功");
					//$scope.progress = 0;
				}, 500);
				$scope.filename = response.data.originalName;
			}, function(response) {
				if(response.status > 0) {
					$scope.errorMsg = response.status + ': ' + response.data;
				}
			}, function(evt) {
				//$scope.progress = parseInt(100.0 * evt.loaded / evt.total);
			});
		};
	}

	function QuestionEditCtrl($scope, $http, $timeout, $location, appConfig, Upload, logger) {
		var init = function() {
			$scope.difficultList = [{
				value: 1,
				text: '低'
			}, {
				value: 2,
				text: '中'
			}, {
				value: 3,
				text: '高'
			}];
			$http.get(appConfig.main.server + '/services/questions/category/list').success(function(data) {
				$scope.categoryList = data.question_categories;
				$("#categories").select2({
					minimumResultsForSearch: Infinity,
					placeholder: "请选择标签",
					tags: true,
					tokenSeparators: [',', ' ']
				})
				$scope.question = JSON.parse(sessionStorage.editQue);
				$http.get(appConfig.main.server + '/services/questions/' + $scope.question.id).success(function(data) {
					$scope.question = data.question;
					$scope.question.timeout2 = $scope.question.timeout / 3600;
					var filename = '';
					if (data.question.answerfile)
						filename = data.question.answerfile.split("/");
					$scope.filename = filename[filename.length - 1];
					$scope.question.answerfile = '';
				});
			});
		}
		init();
		$scope.back = function() {
			$location.path("/questions");
		}
		var checkForm = function() {
			$scope.question.categories = [];
			for(var i in $("#categories").val()) {
				if($("#categories").val()[i]) {
					$scope.question.categories.push($("#categories").val()[i].replace("string:", ""));
				}
			}
			if(!$scope.question.summary) {
				logger.logError("题目名称不能为空");
				return false;
			} else if(!$scope.question.introduction) {
				logger.logError("内容简介不能为空");
				return false;
			} else if(!$scope.question.guide) {
				logger.logError("作业指导书不能为空");
				return false;
			} else if(!$scope.question.timeout2 || $scope.question.timeout2 <= 0) {
				logger.logError("超时时间必须为大于零的有效数");
				return false;
			} 
			return true;
		}

		$scope.save = function() {
			if(checkForm()) {
				$scope.question.timeout = $scope.question.timeout2;
				$http.post(appConfig.main.server + '/services/questions/update', $scope.question)
					.success(function(data) {
						if(data.question) {
							logger.logSuccess("修改成功");
							$timeout(function() {
								$scope.back();
							}, 2000);
						}
					})
			}
		}

		$scope.uploadFile = function(file) {
			if(!!file.name) {
				$scope.fileName = file.name;
			} else {
				logger.logError("文件错误");
				return;
			}
			layer.load(1, {
				shade: [0.2, '#fff'] //0.1透明度的白色背景
			})
			Upload.upload({
				url: appConfig.main.server + '/services/fileupload',
				data: {
					upfile: file,
					uploadFileName: file.name,
					uploadContentType: file.type
				},
			}).then(function(response) {
				layer.closeAll();
				$timeout(function() {
					$scope.question.answerfile = response.data.url;
                    $scope.question.originalName = response.data.originalName;
					logger.logSuccess("文件上传成功");
					//$scope.progress = 0;
				}, 200);
				$scope.filename = response.data.originalName;
			}, function(response) {
				layer.closeAll();
				if(response.status > 0) {
					$scope.errorMsg = response.status + ': ' + response.data;
				}
			}, function(evt) {
				//$scope.progress = parseInt(100.0 * evt.loaded / evt.total);
			});
		};
	}

	function QuestionPublishCtrl($scope, $filter, $timeout, $location, $rootScope, tool, link, appConfig, logger) {
		$scope.ischoose = function(lb) {
			return $scope.role == lb.id ? 'label-primary' : 'label-default';
		};

		$scope.filterRole = function(df) {
			if($scope.role == df.id ) {
				$scope.role = undefined;
			} else {
				$scope.role = df.id;
			}
			return $scope.search();
		};

		$scope.labels = function (labs) {
			var ht = [];
			for (var i in labs) {
				ht.push('<span class="label label-primary">');
				ht.push(labs[i]);
				ht.push('</span> ');
			}
			return $sce.trustAsHtml(ht.join(''));
		};

		$scope.select = function (page) {
			var end, start;
			var numPerPage = $scope.numPerPage == '全部' ? $scope.stores.length:$scope.numPerPage;
			start = (page - 1) * numPerPage;
			end = start + numPerPage;
			$scope.currentPageStores = $scope.filteredStores.slice(start, end);
			$scope.checkIsAllChecked();
		};

		$scope.onFilterChange = function () {
			$scope.select(1);
			$scope.currentPage = 1;
			return $scope.row = '';
		};

		$scope.onNumPerPageChange = function () {
			$scope.select(1);
			return $scope.currentPage = 1;
		};

		$scope.onOrderChange = function () {
			$scope.select(1);
			return $scope.currentPage = 1;
		};

		$scope.order = function (rowName) {
			if ($scope.row === rowName) {
				return;
			}
			$scope.row = rowName;
			$scope.filteredStores = $filter('orderBy')($scope.stores, rowName);
			return $scope.onOrderChange();
		};

		$scope.search = function () {
			window.localStorage['question_userNameKey'] = $scope.searchKeywords;
			$scope.filteredStores = $filter('filter')($scope.stores, {
				'username': $scope.searchKeywords,
				'type': $scope.role
			});
			return $scope.onFilterChange();
		};

		$scope.getusers = function () {
			tool.get(link.allUser, {}, function(data) {
				if(data && data.users) {
					$scope.users = data.users;
					tool.get(link.quesFollowUsers + $scope.question.id + "/follows", {}, function(res) {
						if(res && res.follwers) {
							if(res.follwers.length > 0) {
								$scope.isAllChecked = $scope.users.length == res.follwers.length;
								angular.forEach($scope.users, function(u) {
									for(var i = 0, len = res.follwers.length; i < len; i++) {
										if(u.username == res.follwers[i]) {
											u.checked = true;
											u.hasPublish = true;
											break;
										}
									}
								})
							}
							$rootScope.$broadcast('preloader:hide');
							$scope.stores = $scope.users;
							$scope.filteredStores = $scope.stores;
							$scope.numPerPageOpt = [3, 5, 10, 20, "全部"];
							$scope.numPerPage = $scope.numPerPageOpt[4];

							return $scope.select($scope.currentPage);
						} else {
							logger.logError("获取题目发布用户失败");
						}
					})
				} else {
					logger.logError("获取用户列表失败");
				}
			});
		};

		var init = function() {
			$scope.question = JSON.parse(sessionStorage.question);
			$scope.roleList = [{
				id: undefined,
				name: '全部'
			}, {
				id: 1,
				name: '组用户'
			}, {
				id: 2,
				name: '地方用户'
			}];

			$scope.filteredStores = [];
			$scope.row = '';
			$scope.currentPage = 1;
			$scope.currentPageStores = [];

			$rootScope.$broadcast('preloader:active');
			$scope.roleName = { 0: '管理员', 1: '组用户', 2: '地方用户' };
			$scope.searchKeywords = window.localStorage['question_userNameKey'] == undefined ? '' : window.localStorage['question_userNameKey'];
			$scope.getusers();
		};
		init();

		$scope.publishQuestion = function() {
			var subUsers = [];
			angular.forEach($scope.stores, function(u) {
				console.log(u, u.checked);
				if(u.checked)
					subUsers.push(u.username);
			});
			tool.post(link.quesFollowUsers + $scope.question.id + "/follows", {
				users: subUsers
			}, function(data) {
				if(data) {
					logger.logSuccess("发布成功");
					$timeout(function() {
						$location.path("/questions");
					}, 1000);
				}
			})
		};

		$scope.checkAll = function() {
			if ($scope.currentPageStores.length == 0){
				$scope.isAllChecked = false;
			}
			angular.forEach($scope.currentPageStores, function(u) {
				u.checked = $scope.isAllChecked;
			});
		};

		$scope.checkIsAllChecked = function() {
			var checked = 0;
			angular.forEach($scope.currentPageStores, function(u) {
				if(u.checked)
					checked += 1;
			});
			$scope.isAllChecked = checked == $scope.currentPageStores.length && $scope.currentPageStores.length != 0;
		};

		$scope.back = function() {
			$location.path("/questions");
		};
	}

	function QuestionTopoCtrl($scope, $http, $uibModal, appConfig, $location, logger, $window, $interval) {
		var colors = ['#27C46B', '#EEC800', '#F63A49', '#00CEC1', '#2895C4', '#2771C5'];
		var render_mode = 1; //0表示路由器和服务器稀疏绘制，1表示稠密绘制
		$scope.canedit = false;
		$scope.canchooseuser = false;
		$scope.username = window.localStorage['uname'];
		$scope.createtime = 0;
		$scope.screenHeight = document.body.clientHeight;

		$http.get(appConfig.main.server + '/services/logtype/list').success(function(data) {
			$scope.logtypes = {};
			for (var i in data){
				$scope.logtypes[data[i].value] = data[i].name;
			}
		});

		if(window.localStorage['uname'] == appConfig.main.adminuser ||
				window.localStorage['role'] == 'admin') {
			$scope.canchooseuser = true;
		} else if(window.localStorage['role'] == 'group') {
			$scope.canchooseuser = true;
		}
		$($scope.toastr).remove();
		$scope.$on('$destroy', function() {
			$interval.cancel($scope.timer);
			$($scope.toastr).remove();
		});
		
		$http.get(appConfig.main.server + "/services/questions/" + sessionStorage.questionId).success(function(data) {
			if(data.question)
				$scope.questionSummary = sessionStorage.questionSummary = data.question.summary;
			else
				$scope.questionSummary = sessionStorage.questionSummary;
		}).error(function(data, status, response) {
			$scope.questionSummary = '';
		});

		// 管理员可以编辑
		$scope.instances = {};
		$http.get(appConfig.main.server + "/services/questions/server/" + sessionStorage.questionId + "/alluser").success(function(data) {
			if(data.servers.length == 0) {
				// 没有服务器启动才能编辑
				$scope.canedit = (window.localStorage['uname'] == appConfig.main.adminuser);
			} else {
				for(var i in data.servers) {
					var ins = data.servers[i];
					$scope.instances[ins.template_id + '_' + ins.username] = ins;
				}
			}
		});
		
		var randomColor = function() {
			var r = Math.floor(Math.random() * colors.length);
			r = (r == colors.length ? r - 1 : r);
			return colors.splice(r, 1)[0];
		}
		var dropNetwork = function() {
			var index = $(".gray-border").data("index");
			$scope.network = {
				color: randomColor(),
				questionId: sessionStorage.questionId
			};
			var modalInstance = $uibModal.open({
				animation: true,
				templateUrl: 'network-add.html',
				controller: 'AddNetworkCtrl',
				resolve: {
					network: function() {
						return $scope.network;
					}
				}
			});

			modalInstance.result.then(function() {
				$(".gray-border").remove();

				$scope.networks.splice(index - 1, 0, $scope.network);
			}, function() {
				$(".gray-border").remove();
			});
		}

		$http.get(appConfig.main.server + '/services/images/categories').success(function(data) {
			$scope.image_categories = data.image_categories;
		});
		
		var hideEntryWindow = function() {
			if($scope.toastr){
				$scope.toastr.remove();
			};
		};
		
		var dropServer = function() {
			var index = $(".gray-border").data("index");
			var top = $(".gray-border").attr("top");
			var left = $(".gray-border").attr("left");
			$scope.server = {
				questionId: sessionStorage.questionId,
				top: top,
				left: left
			};
			var modalInstance = $uibModal.open({
				animation: true,
				templateUrl: 'server-add.html',
				size: 'lg',
				controller: 'AddServerCtrl',
				resolve: {
					server: function() {
						return $scope.server;
					},
					datas: function() {
						return {
							images: $scope.images,
							networks: $scope.networks,
							extnet: $scope.extnet,
							flavors: $scope.flavors,
							image_categories: $scope.image_categories
						}
					}
				}
			});

			modalInstance.result.then(function() {
				$(".gray-border").remove();

				$scope.servers.push($scope.server);
			}, function() {
				$(".gray-border").remove();
			});
		}

		var dropRouter = function() {
			var index = $(".gray-border").data("index");
			var top = $(".gray-border").attr("top");
			var left = $(".gray-border").attr("left");
			$scope.router = {
				questionId: sessionStorage.questionId,
				top: top,
				left: left
			};
			var modalInstance = $uibModal.open({
				animation: true,
				templateUrl: 'router-add.html',
				controller: 'AddRouterCtrl',
				resolve: {
					router: function() {
						return $scope.router;
					},
					datas: function() {
						return {
							networks: $scope.networks,
							extnet: $scope.extnet
						}
					}
				}
			});

			modalInstance.result.then(function() {
				$(".gray-border").remove();

				$scope.routers.push($scope.router);
			}, function() {
				$(".gray-border").remove();
			});
		}

		$scope.userchange = function() {
			layer.load(1, {
				shade: [0.2, '#fff'] //0.1透明度的白色背景
			})
			for(var j in $scope.servers) {
				$scope.servers[j].power = "";
				$scope.servers[j].instance = undefined;
			}
			$scope.routerRunning = '';
			hideEntryWindow();
			$http.get(appConfig.main.server + '/services/questions/servers/' + $scope.username + '/' + sessionStorage.questionId).success(function(data) {
				var light = 0;
				var createtime = 0;
				for(var i in data.servers) {
					for(var j in $scope.servers) {
						if(data.servers[i].template_id == $scope.servers[j].id) {
							$scope.servers[j].power = data.servers[i].server.server.power_state.toLowerCase();
							$scope.servers[j].instance = data.servers[i].server.server;
							light += ($scope.servers[j].power == 'running' ? 1 : 0);
						}
					}
					if(createtime == 0) { 
						createtime = parseInt(data.servers[i].created_ts);
					} else if(data.servers[i].created_ts && data.servers[i].created_ts < createtime) {
						createtime = parseInt(data.servers[i].created_ts);
					}
				}
				$scope.createtime = createtime;
				renderSider();
				if(light == $scope.servers.length && light > 0) {
					$scope.routerRunning = 'running';
					// 题目启动了
					getEntry(logger);
				} else {
					$scope.routerRunning = '';
				}
				layer.closeAll();
			});
		}

		function getEntry(logger) {
			$http.get(appConfig.main.server + "/services/questions/" + sessionStorage.questionId + "/entries?username=" + $scope.username).success(function(data) {
				var entryaddress = [];
				for(var i in data.entries) {
					entryaddress.push(data.entries[i].ips.join(','));
					for(var j in $scope.servers) {
						if(data.entries[i].server_id == $scope.servers[j].id) {
							$scope.servers[j].entry = true;
						}
					}
				}
				hideEntryWindow();
				$scope.toastr = logger.toast('入口地址：\n' + entryaddress.join('\n'), '', {
					timeOut: 0
				});
			});
		}

		$scope.networks = [];
		$scope.servers = [];
		$scope.routers = [];
		$http.get(appConfig.main.server + '/services/questions/networks/' + sessionStorage.questionId).success(function(data) {
			for(var i in data.networks) {
				var ntwk = data.networks[i];
				ntwk.color = randomColor();
				ntwk.idx = i;
				if(ntwk.id == -1) {
					$scope.extnet = ntwk;
				} else {
					$scope.networks.push(ntwk);
				}
			}
			renderServer();
			renderRouter();
		});

		var left = 1;

		var getTop = function(nics) {
			var index = 999999999;
			for(var i in nics) {
				var net_id = nics[i].net_id;
				if(net_id == -1) {
					return 1;
				}
				for(var j in $scope.networks) {
					var net = $scope.networks[j];
					if(net.id == net_id && j < index) {
						index = j;
					}
				}
			}
			if (index == 999999999) {
				return 1;
			}
			return parseInt(index) + 2;
		}
		
		var lefts = {}
		var getLeft = function(top, nics) {
			if (lefts[top] == undefined) {
				lefts[top] = 1;
			}
			var r = lefts[top]++;
			return r;
		}
		
		var getMaxLeft = function(top) {
			if (lefts[top] == undefined) {
				return 1;
			}
			return lefts[top];
		}

		var renderServer = function() {
			$http.get(appConfig.main.server + '/services/questions/templates/' + sessionStorage.questionId).success(function(data) {
				for(var i in data.templates) {
					var temp = data.templates[i];
					var top = getTop(temp.nics);
					temp.top = top;
					temp.left = render_mode == 0 ? left++ : getLeft(top, temp.nics);
					$scope.servers.push(temp);
				}

				if(window.localStorage['role'] == 'admin') {
					$http.get(appConfig.main.server + '/services/questions/' + sessionStorage.questionId + '/follows').success(function(data) {
						data.follwers.splice(0, 0, appConfig.main.adminuser);
						if(sessionStorage.username && sessionStorage.username !== "undefined") {
							$scope.username = sessionStorage.username
						} else {
							$scope.username = appConfig.main.adminuser;
						}
						sessionStorage.username = undefined;
						$scope.follows = data.follwers;
						$scope.userchange();
					});
				} else if(window.localStorage['role'] == 'group'){
					$http.get(appConfig.main.server + '/services/accounts/' + $scope.username + '/groupusers').success(function(data) {
						var followers = [];
						for (var i in data.groupusers)
							followers.push(data.groupusers[i].username);
						followers.splice(0, 0, $scope.username);
						/*if(sessionStorage.username && sessionStorage.username !== "undefined") {
							$scope.username = sessionStorage.username
						}
						sessionStorage.username = undefined;
						*/$scope.follows = followers;
						$scope.userchange();
					});
					
				} else {
					$scope.userchange();
				} 
			});
		}

		var renderRouter = function() {
			$http.get(appConfig.main.server + '/services/questions/routers/' + sessionStorage.questionId).success(function(data) {
				for(var i in data.routers) {
					var temp = data.routers[i];
					var top = getTop(temp.ports);
					temp.top = top;
					temp.left = render_mode == 0 ? left++ : getLeft(top, temp.nics);
					$scope.routers.push(temp);
				}
			});
		}

		var renderSider = function() {
			$http.get(appConfig.main.server + '/services/questions/' + sessionStorage.questionId).success(function(data) {
				$scope.questionSider = data.question;
				if($scope.createtime) {
					if (data.question.is_contest){
						$scope.remainTime = data.question.timeout;
					} else {
						$scope.remainTime = data.question.timeout - (
							($scope.serverTime? $scope.serverTime :
								($scope.questionSider.time_now? $scope.questionSider.time_now:
									parseInt(new Date().getTime() / 1000))) - $scope.createtime);
					}
					$scope.cancelInterval();
					$scope.timer = $interval(function() {
						$scope.remainTime -= 1;
						if($scope.remainTime < 0) {
							$scope.remainTime = 0;
							$scope.cancelInterval();
							$scope.shutdownQuestion();
							//$window.location.reload();
							if (data.question.is_contest  || window.localStorage['contest']) {
								layer.open({
				                    type: 1,
									title: false,
									shade: 0.8,
									anim: 1,
									content: '<div style="padding:10px 50px 10px 50px;font-size: xx-large">比赛结束</div>',
									btn: '确定',
									btnAlign: 'c',
									closeBtn: false
				                });
				                $location.url('page/signout', {
				                    reload: true
				                });
							}
							$(".modal-dialog").hide();
						}
					}, 1000);
				}
			});
		}

		$scope.cancelInterval = function() {
			if($scope.timer) {
				$interval.cancel($scope.timer);
				$scope.timer = null;
			}
		}

		$http.get(appConfig.main.server + '/services/images/listall').success(function(data) {
			$scope.images = data.images;
		});

		$http.get(appConfig.main.server + '/services/questions/flavors').success(function(data) {
			$scope.flavors = data.flavors;
		});

		$http.get(appConfig.main.server + '/services/questions/comments/' + sessionStorage.questionId).success(function(data) {
			$scope.comments = data;
		});

		$scope.lineStyle = function(linetoid, lineidx, servertop) {
			var styles = [];
			if(linetoid == -1) {
				styles.push("background: #FE7F0C;");
				styles.push("bottom: 67px;top:" + ((1 - servertop) * 148 - 35) + "px;");
				styles.push("left: " + (lineidx * (lineidx % 2 == 1 ? -10 : 10) + 45) + "px;");
				return styles.join("");
			}
			for(var i in $scope.networks) {
				var nt = $scope.networks[i];
				if(nt.id == linetoid) {
					styles.push("background: " + nt.color + ";");
					if(parseInt(nt.idx) + 1 < servertop) {
						// up
						styles.push("bottom: 67px;top:" + (((parseInt(nt.idx) + 2) - servertop) * 148 - 35) + "px;");
						styles.push("left: " + (lineidx * (lineidx % 2 == 1 ? -10 : 10) + 45) + "px;");
					} else {
						// down
						styles.push("top: 67px;bottom:" + ((servertop - (parseInt(nt.idx) + 1)) * 148 - 35) + "px;");
						styles.push("left: " + (lineidx * (lineidx % 2 == 1 ? -10 : 10) + 45) + "px;");
					}
				}
			}
			return styles.join("");
		}

		$scope.overCallback = function(e, ui) {
			switch(ui.draggable.data("type")) {
				case 'network':
					$('<div class="network-bar gray-border"></div>').insertAfter("#canvas .network-bar:eq(0)");
					break;
				case 'server':
					$('<div class="server-block gray-border" style="top: 62px;left: 50px;"></div>').insertAfter("#canvas .bottom");
					break;
				case 'router':
					$('<div class="server-block gray-border" style="top: 62px;left: 50px;"></div>').insertAfter("#canvas .bottom2");
					break;
			}
		}
		$scope.dropCallback = function(e, ui) {
			switch(ui.draggable.data("type")) {
				case 'network':
					dropNetwork();
					break;
				case 'server':
					dropServer();
					break;
				case 'router':
					dropRouter()
					break;
			}

		}
		$scope.outCallback = function() {
			$(".gray-border").remove();
		}

		$scope.onNetworkDrag = function(event, ui) {
			var top = ui.position.top;
			var index = parseInt((top - 188) / 148) + 1;
			if(index < 1) index = 1;
			if(index > $scope.networks.length) {
				$(".gray-border").data("index", index).insertBefore("#canvas .bottom");
			} else {
				$(".gray-border").data("index", index).insertBefore("#canvas .network-bar:not(.gray-border):eq(" + index + ")");
			}
		}

		$scope.onServerDrag = function(event, ui) {
			var top = ui.position.top;
			var index = parseInt((top - 62) / 148) + 1;
			if(index < 1) index = 1;
			var left = render_mode == 0 ? $(".server-block").length : getMaxLeft(index);
			$(".gray-border").data("index", index).attr("top", index).attr("left", left).css({
				top: (index - 1) * 148 + 62,
				left: (left - 1) * 120 + 50
			});
		}

		$scope.editNetwork = function(ntwk) {
			var modalInstance = $uibModal.open({
				animation: true, //$scope.animationsEnabled,
				templateUrl: 'network-edit.html',
				controller: 'EditNetworkCtrl',
				resolve: {
					network: function() {
						return ntwk;
					}
				}
			});
		}

		$scope.deleteNetwork = function(ntwk) {
			var modalInstance = $uibModal.open({
				animation: true, //$scope.animationsEnabled,
				templateUrl: 'confirm.html',
				controller: 'DeleteNetworkCtrl',
				resolve: {
					network: function() {
						return ntwk;
					}
				}
			});
		}

		$scope.editServer = function(server) {
			if($scope.canedit) {
				var modalInstance = $uibModal.open({
					animation: true,
					templateUrl: 'server-edit.html',
					size: 'lg',
					controller: 'EditServerCtrl',
					resolve: {
						server: function() {
							return server;
						},
						datas: function() {
							return {
								images: $scope.images,
								networks: $scope.networks,
								extnet: $scope.extnet,
								flavors: $scope.flavors,
								image_categories: $scope.image_categories
							}
						}
					}
				});
			}
		}

		$scope.editRouter = function(router) {
			if($scope.canedit) {
				var modalInstance = $uibModal.open({
					animation: true,
					templateUrl: 'router-edit.html',
					size: 'lg',
					controller: 'EditRouterCtrl',
					resolve: {
						router: function() {
							return router;
						},
						datas: function() {
							return {
								networks: $scope.networks,
								extnet: $scope.extnet,
								flavors: $scope.flavors
							}
						}
					}
				});
			}
		}

		$scope.startQuestion = function() {
			if(!$scope.username) {
				logger.logError("请选择拓扑用户");
				return;
			}
			layer.load(1, {
				shade: [0.2, '#fff'] //0.1透明度的白色背景
			})
			$http.post(appConfig.main.server + "/services/questions/start", {
				question_id: sessionStorage.questionId,
				template_id: -1,
				username: $scope.username
			}).success(function(data) {
				if(data['exception']) {
					layer.closeAll();
					logger.logError(data['exception']);
				} else {
					if($scope.createtime == 0)
						$scope.createtime = parseInt(new Date().getTime() / 1000);
					$scope.needServer = 0;
					for(var j in $scope.servers) {
						var instances = data.servers;
						for(var k in instances) {
							if(instances[k].template_id == $scope.servers[j].id) {
								$scope.needServer++;
								$scope.servers[j].instanceid = instances[k].id;
								logger.logSuccess("虚拟实例" + instances[k].id + "启动中");
								monitorInstance($scope.servers[j]);
							}
						}
					}
					$scope.routerRunning = '';
					logger.logSuccess("正在启动题目");
				}
			}).error(function(err) {
				layer.closeAll();
				logger.logError("启动失败");
			});

			function monitorInstance(server) {
				setTimeout(function() {
					$http.get(appConfig.main.server + '/services/questions/server/' + server.instanceid).success(function(data) {
						if(data['exception']) {
							layer.closeAll();
							logger.logError(data['exception']);
						} else {
							//计算题目的启动时间，以最先启动的虚拟实例为准
							if($scope.createtime == 0) {
								$scope.createtime = parseInt(data.server.created_ts);
							} else if(data.server.created_ts && data.server.created_ts < $scope.createtime) {
								$scope.createtime = parseInt(data.server.created_ts);
							}
							
							if(data.server.power_state == 'Running') {
								logger.logSuccess("虚拟实例" + data.server.id + "启动完成");
								server.instance = data.server;
								server.power = 'running';
								$scope.needServer--;
								$scope.remainTime = NaN;
								if($scope.needServer <= 0) {
									logger.logSuccess("题目启动完成");
									$scope.routerRunning = 'running';
									$scope.canedit = false;
									if (data.server.time_now){
										$scope.serverTime = data.server.time_now;
									}
									layer.closeAll();
									/*
									$scope.cancelInterval();
									$scope.remainTime = $scope.questionSider.timeout
									$scope.timer = $interval(function() {
										$scope.remainTime -= 1;
										if($scope.remainTime < 0) {
											$scope.remainTime = 0;
											$scope.cancelInterval();
											$scope.shutdownQuestion();
										}
									}, 1000);
									*/
									renderSider();
									setTimeout(function() {
										getEntry(logger);
									}, 2000);
								}
							} else {
								monitorInstance(server);
							}
						}
					});
				}, 2500);
			}
		}

		$scope.shutdownQuestion = function() {
			if(!$scope.username) {
				logger.logError("请选择拓扑用户");
				return;
			}
			layer.load(1, {
				shade: [0.2, '#fff'] //0.1透明度的白色背景
			})
			$http.post(appConfig.main.server + "/services/questions/stopall", {
				question: sessionStorage.questionId,
				username: $scope.username
			}).success(function(data) {
				if(data['exception']) {
					layer.closeAll();
					logger.logError(data['exception']);
				} else {
					$scope.createtime = 0;
					$scope.cancelInterval();
					hideEntryWindow();
					layer.closeAll();
					for(var j in $scope.servers) {
						$scope.servers[j].power = '';
						$scope.servers[j].instance = undefined;
						$scope.servers[j].instanceid = '';
					}
					$scope.routerRunning = '';
					logger.logSuccess("关闭成功");
				}
			}).error(function(err) {
				layer.closeAll();
				logger.logError("关闭失败");
			});
		}

		$scope.answerQuestion = function() {
			layer.load(1, {
				shade: [0.3, '#fff'] //0.1透明度的白色背景
			});
			sessionStorage.username = $scope.username;
			$http.get(appConfig.main.server + "/services/questions/attackvm/" + sessionStorage.questionId + "/" + sessionStorage.username).success(function(data) {
				if(data.server && data.server.id) {
					gotovnc(data.server.id, $http, $window, logger, appConfig, $location);
				} else {
					var modalInstance = $uibModal.open({
						animation: true,
						templateUrl: 'chooseImage.html',
						controller: 'ChooseImageCtrl',
						resolve: {
							datas: {}
						}
					});
				}
			});
		}

		$scope.back = function() {
			$window.history.back();
		}

		$scope.icon = function(c) {
			return !c.image ? 'unknow' : c.image.categories.indexOf('windows') != -1 ? 'windows' : 'linux';
		}

		$scope.poptemplate = 'popoverTemplate.html';

		$scope.createvm  = function(server) {
			if(!$scope.username) {
				logger.logError("请选择拓扑用户");
				return;
			}
			layer.load(1, {
				shade: [0.2, '#fff'] //0.1透明度的白色背景
			})
			$http.post(appConfig.main.server + "/services/questions/start", {
				question_id: sessionStorage.questionId,
				template_id: server.id,
				username: $scope.username
			}).success(function(data) {
				if(data['exception']) {
					layer.closeAll();
					logger.logError(data['exception']);
				} else {
					if($scope.createtime == 0)
						$scope.createtime = parseInt(new Date().getTime() / 1000);
					logger.logSuccess("虚拟实例 " + data.server.id + " 启动中");
					for(var j in $scope.servers) {
						if (data.server.template_id == $scope.servers[j].id) {
							$scope.needServer++;
							$scope.servers[j].instanceid = data.server.id;
							monitorInstance($scope.servers[j]);
							break;
						}
					}
				}
			}).error(function(err) {
				layer.closeAll();
				logger.logError("虚拟实例" + server.id + "启动失败");
			});

			function monitorInstance(server) {
				setTimeout(function() {
					$http.get(appConfig.main.server + '/services/questions/server/' + server.instanceid).success(function(data) {
						if(data.exception) {
							layer.closeAll();
							logger.logError(data.exception);
						} else {
							//计算题目的启动时间，以最先启动的虚拟实例为准
							if($scope.createtime == 0) {
								$scope.createtime = parseInt(data.server.created_ts);
							} else if(data.server.created_ts && data.server.created_ts < $scope.createtime) {
								$scope.createtime = parseInt(data.server.created_ts);
							}

							if(data.server.power_state == 'Running') {
								logger.logSuccess("虚拟实例" + data.server.id + "启动完成");
								server.instance = data.server;
								server.power = 'running';
								$scope.needServer--;
                                $scope.canedit = false;
								layer.closeAll();
								$scope.userchange();
							} else {
								monitorInstance(server);
							}
						}
					});
				}, 2500);
			}
		}

        $scope.startevm = function(server) {
            layer.load(1, {
                shade: [0.2, '#fff'] //0.1透明度的白色背景
            })
            $http.get(appConfig.main.server + '/services/images/vm/operate/' + server.instance.id + '?action=start').success(function(data) {
            	if(data.exception) {
                    logger.logError("开机失败: " + data.exception);
                    layer.closeAll();
                } else {
                	//监控该虚拟实例直至其已经开机完成
                	monitorInstance(server);
                }
            }).error(function(err) {
                layer.closeAll();
                logger.logError("开机失败");
            });
            
            var trycnt = 0;
            function monitorInstance(server) {
				setTimeout(function() {
					trycnt++;
					$http.get(appConfig.main.server + '/services/questions/server/' + server.instance.id).success(function(data) {
						if(data.exception) {
							layer.closeAll();
							logger.logError(data.exception);
						} else {
							if(data.server.power_state == 'Running') {
								logger.logSuccess("虚拟实例" + data.server.id + "开机完成");
								server.instance = data.server;
								server.power = 'running';
								layer.closeAll();
								$scope.userchange();
							} else if(trycnt <= 12){
								monitorInstance(server);
							} else {
								layer.closeAll();
							}
						}
					});
				}, 2500);
            }
        }

		$scope.shutdownvm = function(server) {
			layer.load(1, {
				shade: [0.2, '#fff'] //0.1透明度的白色背景
			})
			$http.get(appConfig.main.server + '/services/images/vm/operate/' + server.instance.id + '?action=stop', {'timeout': 120000}).success(function(data) {
				if(data.exception) {
					layer.closeAll();
					logger.logError("关机失败: " + data.exception);
				} else {
					//监控该虚拟实例直至其已经关机完成
					monitorInstance(server);
				}
			}).error(function(err) {
				layer.closeAll();
				logger.logError("网络请求超时");
			});
			
			var trycnt = 0;
            function monitorInstance(server) {
				setTimeout(function() {
					trycnt++;
					$http.get(appConfig.main.server + '/services/questions/server/' + server.instance.id).success(function(data) {
						if(data.exception) {
							layer.closeAll();
							logger.logError(data.exception);
						} else {
							if(data.server.power_state == 'Shutdown') {
								logger.logSuccess("虚拟实例" + data.server.id + "关机完成");
								server.instance = data.server;
								server.power = 'shutdown';
								layer.closeAll();
								$scope.userchange();
							} else if(trycnt <= 12){
								monitorInstance(server);
							} else {
								layer.closeAll();
								$scope.userchange();
							}
						}
					});
				}, 2500);
            }
		}
		
		$scope.restart = function(server) {
			layer.load(1, {
				shade: [0.2, '#fff'] //0.1透明度的白色背景
			})
			$http.get(appConfig.main.server + '/services/images/vm/operate/' + server.instance.id + '?action=reboot').success(function(data) {
				layer.closeAll();
				if(data.exception) {
					logger.logError("重启失败: " + data.exception);
				} else {
					$window.location.reload();
				}
			}).error(function(err) {
				layer.closeAll();
				logger.logError("重启失败");
			});
		}

		$scope.showvnc = function(server) {
			$http.get(appConfig.main.server + '/services/images/vm/vnc/' + server.instance.id).success(function(data) {
				$window.open(data.console.url, 'C-Sharpcorner', 'width=1024,height=800');
			});
		}

		$scope.showlog = function(server) {
			$http.get(appConfig.main.server + '/services/images/vm/log/' + server.instance.id).success(function(data) {
				var modalInstance = $uibModal.open({
					animation: true,
					templateUrl: 'logDetail.html',
					controller: 'LogDetailCtrl',
					size: 'lg',
					resolve: {
						datas: function() {
							return {
								context: data.output
							}
						}
					}
				});
			});
		}

		$scope.commitFlag = function(server) {
			var modalInstance = $uibModal.open({
				animation: true,
				templateUrl: 'commitFlag.html',
				controller: 'CommitFlagCtrl',
				resolve: {
					datas: {}
				}
			});
		}

		$scope.destroy = function(server) {
			layer.load(1, {
				shade: [0.2, '#fff'] //0.1透明度的白色背景
			})
			$http.post(appConfig.main.server + '/services/images/vm/delete/' + server.instance.id).success(function(data) {
				layer.closeAll();
				if(data.exception) {
					logger.logError("销毁失败: " + data.exception);
				} else {
					$window.location.reload();
				}
			}).error(function(err) {
				layer.closeAll();
				logger.logError("销毁失败");
			});
		}
		$scope.cmt = {};
		$scope.cmt.content = "";
		$scope.submitComment = function() {
			$http.post(appConfig.main.server + '/services/questions/comment', {
				username: window.localStorage['uname'],
				type: 0,
				qid: sessionStorage.questionId,
				content: $scope.cmt.content
			}).success(function(data) {
				$scope.cmt = {};
				$scope.cmt.content = "";
				$scope.comments.unshift(data);
			}).error(function(err) {
				layer.closeAll();
				logger.logError("评论失败");
			});
		};
		$scope.replyComment = function(cid) {
			$scope.cmt.replynow = cid;
			$scope.cmt.replycontent = "";
		};
		$scope.submitReply = function(replayname, parentid) {
			$http.post(appConfig.main.server + '/services/questions/comment', {
				username: window.localStorage['uname'],
				type: 1,
				replayname: replayname,
				parentid: parentid,
				qid: sessionStorage.questionId,
				content: $scope.cmt.replycontent
			}).success(function(data) {
				$scope.cmt = {};
				$http.get(appConfig.main.server + '/services/questions/comments/' + sessionStorage.questionId).success(function(data) {
					$scope.comments = data;
				});
			}).error(function(err) {
				layer.closeAll();
				logger.logError("评论失败");
			});
		};
		$scope.datasetData = [];
		$scope.showHistory = function() {
			$scope.showhistory = !$scope.showhistory;
		}
	}

	function AddNetworkCtrl(network, $scope, $http, $uibModalInstance, appConfig, logger, $window) {
		var checkForm = function() {
			if(!$scope.network.name) {
				logger.logError("网络名称不能为空");
				return false;
			} else if(!$scope.network.cidr) {
				logger.logError("网络地址段不能为空");
				return false;
			} 
			return true;
		}
		
		$scope.ok = function() {
			if(!checkForm()) return;
			
			var hostrouters = [];
			if($scope.network.host_routers && $scope.network.host_routers.length > 0) {
				var hr = $scope.network.host_routers.split('\n');
				for(var i in hr) {
					hostrouters.push(hr[i]);
				}
			}

			$http.post(appConfig.main.server + "/services/questions/networks", {
				name: $scope.network.name,
				cidr: $scope.network.cidr,
				gateway: $scope.network.gateway,
				host_routers: hostrouters,
				question_id: network.questionId
			}).success(function(data) {
				if(data['exception']) {
					logger.logError(data['exception']);
				} else {
					$window.location.reload();
				}
			}).error(function(err) {
				logger.logError("保存失败");
			});
		};

		$scope.cancel = function() {
			$uibModalInstance.dismiss("cancel");
		};

		$scope.popover = function(server) {
			server.instance = $scope.instances[server.id + '_' + $scope.user];
			$scope.server = server;
		}
	}

	function EditNetworkCtrl(network, $scope, $http, $uibModalInstance, appConfig, logger, $window) {
		var checkForm = function() {
			if(!$scope.network.name) {
				logger.logError("网络名称不能为空");
				return false;
			} else if(!$scope.network.cidr) {
				logger.logError("网络地址段不能为空");
				return false;
			} 
			return true;
		}
		
		$scope.network = {
			name: network.name,
			cidr: network.cidr,
			gateway: network.gateway,
			host_routers: network.host_routers,
			id: network.id
		}
		$scope.ok = function() {
			if(!checkForm()) return;
			
			var hostrouters = [];
			if($scope.network.host_routers && $scope.network.host_routers.length > 0) {
				var hr = $scope.network.host_routers.split('\n');
				for(var i in hr) {
					hostrouters.push(hr[i]);
				}
			}

			$http.post(appConfig.main.server + "/services/questions/network/" + $scope.network.id, {
				name: $scope.network.name,
				cidr: $scope.network.cidr,
				gateway: $scope.network.gateway,
				host_routers: hostrouters
			}).success(function(data) {
				network.name = $scope.network.name;
				network.cidr = $scope.network.cidr;
				network.gateway = $scope.network.gateway;
				network.host_routers = hostrouters;
				network.id = $scope.network.id;
				$uibModalInstance.close();
			}).error(function(err) {
				logger.logError("修改失败");
			});
		};

		$scope.cancel = function() {
			$uibModalInstance.dismiss("cancel");
		};
	}

	function DeleteNetworkCtrl(network, $scope, $http, $window, $uibModalInstance, appConfig, logger) {
		$scope.ok = function() {
			$http.post(appConfig.main.server + "/services/questions/network/" + network.id + '/delete').success(function(data) {
				if(data['exception']) {
					logger.logError(data['exception']);
					$uibModalInstance.close();
				} else {
					$window.location.reload();
				}
			}).error(function(err) {
				logger.logError("修改失败");
			});
		};
		$scope.cancel = function() {
			$uibModalInstance.dismiss("cancel");
		};
	}

	function AddServerCtrl(server, datas, $scope, $http, $uibModalInstance, appConfig, logger, $window, $filter) {
		$scope.server = {
			entry: "yes"
		};
		$scope.allimages = datas.images;
		$scope.images = datas.images;
		$scope.networks = datas.networks;
		$scope.extnet = datas.extnet;
		$scope.flavors = datas.flavors;
		$scope.labelList = datas.image_categories;
		
		$scope.ischoose = function(lb) {
			return $scope.label == lb ? 'label-primary' : 'label-default';
		}

		$scope.nets = [];
		$scope.netcheck = function(id) {
			return $scope.nets.join(",").indexOf(id) >= 0;
		}
		$scope.updateSelection = function($event, id) {
			var checkbox = $event.target;
			var checked = checkbox.checked;
			if(checked) {
				$scope.nets.push(id);
			} else {
				var idx = $scope.nets.indexOf(id);
				$scope.nets.splice(idx, 1);
			}
		};
		
		$scope.filterLabel = function(lb) {
			$scope.label = lb;
			$scope.images = $filter('filter')($scope.allimages, {
				"categories": lb
			});
		}

		$scope.updatecpu = function(cp) {
			$scope.cpu = cp;
		};
		$scope.updatemem = function(m) {
			$scope.mem = m;
		};
		$scope.flavorChange = function(fl) {
			if(fl) {
				$scope.cpu = fl.vcpus;
				$scope.mem = fl.ram;
				$scope.server.disk = fl.disk;
			} else {
				$scope.cpu = null;
				$scope.mem = null;
				$scope.server.disk = null;
			}
		}
		
		var checkForm = function() {
			if(!$scope.server.name) {
				logger.logError("服务器名称不能为空");
				return false;
			} else if(!$scope.server.image) {
				logger.logError("镜像不能为空");
				return false;
			} else if(!$scope.nets || $scope.nets.length == 0) {
				logger.logError("网络不能为空");
				return false;
			} else if(!$scope.cpu){
            	logger.logError("CPU数目不能为空");
				return false;
            } else if(!$scope.mem){
            	logger.logError("内存大小不能为空");
				return false;
            } else if(!$scope.server.disk){
            	logger.logError("硬盘大小不能为空");
				return false;
            }
            return true;
		};

		$scope.ok = function() {
			if (!checkForm()){
				return;
			}
			server.name = $scope.server.name;
			if($scope.server.image) {
				server.imageRef = $scope.server.image.id;
			}
			server.nics = [];
			for(var i in $scope.networks) {
				var net_id = $scope.networks[i].id;
				if (!$scope.netcheck(net_id))
					continue;
				server.nics.push({
					net_id: net_id,
					ip: $scope.networks[i].ip
				})
			}
			server.cpu = $scope.cpu;
			server.memory = $scope.mem;
			server.disk = $scope.server.disk;
			server.entry = ($scope.server.entry == "true");
			server.question_id = server.questionId;
			$http.post(appConfig.main.server + "/services/questions/templates", server).success(function(data) {
				if(data['exception']) {
					logger.logError(data['exception']);
					$uibModalInstance.close();
				} else {
					$window.location.reload();
				}
			}).error(function(err) {
				logger.logError("保存失败");
				$window.location.reload();
			});

			$uibModalInstance.close();
		};

		$scope.cancel = function() {
			$uibModalInstance.dismiss("cancel");
		};
	}

	function EditServerCtrl(server, datas, $scope, $http, $uibModalInstance, appConfig, logger, $window, $filter) {
		$scope.allimages = datas.images;
		$scope.images = datas.images;
		$scope.networks = datas.networks;
		$scope.extnet = datas.extnet;
		$scope.flavors = datas.flavors;
		$scope.server = server;
		$scope.cpu = server.cpu;
		$scope.mem = server.memory;
		$scope.disk = server.disk;
		$scope.server.entry = server.entry ? "yes" : "no";
		$scope.labelList = datas.image_categories;
		$scope.nets = [];
		for(var i in server.nics) {
			$scope.nets.push(server.nics[i].net_id);
			for(var j in $scope.networks) {
				if($scope.networks[j].id == server.nics[i].net_id) {
					$scope.networks[j].ip = server.nics[i].ip;	
				}
			}
		}
		for(var i in datas.images) {
			if(server.imageRef == datas.images[i].id) {
				$scope.server.image = datas.images[i];
			}
		}
	
		$scope.netcheck = function(id) {
			return $scope.nets.join(",").indexOf(id) >= 0;
		}
		$scope.updateSelection = function($event, id) {
			var checkbox = $event.target;
			var checked = checkbox.checked;
			if(checked) {
				$scope.nets.push(id);
			} else {
				var idx = $scope.nets.indexOf(id);
				$scope.nets.splice(idx, 1);
			}
		};
		
		$scope.ischoose = function(lb) {
			return $scope.label == lb ? 'label-primary' : 'label-default';
		}
		$scope.filterLabel = function(lb) {
			$scope.label = lb;
			$scope.images = $filter('filter')($scope.allimages, {
				"categories": lb
			});
		}

		$scope.updatecpu = function(cp) {
			$scope.cpu = cp;
		};
		$scope.updatemem = function(m) {
			$scope.mem = m;
		};
		$scope.flavorChange = function(fl) {
			if(fl) {
				$scope.cpu = fl.vcpus;
				$scope.mem = fl.ram;
				$scope.server.disk = fl.disk;
			} else {
				$scope.cpu = null;
				$scope.mem = null;
				$scope.server.disk = null;
			}
		}

		$scope.ok = function() {
			server.name = $scope.server.name;
			if($scope.server.image) {
				server.imageRef = $scope.server.image.id;
			}
			server.nics = [];
			for(var i in $scope.networks) {
				var net_id = $scope.networks[i].id;
				if (!$scope.netcheck(net_id))
					continue;
				server.nics.push({
					net_id: net_id,
					ip: $scope.networks[i].ip
				})
			}
			server.cpu = $scope.cpu;
			server.memory = $scope.mem;
			server.disk = $scope.server.disk;
			server.entry = ($scope.server.entry == "yes");
			server.question_id = server.questionId;
			$http.post(appConfig.main.server + "/services/questions/template/" + server.id, server).success(function(data) {
				if(data['exception']) {
					logger.logError(data['exception']);
					$uibModalInstance.close();
				} else {
					$window.location.reload();
				}
			}).error(function(err) {
				logger.logError("保存失败");
			});

			$uibModalInstance.close();
		};

		$scope.cancel = function() {
			$uibModalInstance.dismiss("cancel");
		};

		$scope.remove = function() {
			$http.post(appConfig.main.server + "/services/questions/template/" + server.id + "/delete").success(function(data) {
				if(data['exception']) {
					logger.logError(data['exception']);
					$uibModalInstance.close();
				} else {
					$window.location.reload();
				}
			}).error(function(err) {
				logger.logError("删除失败");
			});
		}
	}

	function AddRouterCtrl(router, datas, $scope, $http, $uibModalInstance, $window, logger, appConfig) {
		$scope.networks = datas.networks;
		$scope.extnet = datas.extnet;

		$scope.nets = [];
		$scope.netcheck = function(id) {
			return $scope.nets.indexOf(id) >= 0;
		}
		$scope.updateSelection = function($event, id) {
			var checkbox = $event.target;
			var checked = checkbox.checked;
			if(checked) {
				$scope.nets.push(id);
			} else {
				var idx = $scope.nets.indexOf(id);
				$scope.nets.splice(idx, 1);
			}
		};
		$scope.ok = function() {
			router.name = $scope.router.name;
			router.question_id = router.questionId;
			router.ports = [];
			for(var i in $scope.nets) {
				if($scope.nets[i] == -1) {
					router.ports.push({
						net_id: -1
					});
					break;
				}
			}
			for(var i in $scope.networks) {
				if($scope.networks[i].chek) {
					router.ports.push({
						net_id: $scope.networks[i].id,
						gateway: $scope.networks[i].gateway
					});
				}
			}
			$uibModalInstance.close();
			layer.load(1, {
				shade: [0.2, '#fff'] //0.1透明度的白色背景
			})
			$http.post(appConfig.main.server + "/services/questions/routers", router).success(function(data) {
				if(data['exception']) {
					logger.logError(data['exception']);
				} else {
					$window.location.reload();
				}
			}).error(function(err) {
				logger.logError("保存失败");
			});
		};

		$scope.cancel = function() {
			$uibModalInstance.dismiss("cancel");
		};
	}

	function EditRouterCtrl(router, datas, $scope, $http, $uibModalInstance, $window, logger, appConfig) {
		$scope.router = router;
		$scope.networks = datas.networks;
		$scope.extnet = datas.extnet;
		for(var i in router.ports) {
			var p = router.ports[i];
			if(p.net_id == -1) {
				$scope.extnet.chek = true;
			} else {
				for(var j in $scope.networks) {
					var n = $scope.networks[j];
					if(n.id == p.net_id) {
						n.chek = true;
						n.gateway = p.gateway;
					}
				}
			}
		}

		$scope.nets = [];
		$scope.updateSelection = function($event, id) {
			var checkbox = $event.target;
			var checked = checkbox.checked;
			if(checked) {
				$scope.nets.push(id);
			} else {
				var idx = $scope.nets.indexOf(id);
				$scope.nets.splice(idx, 1);
			}
		};
		$scope.ok = function() {
			router.ports = [];
			if($scope.extnet.chek) {
				router.ports.push({
					net_id: -1
				});
			}
			for(var i in $scope.networks) {
				if($scope.networks[i].chek) {
					router.ports.push({
						net_id: $scope.networks[i].id,
						gateway: $scope.networks[i].gateway
					});
				}
			}
			$uibModalInstance.close();
			layer.load(1, {
				shade: [0.2, '#fff'] //0.1透明度的白色背景
			})
			$http.post(appConfig.main.server + "/services/questions/router/" + router.id, router).success(function(data) {
				layer.closeAll();
				if(data['exception']) {
					logger.logError(data['exception']);
				} else {
					$window.location.reload();
				}
			}).error(function(err) {
				layer.closeAll();
				logger.logError("保存失败");
			});
		};

		$scope.cancel = function() {
			$uibModalInstance.dismiss("cancel");
		};

		$scope.remove = function() {
			$http.post(appConfig.main.server + "/services/questions/router/" + router.id + "/delete").success(function(data) {
				if(data['exception']) {
					logger.logError(data['exception']);
					$uibModalInstance.close();
				} else {
					$window.location.reload();
				}
			}).error(function(err) {
				logger.logError("删除失败");
			});
		}
	}

	function QuestionUserCtrl(datas, $scope, $http, $uibModalInstance, $window, $filter, logger, appConfig) {
		layer.load(1, {
			shade: [0.3, '#fff'] //0.1透明度的白色背景
		});
		$http.get(appConfig.main.server + "/services/questions/server/" + datas.ques.id + "/alluser").success(function(data) {
			$scope.stores = data.servers;
			$scope.userQuestion = $scope.stores;
			$scope.userList = data.users;
			$scope.userRole = window.localStorage['role'];
			layer.closeAll();
		});

		$scope.onUserChange = function() {
			if($scope.user)
				$scope.userQuestion = $filter('filter')($scope.stores, {
					"username": $scope.user
				});
			else
				$scope.userQuestion = $scope.stores;
		};
		
		$scope.rebootvm = function(vm) {
			layer.load(1, {
				shade: [0.3, '#fff'] //0.1透明度的白色背景
			})
			$http.get(appConfig.main.server + '/services/images/vm/operate/' + vm.id + '?action=reboot').success(function(data) {
				layer.closeAll();
				if(data.exception) {
					logger.logError("重启失败: " + data.exception);
				}
			}).error(function(err) {
				layer.closeAll();
				logger.logError("重启失败");
			});
		}
		
		$scope.destroyvm = function(vm) {
			layer.load(1, {
				shade: [0.3, '#fff'] //0.1透明度的白色背景
			})
			$http.post(appConfig.main.server + '/services/images/vm/delete/' + vm.id).success(function(data) {
				layer.closeAll();
				if(data.exception) {
					logger.logError("销毁失败: " + data.exception);
				} else {
					var index = $scope.stores.indexOf(vm);
					$scope.stores.splice(index, 1);
					$scope.onUserChange();
				}
			}).error(function(err) {
				layer.closeAll();
				logger.logError("销毁失败");
			});
		}
		
		$scope.destroyquestion = function(vm) {
			layer.load(1, {
				shade: [0.3, '#fff'] //0.1透明度的白色背景
			});
			$http.post(appConfig.main.server + "/services/questions/stop", {
				server: vm.id,
				serverName: vm.server.server.name,
				username: vm.username,
				question: datas.ques.id
			}).success(function(data) {
				layer.closeAll();
				if(data['exception']) {
					logger.logError("关闭题目失败：" + data['exception']);
				} else {
					var len = $scope.stores.length;
					for (var i = len-1; i >= 0; i--) {
						if ($scope.stores[i].username == vm.username)
							$scope.stores.splice(i, 1);
					}
					$scope.onUserChange();
				}
			}).error(function(err) {
				layer.closeAll();
				logger.logError("关闭题目失败");
			});
		}
		
		$scope.destroyAll = function() {
			layer.load(1, {
				shade: [0.3, '#fff'] //0.1透明度的白色背景
			});
			if($scope.user) {
				$http.post(appConfig.main.server + "/services/questions/stopall", {
					question: datas.ques.id,
					username: $scope.user
				}).success(function(data) {
					layer.closeAll();
					if(data['exception']) {
						logger.logError(data['exception']);
					} else {
						$scope.stores = $filter('filter')($scope.stores, {
							"username": '!' + $scope.user
						});
						$scope.userList.splice($scope.userList.indexOf($scope.user), 1);
						$scope.user = "";
						$scope.onUserChange();
					}
				}).error(function(err) {
					layer.closeAll();
					logger.logError("销毁失败");
				});
			} else {
				$http.post(appConfig.main.server + "/services/questions/stopall", {
					question: datas.ques.id
				}).success(function(data) {
					layer.closeAll();
					if(data['exception']) {
						logger.logError(data['exception']);
					} else {
						$scope.stores = [];
						$scope.userList = [];
						$scope.onUserChange();
					}
				}).error(function(err) {
					layer.closeAll();
					logger.logError("销毁失败");
				});
			}
		}
		$scope.cancel = function() {
			$uibModalInstance.dismiss("cancel");
		};
	}

	function LogDetailCtrl($scope, $sce, datas, $uibModalInstance, appConfig) {
		$scope.context = datas.context;
		$scope.cancel = function() {
			$uibModalInstance.dismiss("cancel");
		};
	}
	
	function StopAllQuestionsCtrl($scope, $http, $uibModalInstance, appConfig, logger) {
		$scope.cancel = function() {
			$uibModalInstance.dismiss("cancel");
		};
		$scope.ok = function() {
			$uibModalInstance.dismiss("cancel");
			if($scope.inputtext != '关闭全部题目')
				return;
			
			layer.load(1, {
				shade: [0.3, '#fff'] //0.1透明度的白色背景
			});
			$http.post(appConfig.main.server + "/services/questions/stopall",{}).success(function(data) {
				layer.closeAll();
				if(data['exception']) {
					logger.logError(data['exception']);
				} else {
					logger.logSuccess("关闭全部题目成功");
				}
			}).error(function(err) {
				layer.closeAll();
				logger.logError("关闭全部题目失败");
			});
		}
	}


	function CommitFlagCtrl($scope, $http, datas, $uibModalInstance, appConfig, logger) {
		$scope.cancel = function() {
			$uibModalInstance.dismiss("cancel");
		};
		$scope.submit = function() {
			$uibModalInstance.dismiss("cancel");
			layer.load(1, {
				shade: [0.3, '#fff'] //0.1透明度的白色背景
			});
			$http.post(appConfig.main.server + "/services/questions/answer/" + sessionStorage.questionId, {
				flag: $scope.flag
			}).success(function(data) {
				layer.closeAll();
				if(data['exception']) {
					logger.logError(data['exception']);
				} else {
					logger.logSuccess("提交旗帜成功，获得 " + data.score + ' 分');
				}
			}).error(function(err) {
				layer.closeAll();
				logger.logError("提交失败");
			});
		}
	}

	function ChooseImageCtrl($scope, $http, datas, $uibModalInstance, appConfig, logger, $window) {
		$http.get(appConfig.main.server + "/services/questions/images2/attack").success(function(data) {
			layer.closeAll();
			if(data['exception']) {
				logger.logError(data['exception']);
			} else {
				$scope.images = data.images;
			}
		}).error(function(err) {
			layer.closeAll();
			logger.logError("创建失败");
		});
		$scope.submit = function() {
			if($scope.attackimg) {
				layer.load(1, {
					shade: [0.3, '#fff'] //0.1透明度的白色背景
				});
				$uibModalInstance.dismiss("cancel");
				$http.get(appConfig.main.server + "/services/questions/answerattack/" + sessionStorage.questionId + "/" + $scope.attackimg.id + "/" + sessionStorage.username).success(function(data) {
					if(data['exception']) {
						layer.closeAll();
						logger.logError(data['exception']);
					} else {
						gotovnc(data.server.id, $http, $window, logger, appConfig)
					}
				}).error(function(err) {
					layer.closeAll();
					logger.logError("创建失败");
				});
			}
		}
		$scope.cancel = function() {
			$uibModalInstance.dismiss("cancel");
		};
	}
	function openwin(url) {
		var a = document.createElement("a");
		a.setAttribute("href", url);
		a.setAttribute("target", "_blank");
		a.setAttribute("id", "camnpr");
		document.body.appendChild(a);
		a.click();
	}

	function gotovnc(id, $http, $window, logger, appConfig, $location) {
		$http.get(appConfig.main.server + '/services/questions/server/' + id).success(function(data) {
			if(data['exception']) {
				layer.closeAll();
				logger.logError(data['exception']);
			} else {
				if(data.server.power_state == 'Running') {
					layer.closeAll();
					logger.logSuccess("创建成功");
					sessionStorage.attackServerID = id;
					var height = screen.height - 100;
					// $window.open("#/page/vnc",'_blank','fullscreen=yes,toolbar=no,titlebar=no,scrollbars=no,menubar=no,location=no,status=no,width=' + screen.width + ',height=' + height + ',left=0,top=0');
					var pWin = window.open("#/page/vnc",'_blank','fullscreen=yes,toolbar=no,titlebar=no,scrollbars=no,menubar=no,location=no,status=no,width=' + screen.width + ',height=' + height + ',left=0,top=0');
					if(pWin == null){
						openwin("#/page/vnc");
					}
				} else {
					setTimeout(function() {
						gotovnc(id, $http, $window, logger, appConfig, $location);
					}, 1000)
				}
			}
		});
	}

	function fmtTime() {
		return function(v) {
			var r = '';
			if(v > 3600 * 24) {
				r += parseInt(v / 3600 / 24) + '天';
				v = v % (3600 * 24);
			}
			r += parseInt(v / 3600) + '小时';
			v = v % 3600;
			r += parseInt(v / 60) + '分钟';
			v = v % 60;
			r += v + '秒';
			return r;
		}
	}
	
	function fmtUuid() {
		return function(ID) {
			if (ID == undefined) 
				return "";
			var len = ID.length;
			var r = "";
			for (var i = 0; i < len; i++) {
				var a = ID.charAt(i);
				r += a;
				if (i == 8 || i == 12 || i == 16 || i == 20)
		            r += '-';
			}
			return r;
		}
	}

	function VNCCtrl($scope, $http, $uibModal, appConfig, $location, logger, $window, $interval, $sce) {
		var layer_load = function() {
			layer.open({
				type: 1,
				title: '若等待时间过长，请点击刷新',
				closeBtn: 0,
				icon: -1,
				shade: [
					0.98,
					'#2b4363'
				],
				area: ['300px', '250px'],
				scrollbar: false,
				anim: 1,
				btn: '刷新',
				btnAlign: 'c',
				yes: function(index, layero){
					$window.location.reload();
					layer.close(index);
				},
				content: '<div id="loading-1">  <div id="loading-center-1">  <div id="loading-center-absolute-1"> <div style="padding-top: 30px; margin-left: -10px;">界面准备中，请稍后...</div> <div class="loading-object-1" id="loading-object_one-1"></div>  <div class="loading-object-1" id="loading-object_two-1" style="left:20px;"></div>  <div class="loading-object-1" id="loading-object_three-1" style="left:40px;"></div>  <div class="loading-object-1" id="loading-object_four-1" style="left:60px;"></div>  <div class="loading-object-1" id="loading-object_five-1" style="left:80px;"></div>  </div>  </div>  </div>',
				shadeClose: false
			});
		};

		function sleep(d){
			for(var t = Date.now();Date.now() - t <= d;);
		}

		$scope.changeConnection = function() {
			$scope.screenHeight = document.body.clientHeight;
			layer_load();
			$http.get(appConfig.main.server + '/services/images/vm/vnc/' + sessionStorage.attackServerID).success(function (data) {
				$scope.rdpurl = $sce.trustAsResourceUrl(data.console.rdpurl);
				$scope.vncurl = $sce.trustAsResourceUrl(data.console.url);
				if ($scope.connectionButton == '高速') {
					$scope.conntionURL = $sce.trustAsResourceUrl($scope.vncurl);
					sleep(2);
					$scope.conntionURL = $sce.trustAsResourceUrl($scope.rdpurl);
					$scope.connectionButton = '稳定';
				}
				else if ($scope.connectionButton == '稳定'){
					$scope.conntionURL =  $sce.trustAsResourceUrl($scope.vncurl);
					$scope.connectionButton = '高速';
				}
				setTimeout("layer.closeAll();", 2000);
			});
		};

		$scope.getConnection = function() {
			layer_load();
			$http.get(appConfig.main.server + '/services/images/vm/vnc/' + sessionStorage.attackServerID).success(function (data) {
				$scope.rdpurl = $sce.trustAsResourceUrl(data.console.rdpurl);
				$scope.vncurl = $sce.trustAsResourceUrl(data.console.url);
				if ($scope.connectionButton == '稳定') {
					$scope.conntionURL = $sce.trustAsResourceUrl($scope.rdpurl);
				}
				else if ($scope.connectionButton == '高速'){
					$scope.conntionURL =  $sce.trustAsResourceUrl($scope.vncurl);
				}
			});
			setTimeout("layer.closeAll();", 2000);
		};

		$scope.cancelInterval = function() {
			if($scope.timer) {
				$interval.cancel($scope.timer);
				$scope.timer = null;
			}
		};

		var renderSider = function() {
			$http.get(appConfig.main.server + '/services/questions/' + sessionStorage.questionId).success(function(data) {
				$scope.questionSider = data.question;
				if($scope.createtime) {
					if(data.question.is_contest) {
						$scope.remainTime = data.question.timeout;
					} else {
						if (!$scope.remainTime){
							$scope.remainTime = data.question.timeout - (parseInt(new Date().getTime() / 1000) - $scope.createtime);
						}
					}
					$scope.timer = $interval(function() {
						$scope.remainTime -= 1;
						if($scope.remainTime <= 0) {
							$scope.cancelInterval();
							layer.open({
			                    type: 1,
								title: false,
								shade: 0.8,
								anim: 1,
								content: '<div style="padding:10px 50px 10px 50px;">答题时间到，页面将在5秒后自动关闭</div>',
								btn: '确定',
								btnAlign: 'c',
								closeBtn: true
			                });
							setTimeout(function() {
								$window.close();
							}, 5000);
						}
						if($scope.rdpurl == undefined && ((data.question.timeout - $scope.remainTime) < 360) && ($scope.remainTime % 5 == 0)){
							$http.get(appConfig.main.server + '/services/images/vm/vnc/' + sessionStorage.attackServerID).success(function (data) {
								$scope.rdpurl = data.console.rdpurl;
								if ($scope.rdpurl != undefined ){
									layer.open({
										type: 1,
										title: '提示',
										closeBtn: 0,
										icon: -1,
										shade: [
											0.3,
											'#2b4363'
										],
										area: ['300px', '150px'],
										scrollbar: false,
										anim: 1,
										btn: ['确定', '取消'],
										btnAlign: 'c',
										yes: function(index){
											$scope.connectionButton = '稳定';
											$scope.conntionURL = $sce.trustAsResourceUrl($scope.rdpurl);
											layer.close(index);
										},
										content: '<div style="text-align:center; padding-top:20px">攻击机启动完毕，是否跳转到高速连接方式</div>',
										shadeClose: false
									});

								}
							});
						}
					}, 1000);
				}
			});
		};

		function getEntry(logger) {
			$http.get(appConfig.main.server + "/services/questions/" + sessionStorage.questionId + "/entries?username=" + sessionStorage.username).success(function(data) {
				var entryaddress = [];
				for(var i in data.entries) {
					entryaddress.push(data.entries[i].ips.join(', '));
					for(var j in $scope.servers) {
						if(data.entries[i].server_id == $scope.servers[j].id) {
							$scope.servers[j].entry = true;
						}
					}
				}
				$scope.enteraddress = entryaddress.join('\n');
			});
		}
		
		$scope.submitComment = function() {
			$http.post(appConfig.main.server + '/services/questions/comment', {
				username: window.localStorage['uname'],
				type: 0,
				qid: sessionStorage.questionId,
				content: $scope.cmt.content
			}).success(function(data) {
				$scope.cmt = {};
				$scope.cmt.content = '';
				$scope.comments.unshift(data);
			}).error(function(err) {
				layer.closeAll();
				logger.logError("评论失败");
			});
		};

		$scope.replyComment = function(cid) {
			$scope.cmt.replynow = cid;
			$scope.cmt.replycontent = "";
		};
		
		$scope.submitReply = function(replayname, parentid) {
			$http.post(appConfig.main.server + '/services/questions/comment', {
				username: window.localStorage['uname'],
				type: 1,
				replayname: replayname,
				parentid: parentid,
				qid: sessionStorage.questionId,
				content: $scope.cmt.replycontent
			}).success(function(data) {
				$scope.cmt = {};
				$http.get(appConfig.main.server + '/services/questions/comments/' + sessionStorage.questionId).success(function(data) {
					$scope.comments = data;
				});
			}).error(function(err) {
				layer.closeAll();
				logger.logError("评论失败");
			});
		};

		$scope.showbig = function() {
			$scope.big = !$scope.big;
			$scope.screenHeight = document.body.clientHeight;
			$scope.getConnection();
			// $scope.conntionURL =  $sce.trustAsResourceUrl($scope.conntionURL.split("&dummyVar=")[0] + "&dummyVar="+ (new Date()).getTime());
		};

		$scope.setFullScreen = function() {
			$scope.fullScreen = !$scope.fullScreen;
			if($scope.fullScreen) {
				$scope.bodyClientHeight = $scope.screenHeight;
				$scope.screenHeight = $window.screen.height;
				openFullScreen();
				$scope.big = true;
			} else {
				closeFullScreen();
				$scope.screenHeight = $scope.bodyClientHeight;
			}
			$scope.getConnection();
			// $scope.conntionURL =  $sce.trustAsResourceUrl($scope.conntionURL.split("&dummyVar=")[0] + "&dummyVar="+ (new Date()).getTime());
		};

		function openFullScreen()
		{
			var docElm = document.documentElement;
			//W3C
			if (docElm.requestFullscreen) {
				docElm.requestFullscreen();
			}
			//FireFox
			else if (docElm.mozRequestFullScreen) {
				docElm.mozRequestFullScreen();
			}
			//Chrome等
			else if (docElm.webkitRequestFullScreen) {
				docElm.webkitRequestFullScreen();
			}
			//IE11
			else if (elem.msRequestFullscreen) {
				elem.msRequestFullscreen();
			}
		}

		function closeFullScreen() {
			if (document.exitFullscreen) {
				document.exitFullscreen();
			}
			else if (document.mozCancelFullScreen) {
				document.mozCancelFullScreen();
			}
			else if (document.webkitCancelFullScreen) {
				document.webkitCancelFullScreen();
			}
			else if (document.msExitFullscreen) {
				document.msExitFullscreen();
			}
		}

		$scope.back = function() {
			$location.path("/questions/topo");
		};

		$scope.commitFlag = function(server) {
			var modalInstance = $uibModal.open({
				animation: true,
				templateUrl: 'commitFlag.html',
				controller: 'CommitFlagCtrl',
				resolve: {
					datas: {}
				}
			});
		};

		var init = function() {
			$scope.screenHeight = document.body.clientHeight;
			$scope.fullScreen = false;
			$scope.cmt = {};
			$scope.cmt.content = '';
			$scope.big = false;
			$scope.connectionButton = '稳定';
			$http.get(appConfig.main.server + '/services/images/vm/vnc/' + sessionStorage.attackServerID).success(function (data) {
				$scope.rdpurl = $sce.trustAsResourceUrl(data.console.rdpurl);
				$scope.vncurl = $sce.trustAsResourceUrl(data.console.url);
				if ($scope.rdpurl == undefined || $scope.rdpurl == 'undefined') {
					$scope.conntionURL = $sce.trustAsResourceUrl($scope.vncurl);
					$scope.connectionButton = '高速';
				}
				else {
					$scope.conntionURL = $sce.trustAsResourceUrl($scope.rdpurl);
					$scope.connectionButton = '稳定';
				}
			});
			
			$(".toast").remove();
			$http.get(appConfig.main.server + '/services/questions/comments/' + sessionStorage.questionId).success(function(data) {
				$scope.comments = data;
			});
			$http.get(appConfig.main.server + '/services/questions/servers/' + sessionStorage.username + '/' + sessionStorage.questionId).success(function(data) {
				var light = 0;
				var createtime = 0;
				for(var i in data.servers) {
					for(var j in $scope.servers) {
						if(data.servers[i].template_id == $scope.servers[j].id) {
							$scope.servers[j].power = data.servers[i].server.server.power_state.toLowerCase();
							$scope.servers[j].instance = data.servers[i].server.server;
							light += ($scope.servers[j].power == 'running' ? 1 : 0);
						}
					}

					if(createtime == 0) {
						createtime = parseInt(data.servers[i].created_ts);
					} else if(data.servers[i].created_ts && data.servers[i].created_ts < createtime) {
						createtime = parseInt(data.servers[i].created_ts);
					}
				}
				$scope.createtime = createtime;
				renderSider();
				getEntry(logger);

				/*if(light == $scope.servers.length && light > 0) {
				 $scope.routerRunning = 'running';
				 // 题目启动了
				 } else {
				 $scope.routerRunning = '';
				 }*/
			});


		};
		init();
	}

	function slideFollow($timeout, $http, appConfig) {
		return {
			restrict: 'E',
			replace: true,
			scope: {
				id: "@",
				datasetData: "="
			},
			template: '<li class="tl-item" ng-repeat="log in datasetData"><div class="tl-body"><div class="tl-entry"><div class="tl-icon btn-icon btn-icon-round bg-info"><i class="fa fa-asterisk"></i></div><div class="tl-time">{{log.logtime | date:"yyyy-MM-dd"}} {{log.logtime | date:"HH:mm:ss"}}</div><div class="tl-content"><p class="tl-tile" style="margin: 20px 0 0 0;">{{log.username}} {{logtypes[log.type]}} {{log.errorinfo}}</p></div></div></div></li>',
			link: function(scope, elem, attrs) {
				$http.get(appConfig.main.server + "/services/log/answer/" + sessionStorage.questionId).success(function(data) {
					scope.datasetData = data;
					$timeout(function() {
						var className = $("." + $(elem).parent()[0].className);
						var i = 0,
							sh, ht = 0;
						// 开启定时器
						sh = setInterval(slide, 5000);

						function slide() {
							if(i < scope.datasetData.length) {
								ht += className.children("li:eq(" + i + ")").height();
								i++;
								className.animate({
									marginTop: -ht + "px"
								}, "slow");
							} else {
								i = 0;
								ht = 0;
								className.css("margin-top", "0px");
							}
						}
						
						// 清除定时器
						className.hover(function() {
							clearInterval(sh);
						}, function() {
							clearInterval(sh);
							sh = setInterval(slide, 5000);
						})

					}, 0)
				});
			}
		}
	}

	function mySelect() {
		var link = function(scope, element, attrs) {
			element.select2({
				placeholder: attrs.placeholder
			})
		}
		return {
			restrict: 'A',
			link: link
		}
	}
})();