(function() {
	'use strict';

	angular.module('app.achievements', ['app.service'])
        .controller('AchievementsUsersCtrl', ['$scope', '$location', '$filter', '$uibModal', 'tool', 'link', 'logger', 'appConfig', AchievementsUsersCtrl])
        .controller('AchievementsUserCtrl', ['$scope', '$location', '$filter', '$uibModal', 'tool', 'link', 'logger', 'appConfig', AchievementsUserCtrl])
        .controller('AchievementsLocalUserCtrl', ['$scope', '$location', '$filter', '$uibModal', 'tool', 'link', 'logger', 'appConfig', AchievementsLocalUserCtrl])
        .controller('AchievementsQuestionsCtrl', ['$scope', '$location', '$filter', '$uibModal', 'tool', 'link', 'logger', 'appConfig', AchievementsQuestionsCtrl])
        .controller('AchievementsQuestionCtrl', ['$scope', '$location', '$filter', '$uibModal', 'tool', 'link', 'logger', 'appConfig', AchievementsQuestionCtrl]);

    function AchievementsUsersCtrl($scope, $location, $filter, $uibModal, tool, link, logger, appConfig) {
        $scope.canedit = window.localStorage['uname'] == appConfig.main.adminuser;
        if ($scope.canedit) {
            $scope.roleList = [{
                id: undefined,
                name: '全部'
            }, {
            //     id: 0,
            //     name: '管理员'
            // }, {
                id: 1,
                name: '组用户'
            }, {
                id: 2,
                name: '地方用户'
            }]
        } else {
            $scope.roleList = [{
                id: undefined,
                name: '全部'
            }, {
                id: 1,
                name: '组用户'
            }, {
                id: 2,
                name: '地方用户'
            }]
        }

        $scope.ischoose = function(lb) {
            return $scope.role == lb.id ? 'label-primary' : 'label-default';
        };

        $scope.filterRole = function(df) {
            if($scope.role == df.id ) {
                $scope.role = undefined;
            } else {
                $scope.role = df.id;
            }
            return $scope.search();
        };

        $scope.labels = function (labs) {
            var ht = [];
            for (var i in labs) {
                ht.push('<span class="label label-primary">');
                ht.push(labs[i]);
                ht.push('</span> ');
            }
            return $sce.trustAsHtml(ht.join(''));
        };

        $scope.select = function (page) {
            var end, start;
            start = (page - 1) * $scope.numPerPage;
            end = start + $scope.numPerPage;
            $scope.currentPage = window.localStorage['currPage'] = page;
            return $scope.currentPageStores = $scope.filteredStores.slice(start, end);
        };

        $scope.onFilterChange = function () {
            $scope.select($scope.currentPage);
            return $scope.row = '';
        };

        $scope.onNumPerPageChange = function () {
        	window.localStorage['numPerPage'] = $scope.numPerPage;
            $scope.select(1);
            return $scope.currentPage = 1;
        };

        $scope.onOrderChange = function () {
            $scope.select(1);
            return $scope.currentPage = 1;
        };

        $scope.search = function () {
	        window.localStorage['achievement_userNameKey'] = $scope.searchKeywords;
            $scope.filteredStores = $filter('filter')($scope.stores, {
                'username': $scope.searchKeywords,
                'type': $scope.role
            });
            return $scope.onFilterChange();
        };

        $scope.order = function (rowName) {
            if ($scope.row === rowName) {
                return;
            }
            $scope.row = rowName;
            $scope.filteredStores = $filter('orderBy')($scope.stores, rowName);
            return $scope.onOrderChange();
        };

        $scope.showUser = function (user) {
            sessionStorage.achievementsUser = user;
            $location.path("/achievements/user");
        };

        var init = function () {
            $scope.roleName = { 0: '管理员', 1: '组用户', 2: '地方用户' };
	        $scope.searchKeywords = window.localStorage['achievement_userNameKey'] == undefined ? '' : window.localStorage['achievement_userNameKey'];
            $scope.filteredStores = [];
            $scope.row = '';
            $scope.numPerPageOpt = [3, 5, 10, 20];
            var numPerPage = window.localStorage['numPerPage'];
        	$scope.numPerPage = numPerPage == undefined ? $scope.numPerPageOpt[2] : parseInt(numPerPage);
            $scope.currentPage = 1;
            $scope.currentPageStores = [];

            tool.get(link.achievements + "alluserallsituation", {}, function (data) {
                $scope.stores = [];
                var currentPage = window.localStorage['currPage'];
                if(currentPage != undefined) $scope.currentPage = currentPage;
                for (var user in data) {
                    $scope.stores.push({
                        'username': user,
                        'realname': data[user].realname,
                        'type': data[user].type,
                        'complete': data[user].questions_solved.solved_questions,
                        'questions': data[user].questions_solved.total_questions,
                        'score': data[user].questions_solved.aggregate_score
                    })
                }
                $scope.search();
            });

        };
        init();
    }
    function AchievementsQuestionsCtrl($scope, $location, $filter, $uibModal, tool, link, logger, appConfig) {
        $scope.ischoose = function(lb) {
            return $scope.role == lb.id ? 'label-primary' : 'label-default';
        };
        $scope.filterRole = function(df) {
            if($scope.role == df.id ) {
                $scope.role = undefined;
            } else {
                $scope.role = df.id;
            }
            return $scope.search();
        };

        $scope.labels = function (labs) {
            var ht = [];
            for (var i in labs) {
                ht.push('<span class="label label-primary">');
                ht.push(labs[i]);
                ht.push('</span> ');
            }
            return $sce.trustAsHtml(ht.join(''));
        };

        $scope.select = function (page) {
            var end, start;
            start = (page - 1) * $scope.numPerPage;
            end = start + $scope.numPerPage;
            $scope.currentPage = window.localStorage['currPage'] = page;
            return $scope.currentPageStores = $scope.filteredStores.slice(start, end);
        };

        $scope.onFilterChange = function () {
            $scope.select($scope.currentPage);
            return $scope.row = '';
        };

        $scope.onNumPerPageChange = function () {
        	window.localStorage['numPerPage'] = $scope.numPerPage;
			$scope.select(1);
            return $scope.currentPage = 1;
        };

        $scope.onOrderChange = function () {
            $scope.select(1);
            return $scope.currentPage = 1;
        };

        $scope.search = function () {
	        window.localStorage['achievement_questionNameKey'] = $scope.searchKeywords;
            $scope.filteredStores = $filter('filter')($scope.stores, {
                'summary': $scope.searchKeywords
            });
            return $scope.onFilterChange();
        };

        $scope.order = function (rowName) {
            if ($scope.row === rowName) {
                return;
            }
            $scope.row = rowName;
            $scope.filteredStores = $filter('orderBy')($scope.stores, rowName);
            return $scope.onOrderChange();
        };

        $scope.showQuestion = function (question) {
            sessionStorage.achievementsQuestionId = question;
            $location.path("/achievements/question");
        };

        var init = function () {
            $scope.roleName = { 0: '管理员', 1: '组用户', 2: '地方用户' };
	        $scope.searchKeywords = window.localStorage['achievement_questionNameKey'] == undefined ? '' : window.localStorage['achievement_questionNameKey'];
            $scope.filteredStores = [];
            $scope.row = '';
            $scope.numPerPageOpt = [3, 5, 10, 20];
            var numPerPage = window.localStorage['numPerPage'];
        	$scope.numPerPage = numPerPage == undefined ? $scope.numPerPageOpt[2] : parseInt(numPerPage);
            $scope.currentPage = 1;
            $scope.currentPageStores = [];

            tool.get(link.achievements + "questionsusersolved", {}, function (data) {
            	$scope.stores = [];
            	var currentPage = window.localStorage['currPage'];
                if(currentPage != undefined) $scope.currentPage = currentPage;
            	data.questions.reverse();
                for (var i in data.questions) {
                    $scope.stores.push({
                        'id': data.questions[i].id,
                        'summary': data.questions[i].summary,
                        'complete': data.questions[i].solved_users_num,
                        'users': data.questions[i].users_publish_num,
                        'score': data.questions[i].flag_score
                    })
                }
                $scope.search();
            });

        };
        init();
    }
    function AchievementsUserCtrl($scope, $location, $filter, $uibModal, tool, link, logger, appConfig) {
        $scope.ischoose = function(lb) {
            return $scope.role == lb.id ? 'label-primary' : 'label-default';
        };
        $scope.filterRole = function(df) {
            if($scope.role == df.id ) {
                $scope.role = undefined;
            } else {
                $scope.role = df.id;
            }
            return $scope.search();
        };

        $scope.labels = function (labs) {
            var ht = [];
            for (var i in labs) {
                ht.push('<span class="label label-primary">');
                ht.push(labs[i]);
                ht.push('</span> ');
            }
            return $sce.trustAsHtml(ht.join(''));
        };

        $scope.select = function (page) {
            var end, start;
            start = (page - 1) * $scope.numPerPage;
            end = start + $scope.numPerPage;
            return $scope.currentPageStores = $scope.filteredStores.slice(start, end);
        };

        $scope.onFilterChange = function () {
            $scope.select(1);
            $scope.currentPage = 1;
            return $scope.row = '';
        };

        $scope.onNumPerPageChange = function () {
            $scope.select(1);
            return $scope.currentPage = 1;
        };

        $scope.onOrderChange = function () {
            $scope.select(1);
            return $scope.currentPage = 1;
        };

        $scope.search = function () {
	        window.localStorage['achievement_questionNameKey'] = $scope.searchKeywords;
            $scope.filteredStores = $filter('filter')($scope.stores, {
                'summary': $scope.searchKeywords
            });
            return $scope.onFilterChange();
        };

        $scope.order = function (rowName) {
            if ($scope.row === rowName) {
                return;
            }
            $scope.row = rowName;
            $scope.filteredStores = $filter('orderBy')($scope.stores, rowName);
            return $scope.onOrderChange();
        };

        $scope.back = function () {
            $location.path("/achievements/users");
        };

        var init = function () {
	        $scope.searchKeywords = window.localStorage['achievement_questionNameKey'] == undefined ? '' : window.localStorage['achievement_questionNameKey'];
            $scope.filteredStores = [];
            $scope.row = '';
            $scope.numPerPageOpt = [3, 5, 10, 20];
            $scope.numPerPage = $scope.numPerPageOpt[2];
            $scope.currentPage = 1;
            $scope.currentPageStores = [];

            var user = sessionStorage.achievementsUser;
            tool.get(link.achievements + "userquestionssolved?username=" + user, {}, function (data) {
                $scope.stores = [];
                $scope.username = data.username;
                $scope.realname = data.realname;
                for (var i in data.questions_solved.questions) {
                    $scope.stores.push({
                        'summary': data.questions_solved.questions[i].summary,
                        'time': data.questions_solved.questions[i].time,
                        'score': data.questions_solved.questions[i].score
                    });
                }
                $scope.search();
                return $scope.select($scope.currentPage);
            });

        };
        init();
    }
    function AchievementsLocalUserCtrl($scope, $location, $filter, $uibModal, tool, link, logger, appConfig) {
        $scope.ischoose = function(lb) {
            return $scope.role == lb.id ? 'label-primary' : 'label-default';
        };
        $scope.filterRole = function(df) {
            if($scope.role == df.id ) {
                $scope.role = undefined;
            } else {
                $scope.role = df.id;
            }
            return $scope.search();
        };

        $scope.labels = function (labs) {
            var ht = [];
            for (var i in labs) {
                ht.push('<span class="label label-primary">');
                ht.push(labs[i]);
                ht.push('</span> ');
            }
            return $sce.trustAsHtml(ht.join(''));
        };

        $scope.select = function (page) {
            var end, start;
            start = (page - 1) * $scope.numPerPage;
            end = start + $scope.numPerPage;
            return $scope.currentPageStores = $scope.filteredStores.slice(start, end);
        };

        $scope.onFilterChange = function () {
            $scope.select(1);
            $scope.currentPage = 1;
            return $scope.row = '';
        };

        $scope.onNumPerPageChange = function () {
            $scope.select(1);
            return $scope.currentPage = 1;
        };

        $scope.onOrderChange = function () {
            $scope.select(1);
            return $scope.currentPage = 1;
        };

        $scope.search = function () {
	        window.localStorage['achievement_userNameKey'] = $scope.searchKeywords;
            $scope.filteredStores = $filter('filter')($scope.stores, {
                'summary': $scope.searchKeywords
            });
            return $scope.onFilterChange();
        };

        $scope.order = function (rowName) {
            if ($scope.row === rowName) {
                return;
            }
            $scope.row = rowName;
            $scope.filteredStores = $filter('orderBy')($scope.stores, rowName);
            return $scope.onOrderChange();
        };

        var init = function () {
	        $scope.searchKeywords = window.localStorage['achievement_userNameKey'] == undefined ? '' : window.localStorage['achievement_userNameKey'];
            $scope.filteredStores = [];
            $scope.row = '';
            $scope.numPerPageOpt = [3, 5, 10, 20];
            $scope.numPerPage = $scope.numPerPageOpt[2];
            $scope.currentPage = 1;
            $scope.currentPageStores = [];

            var user = sessionStorage.achievementsUser;
            tool.get(link.achievements + "userquestionssolved?username=" + user, {}, function (data) {
                $scope.stores = [];
                $scope.username = data.username;
                $scope.realname = data.realname;
                for (var i in data.questions_solved.questions) {
                    $scope.stores.push({
                        'summary': data.questions_solved.questions[i].summary,
                        'time': data.questions_solved.questions[i].time,
                        'score': data.questions_solved.questions[i].score
                    });
                }
                $scope.search();
                return $scope.select($scope.currentPage);
            });

        };
        init();
    }
    function AchievementsQuestionCtrl($scope, $location, $filter, $uibModal, tool, link, logger, appConfig) {
        $scope.canedit = window.localStorage['uname'] == appConfig.main.adminuser;
        if ($scope.canedit) {
            $scope.roleList = [{
                id: undefined,
                name: '全部'
            // }, {
            //     id: 0,
            //     name: '管理员'
            }, {
                id: 1,
                name: '组用户'
            }, {
                id: 2,
                name: '地方用户'
            }]
        } else {
            $scope.roleList = [{
                id: undefined,
                name: '全部'
            }, {
                id: 1,
                name: '组用户'
            }, {
                id: 2,
                name: '地方用户'
            }]
        }

        $scope.ischoose = function(lb) {
            return $scope.role == lb.id ? 'label-primary' : 'label-default';
        };
        $scope.filterRole = function(df) {
            if($scope.role == df.id ) {
                $scope.role = undefined;
            } else {
                $scope.role = df.id;
            }
            return $scope.search();
        };

        $scope.labels = function (labs) {
            var ht = [];
            for (var i in labs) {
                ht.push('<span class="label label-primary">');
                ht.push(labs[i]);
                ht.push('</span> ');
            }
            return $sce.trustAsHtml(ht.join(''));
        };

        $scope.select = function (page) {
            var end, start;
            start = (page - 1) * $scope.numPerPage;
            end = start + $scope.numPerPage;
            return $scope.currentPageStores = $scope.filteredStores.slice(start, end);
        };

        $scope.onFilterChange = function () {
            $scope.select(1);
            $scope.currentPage = 1;
            return $scope.row = '';
        };

        $scope.onNumPerPageChange = function () {
            $scope.select(1);
            return $scope.currentPage = 1;
        };

        $scope.onOrderChange = function () {
            $scope.select(1);
            return $scope.currentPage = 1;
        };

        $scope.search = function () {
	        window.localStorage['achievement_userNameKey'] = $scope.searchKeywords;
            $scope.filteredStores = $filter('filter')($scope.stores, {
                'username': $scope.searchKeywords,
                'type': $scope.role
            });
            return $scope.onFilterChange();
        };

        $scope.order = function (rowName) {
            if ($scope.row === rowName) {
                return;
            }
            $scope.row = rowName;
            $scope.filteredStores = $filter('orderBy')($scope.stores, rowName);
            return $scope.onOrderChange();
        };

        $scope.back = function () {
            $location.path("/achievements/questions");
        };

        var init = function () {
            $scope.roleName = { 0: '管理员', 1: '组用户', 2: '地方用户' };
	        $scope.searchKeywords = window.localStorage['achievement_userNameKey'] == undefined ? '' : window.localStorage['achievement_userNameKey'];
            $scope.filteredStores = [];
            $scope.row = '';
            $scope.numPerPageOpt = [3, 5, 10, 20];
            $scope.numPerPage = $scope.numPerPageOpt[2];
            $scope.currentPage = 1;
            $scope.currentPageStores = [];

            var question = sessionStorage.achievementsQuestionId;
            tool.get(link.achievements + "questionsusersolved?id=" + question, {}, function (data) {
                $scope.question = data.questions[0].summary;
                $scope.stores = data.questions[0].scoreboards;
                $scope.search();
                return $scope.select($scope.currentPage);
            });

        };
        init();
    }
})();