(function() {
	'use strict';

	angular.module('app.courses', [
			'ngFileUpload', 'app.service'
		])
		.controller('CoursesCtrl', ['$scope', '$filter', '$http', '$state', '$location', '$uibModal', 'logger', 'appConfig', CoursesCtrl])
		.controller('CourseAddCtrl', ['$scope', '$http', '$uibModal', '$location', 'Upload', 'logger', 'appConfig', CourseAddCtrl])
		.controller('CourseEditCtrl', ['$scope', '$http', '$uibModal', '$location', 'Upload', '$stateParams', 'logger', 'appConfig', CourseEditCtrl])
		.controller('AddCourseModalCtrl', ['$scope', '$http', 'datas', '$uibModalInstance', 'appConfig', AddCourseModalCtrl])
		.controller('CourseLearnCtrl', ['$scope', '$http', '$uibModal', '$location', '$stateParams', 'logger', 'appConfig', CourseLearnCtrl])
		.controller('ConfirmCtrl', ['$scope', '$uibModalInstance', ConfirmCtrl]);

	function CourseAddCtrl($scope, $http, $uibModal, $location, Upload, logger, appConfig) {

		$scope.files = {};

		$http.get(appConfig.main.server + '/services/courses/lesson/category/list')
			.success(function(data) {
				$scope.cates = data.question_categories;
			});

		$http.get(appConfig.main.server + '/services/questions/listall')
			.success(function(data) {
                $scope.questions = data.questions;
                $scope.questions2 = [];
			});

		$scope.lessonManage = function() {
			var modalInstance = $uibModal.open({
				animation: true,
				templateUrl: 'addCourseContent.html',
				controller: 'AddCourseModalCtrl',
				resolve: {
					datas: function() {
						return {
                            questions: $scope.questions,
                            questions2: $scope.questions2,
							cates: $scope.cates
						}
					}
				}
			});

			modalInstance.result.then(function(lessons) {
				$scope.questionSel = lessons;
			}, function() {

			});
		}
		$scope.calFileSize = function(size) {
			var sizeLabel = ["B", "KB", "MB", "GB"];
			for(var index = 0; index < sizeLabel.length; index++) {
				if(size < 1024) {
					return round(size, 2) + sizeLabel[index];
				}
				size = size / 1024;
			}
			return round(size, 2) + sizeLabel[index];
		}
		var round = function(number, count) {
			return Math.round(number * Math.pow(10, count)) / Math.pow(10, count);
		};
		var getObjectURL = function(file) {
			var url = null;
			if(window.createObjectURL != undefined) { // basic
				url = window.createObjectURL(file);
			} else if(window.URL != undefined) { // mozilla(firefox)
				url = window.URL.createObjectURL(file);
			} else if(window.webkitURL != undefined) { // webkit or chrome
				url = window.webkitURL.createObjectURL(file);
			}
			return url;
		}
		$scope.uploadfile = function(file, type) {
			var curSize = parseFloat($scope.calFileSize(file.size));
			if(!!curSize && !!file.name) {
				if(curSize > 500 * 1024) {
					logger.logError("文件大小不能超过500M");
					return;
				}
				if($scope.currentSize == curSize && $scope.fileName == file.name) {
					logger.logError("文件已上传,请勿重复提交");
					return;
				} else {
					$scope.currentSize = curSize;
				}
			} else {
				logger.logError("文件错误");
				return;
			}
			var objUrl = getObjectURL(file);
			Upload.upload({
				url: appConfig.main.server + '/services/fileupload',
				data: {
					upfile: file,
					uploadFileName: file.name,
					uploadContentType: file.type
				}
			}).then(function(response) {
				logger.logSuccess("文件上传成功");
				$scope.files[type] = {
					name: response.data.originalName,
					url: response.data.url
				}
			}, function(response) {
				if(response.status > 0) {
					$scope.errorMsg = response.status + ': ' + response.data;
				}
			}, function(evt) {
				//$scope.progress = parseInt(100.0 * evt.loaded / evt.total);
			});
		};
		
		var checkForm = function() {
			if (!$scope.title) {
				logger.logError("课程名称不能为空");
				return false;
			} else if (!$scope.introduction) {
				logger.logError("简介不能为空");
				return false;
			} else if (!$scope.questionSel || $scope.questionSel.length == 0) {
				logger.logError("课时列表不能为空");
				return false;
			}
            return true;
		};

		$scope.save = function() {
			if (!checkForm()) {
				return;
			}
			var course = {
				title: $scope.title,
				questions: [],
				difficult: $scope.difficult,
				introduction: $scope.introduction
			}
			for(var i in $scope.questionSel) {
				course.questions.push($scope.questionSel[i].id);
			}
			course.pic = $scope.files['picture'] ? $scope.files['picture'].url : null;
			course.video = $scope.files['video'] ? $scope.files['video'].url : null;
			$http.post(appConfig.main.server + "/services/courses/save", course).success(function(data) {
				if(data.exception) {
					logger.logError(data.exception);
				} else {
					$location.url('/courses');
				}
			}).error(function() {
				logger.logError("保存出错！");
			});
		}

		$scope.back = function() {
			$location.url('/courses');
		}
	}

	function CourseEditCtrl($scope, $http, $uibModal, $location, Upload, $stateParams, logger, appConfig) {

		$scope.files = {};

		$http.get(appConfig.main.server + '/services/courses/' + $stateParams.courseId).success(function(data) {
			var lesson = data.lesson;
			$scope.title = lesson.title;
			$scope.difficult = lesson.difficult + '';
			$scope.introduction = lesson.introduction;
			$scope.questionSel = lesson.questions;
			if(data.picture) {
				$scope.files['picture'] = {
					name: data.picture,
					url: data.picture
				}
			}
			if(data.video) {
				$scope.files['video'] = {
					name: data.video,
					url: data.video
				}
			}
            $http.get(appConfig.main.server + '/services/questions/listall')
                .success(function(data) {
                    $scope.questions = data.questions;
                    $scope.questions2 = $scope.questionSel;
                });
		});

		$http.get(appConfig.main.server + '/services/courses/lesson/category/list')
			.success(function(data) {
				$scope.cates = data.question_categories;
			});

		$scope.lessonManage = function() {
			var modalInstance = $uibModal.open({
				animation: true,
				templateUrl: 'addCourseContent.html',
				controller: 'AddCourseModalCtrl',
				resolve: {
					datas: function() {
						return {
                            questions: $scope.questions,
                            questions2: $scope.questions2,
							cates: $scope.cates
						}
					}
				}
			});

			modalInstance.result.then(function(lessons) {
				$scope.questionSel = lessons;
			}, function() {

			});
		}

		$scope.calFileSize = function(size) {
			var sizeLabel = ["B", "KB", "MB", "GB"];
			for(var index = 0; index < sizeLabel.length; index++) {
				if(size < 1024) {
					return round(size, 2) + sizeLabel[index];
				}
				size = size / 1024;
			}
			return round(size, 2) + sizeLabel[index];
		}
		var round = function(number, count) {
			return Math.round(number * Math.pow(10, count)) / Math.pow(10, count);
		};
		var getObjectURL = function(file) {
			var url = null;
			if(window.createObjectURL != undefined) { // basic
				url = window.createObjectURL(file);
			} else if(window.URL != undefined) { // mozilla(firefox)
				url = window.URL.createObjectURL(file);
			} else if(window.webkitURL != undefined) { // webkit or chrome
				url = window.webkitURL.createObjectURL(file);
			}
			return url;
		};
		$scope.uploadfile = function(file, type) {
			$scope.progress = 0;
			if(!file) {
				return;
			}
			var curSize = parseFloat($scope.calFileSize(file.size));
			if(!!curSize && !!file.name) {
				if(curSize > 500 * 1024) {
					logger.logError("文件大小不能超过500M");
					return;
				}
				if($scope.currentSize == curSize && $scope.fileName == file.name) {
					logger.logError("文件已上传,请勿重复提交");
					return;
				} else {
					$scope.currentSize = curSize;
				}
			} else {
				logger.logError("文件错误");
				return;
			}
			var objUrl = getObjectURL(file);
			Upload.upload({
				url: appConfig.main.server + '/services/fileupload',
				data: {
					upfile: file,
					uploadFileName: file.name,
					uploadContentType: file.type
				}
			}).then(function(response) {
				logger.logSuccess("文件上传成功");
				$scope.files[type] = {
					name: response.data.originalName,
					url: response.data.url
				}
			}, function(response) {
				if(response.status > 0) {
					$scope.errorMsg = response.status + ': ' + response.data;
				}
			}, function(evt) {
				if(type == 'video') {
					$scope.progress = parseInt(100.0 * evt.loaded / evt.total);
				}
			});
		};

		$scope.save = function() {
			var course = {
				title: $scope.title,
				questions: [],
				difficult: $scope.difficult,
				introduction: $scope.introduction
			}
			for(var i in $scope.questionSel) {
				course.questions.push($scope.questionSel[i].id);
			}
			course.pic = $scope.files['picture'] ? $scope.files['picture'].url : null;
			course.video = $scope.files['video'] ? $scope.files['video'].url : null;
			$http.post(appConfig.main.server + "/services/courses/" + $stateParams.courseId, course).success(function(data) {
				if(data.exception) {
					logger.logError(data.exception);
				} else {
					$location.url('/courses');
				}
			}).error(function() {
				logger.logError("保存出错！");
			});
		}

		$scope.back = function() {
			$location.url('/courses');
		}
	}

	function AddCourseModalCtrl($scope, $http, datas, $uibModalInstance, appConfig) {
        $scope.cates = datas.cates;
        $scope.questions2 = datas.questions2;
        $scope.questions = datas.questions.filter(function (q1) {
            return $scope.questions2.map(function (q2) {
                    return q2.id
                }).indexOf(q1.id) === -1
        });

        $scope.changeCate = function(cate) {
            $scope.questions = function () {
                if (cate === null) {
                    return datas.questions;
                } else {
                    return datas.questions.filter(function (q) {
                        return q.categories.indexOf(cate) !== -1
                    })
                }
            }().filter(function (q1) {
                return $scope.questions2.map(function (q2) {
                        return q2.id
                    }).indexOf(q1.id) === -1
            });
        };

		$scope.ltor = function() {
			for(var i in $scope.lesson) {
				$scope.questions.splice($scope.questions.indexOf($scope.lesson[i]), 1);
				$scope.questions2.push($scope.lesson[i]);
			}
		};

		$scope.lator = function() {
			for(var i in $scope.questions) {
				$scope.questions2.push($scope.questions[i]);
			}
			$scope.questions = [];
		};

		$scope.rtol = function() {
			for(var i in $scope.lesson2) {
				$scope.questions2.splice($scope.questions2.indexOf($scope.lesson2[i]), 1);
				$scope.questions.push($scope.lesson2[i]);
			}
		};

		$scope.ratol = function() {
			for(var i in $scope.questions2) {
				$scope.questions.push($scope.questions2[i]);
			}
            $scope.questions2.splice(0, $scope.questions2.length);
		};

		$scope.ok = function() {
			$uibModalInstance.close($scope.questions2);
		};

		$scope.cancel = function() {
			$uibModalInstance.dismiss("cancel");
		};
	}

	function CoursesCtrl($scope, $filter, $http, $state, $location, $uibModal, logger, appConfig) {

		var init;

		init = function() {
			$scope.searchKeywords = window.localStorage['courseNameKey'] == undefined ? '' : window.localStorage['courseNameKey'];
			$scope.canedit = (window.localStorage['uname'] == appConfig.main.adminuser);
			$http.get(appConfig.main.server + '/services/courses/listall')
				.success(function(data) {
					$scope.courses = data.lessons;
					$scope.search();
				});
		};

		$scope.addCourse = function() {
			$location.url('/courses/add');
		}

		$scope.editor = function(courseId) {
			$state.go('courses/editor', {
				courseId: courseId
			});
		}

		$scope.catalog = function() {
			$location.url('/courses/catalog');
		}

		$scope.courseRemove = function(course) {
			var modalInstance = $uibModal.open({
				animation: true,//$scope.animationsEnabled,
				templateUrl: 'confirm.html',
				controller: 'ConfirmCtrl'
			});
			modalInstance.result.then(function() {
				$http.post(appConfig.main.server + "/services/courses/delete?id=" + course.id, {}).success(function(data) {
					logger.logSuccess("记录已删除！");
					$scope.courses.splice($scope.courses.indexOf(course), 1);
					$scope.search();
				});
			});
		}
		
		$scope.difficultList = [{
			name: '全部',
			val: undefined
		}, {
			name: '低',
			val: 1
		}, {
			name: '中',
			val: 2
		}, {
			name: '高',
			val: 3
		}];

        $scope.difficultDict = {
            1: '低',
			2: '中',
			3: '高',
	        undefined: '无'
		};



		$scope.ischoose2 = function(df) {
			return $scope.difficult == df.val ? 'label-primary' : 'label-default';
		}
		
		$scope.filterDifficult = function(df) {
			if($scope.difficult == df.val) {
				$scope.difficult = undefined;
			} else {
				$scope.difficult = df.val;
			}
			return $scope.search();
		}
		
		$scope.search = function() {
			window.localStorage['courseNameKey'] = $scope.searchKeywords;
			$scope.filteredCourses = $filter('filter')($scope.courses, {
				"title": $scope.searchKeywords,
				'difficult': $scope.difficult
			});
			return $scope.filteredCourses;
		};

		$scope.difficultColorBG = function(dif) {
			if(dif) {
				return 'bg-' + appConfig.difficultColor[dif + ''];
			} else {
				return 'bg-dark';
			}
		}

		$scope.difficultColorText = function(dif) {
			if(dif) {
				return 'text-' + appConfig.difficultColor[dif + ''];
			} else {
				return 'text-dark';
			}
		}

		$scope.learn = function(courseid) {
			$state.go('courses/learn', {
				courseId: courseid
			});
		}

        $scope.canedit = (window.localStorage['uname'] == appConfig.main.adminuser);
		init();
	}

	function CourseLearnCtrl($scope, $http, $uibModal, $location, $stateParams, logger, appConfig) {

		$http.get(appConfig.main.server + '/services/courses/' + $stateParams.courseId).success(function(data) {
			$scope.course = data.lesson;
			$scope.video = appConfig.main.upload + data.video;
			$scope.diff = [];
			for(var i = 0; i < data.lesson.difficult; i++) {
				$scope.diff.push(i);
			}
			var color = ['#27C46B', '#EEC800', '#F63A49'];
			$scope.diffcolor = color[data.lesson.difficult - 1];

            jwplayer('myplayer').setup({
                file: $scope.video,
                width: '100%',
                height: '600'
            });
		});

		$scope.back = function() {
			$location.url('/courses');
		}

		$scope.learnnow = function(questionId, courseid) {
			$http.post(appConfig.main.server + '/services/questions/lesson/' + courseid + '/learn', {
				question_id: questionId
			}).success(function(data) {
				sessionStorage.questionId = questionId;
				$location.url('questions/topo');
			});
		}
	}

	function ConfirmCtrl($scope, $uibModalInstance) {

		$scope.ok = function() {
			$uibModalInstance.close();
		};

		$scope.cancel = function() {
			$uibModalInstance.dismiss("cancel");
		};
	}

})();
