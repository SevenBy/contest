(function () {
    'use strict';

    angular.module('app.service', [])
        .service('link', linkService)
        .service('tool', ['$http', 'logger', 'appConfig', toolService]);

    function linkService() {
        return {
            contestInfo: "/services/contests/",
            allUser: "/services/accounts/listall",//获取所有用户信息
            allQues: "/services/questions/listall",//题目列表（所有）
            quesFollowUsers: "/services/questions/",//获取题目的发布用户
            getUsers: "/services/accounts/listall",//用户列表（所有）
            delUser: "/services/accounts/delete",//删除单个用户
            addUser: "/services/accounts/save",//新增用户
            editUser: "/services/accounts/update",//更新用户信息
            userPublish: "/services/accounts/",//发布用户到组用户，获取组用户的已发布用户，题目授权给用户，获取用户已授权题目
            achievements: "/services/achievements/",//考核信息
            changePwd: "/services/accounts/password/",//修改用户密码
            test: "test"
        }
    }

    function toolService($http, logger, appConfig) {
        var service = {};
        service.post = function (url, data, done) {
            $http.post(appConfig.main.server + url, data).success(function (res) {
                done(res);
            }).error(function (err) {
                console.warn(err);
            })
        };
        service.get = function (url, data, done) {
            $http.get(appConfig.main.server + url, data).success(function (res) {
                done(res);
            }).error(function (err) {
                console.warn(err);
            })
        };
        return service;
    }

})();