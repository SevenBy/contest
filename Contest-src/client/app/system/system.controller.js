(function() {
	'use strict';

	angular.module('app.system')
		.controller('SystemCtrl', ['$scope', '$http', '$location', 'appConfig', SystemCtrl])
		.controller('AccountsCtrl', ['$scope', '$http', '$filter', 'appConfig', AccountsCtrl])
		.controller('LogsCtrl', ['$scope', '$http', '$filter', 'appConfig', LogsCtrl])
		.filter('logtype', function() {
			return function(log) {
				if(log.type == 1) {
					return '用户登录';
				} else if(log.type == 10) {
					return '用户登录比赛';
				} else if(log.type == 2) {
					return '注销登录';
				} else if(log.type == 11) {
					return '用户登出比赛';
				} else if(log.type == 3) {
					if(log.state != -1) {
						return '启动题目 ' + log.qstName + ' 成功';
					} else {
						return '启动题目 ' + log.qstName + ' 失败';
					}
				} else if(log.type == 4) {
					if(log.state != -1) {
						return '关闭题目 ' + log.qstName + ' 成功';
					} else {
						return '关闭题目 ' + log.qstName + ' 失败';
					}
				} else if(log.type == 5) {
					return '启动题目 ' + log.qstName + ' 的攻击机';
				} else if(log.type == 6) {
					return '启动单个虚拟机，' + log.servername;
				} else if(log.type == 7) {
					return '删除单个虚拟机，' + log.servername;
				} else if(log.type == 8) {
					return '评价题目 ' + log.qstName;
				} else if(log.type == 9) {
					if(log.state == 1 && log.score > 0) {
						return '提交题目 ' + log.qstName + ' 的旗帜文件成功，得 ' + log.score + '分';
					} else {
						return '提交题目 ' + log.qstName + ' 的旗帜文件失败，不得分';
					}
				} else if(log.type == 20) {
					return '创建题目 ' + log.extra;
				} else if(log.type == 21) {
					return '删除题目 ' + log.extra;
				} else if(log.type == 30) {
					return '创建用户 ' + log.extra;
				} else if(log.type == 31) {
					return '删除用户 ' + log.extra;
				}
			}
		});

	function SystemCtrl($scope, $http, $location, appConfig) {

	}

	function AccountsCtrl($scope, $http, $filter, appConfig) {

		var init;

		$scope.searchKeywords = '';

		$scope.filteredStores = [];

		$scope.row = '';

		$scope.select = function(page) {
			var end, start;
			start = (page - 1) * $scope.numPerPage;
			end = start + $scope.numPerPage;
			return $scope.currentPageStores = $scope.filteredStores.slice(start, end);
		};

		$scope.onFilterChange = function() {
			$scope.select(1);
			$scope.currentPage = 1;
			return $scope.row = '';
		};

		$scope.onNumPerPageChange = function() {
			$scope.select(1);
			return $scope.currentPage = 1;
		};

		$scope.onOrderChange = function() {
			$scope.select(1);
			return $scope.currentPage = 1;
		};

		$scope.search = function() {
			$scope.filteredStores = $filter('filter')($scope.stores, $scope.searchKeywords);
			return $scope.onFilterChange();
		};

		$scope.order = function(rowName) {
			if($scope.row === rowName) {
				return;
			}
			$scope.row = rowName;
			$scope.filteredStores = $filter('orderBy')($scope.stores, rowName);
			return $scope.onOrderChange();
		};

		$scope.numPerPageOpt = [3, 5, 10, 20];

		$scope.numPerPage = $scope.numPerPageOpt[2];

		$scope.currentPage = 1;

		$scope.currentPageStores = [];

		var utype = {
			"1": "组用户",
			"2": "地方用户",
		}
		$scope.usertype = function(v) {
			return utype[v];
		}

		$scope.statusLabel = function(st) {
			return appConfig.contestStatus[st];
		}

		$scope.statusCls = function(st) {
			var cls = {
				0: '#FFB61C',
				1: '#1C7EBB',
				2: '#23AE89'
			}
			return cls[st];
		}

		init = function() {
			$http.get(appConfig.main.server + '/services/accounts/listall').success(function(data) {
				$scope.stores = data.users;
				$scope.search();
				return $scope.select($scope.currentPage);
			});
		};

		init();
	}

	function LogsCtrl($scope, $http, $filter, appConfig) {

		var init;

		$scope.searchKeywords = window.localStorage['log_userNameKey'] == undefined ? '' : window.localStorage['log_userNameKey'];

		$scope.filteredStores = [];

		$scope.row = 'logtime';
		$scope.selectrow = 'logtime';

		$scope.desc = 'desc';
		
		$scope.disable_query = window.localStorage['role'] == 'user';

		$http.get(appConfig.main.server + '/services/logtype/list').success(function(data) {
			$scope.logtypes = [{name: "全部", value: undefined}].concat(data);
		});

		$scope.ischoose = function(lt) {
			return $scope.logtype == lt.value ? 'label-primary' : 'label-default';
		}

		$scope.filterLabel = function(lt) {
			if($scope.logtype == lt.value) {
				$scope.logtype = undefined;
			} else {
				$scope.logtype = lt.value;
			}
			return $scope.select(1);
		}

		$scope.select = function(page) {
			$scope.currentPage = page;
			window.localStorage['log_userNameKey'] = $scope.searchKeywords;
			$http.post(appConfig.main.server + '/services/log/list?username=' + $scope.searchKeywords, {
				page: $scope.currentPage,
				rows: $scope.numPerPage == '全部'? 0: $scope.numPerPage,
				sort: $scope.selectrow,
				order: $scope.desc,
				logtype: $scope.logtype
			}).success(function(data) {
				$scope.total = data.totalElements;
				$scope.logs = data.content;
			});
		};

		$scope.onFilterChange = function() {
			$scope.select(1);
		};

		$scope.onNumPerPageChange = function() {
			$scope.select(1);
		};

		$scope.onOrderChange = function() {
			$scope.select(1);
		};

		$scope.search = function() {
			$scope.select(1);
		};

		$scope.order = function(rowName) {
			if($scope.row === rowName) {
				return;
			} else {
				$scope.row = rowName;
			}
			if(rowName.substring(0, 1) == '-') {
				$scope.desc = 'desc';
				$scope.selectrow = rowName.substring(1);
			} else {
				$scope.desc = 'asc';
				$scope.selectrow = rowName;
			}
			$scope.select(1);
		};

		// $scope.numPerPageOpt = [3, 5, 10, 20, '全部'];
		$scope.numPerPageOpt = [3, 5, 10, 20, 50];

		$scope.numPerPage = $scope.numPerPageOpt[2];

		$scope.currentPage = 1;

		$scope.currentPageStores = [];

		init = function() {
			$scope.select(1);
		};

		init();
	}

})();