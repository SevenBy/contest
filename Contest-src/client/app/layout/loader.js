(function () {
    $(window).load(function(){
        hideLoader();
        showUser();
    });
    
    function showUser() {
    		if($("#user-header").length < 1) {
    			setTimeout(showUser, 1000);
    		} else {
    			$("#user-header").html(window.localStorage['displayname']);
    		}
    }

    function hideLoader() {
        $('#loader-container').fadeOut("fast")
    }    
})(); 
