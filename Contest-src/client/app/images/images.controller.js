(function() {
	'use strict';

	angular.module('app.contests')
		.controller('ImagesCtrl', ['$scope', '$http', '$filter', '$location', '$sce', '$uibModal', '$state', '$rootScope', 'logger', 'appConfig', ImagesCtrl])
		.controller('VmlistCtrl', ['$scope', '$http', '$filter', '$stateParams', '$uibModal', '$window', 'logger', 'appConfig', VmlistCtrl])
		.controller('ImagesDetailCtrl', ['$scope', '$sce', 'datas', '$uibModalInstance', 'appConfig', ImagesDetailCtrl])
		.controller('LogDetailCtrl', ['$scope', '$sce', 'datas', '$uibModalInstance', 'appConfig', LogDetailCtrl])
		.controller('ImageEditCtrl', ['$scope', '$http', '$timeout', '$location', 'appConfig', 'Upload', 'logger', ImageEditCtrl]);

	function ImagesCtrl($scope, $http, $filter, $location, $sce, $uibModal, $state, $rootScope, logger, appConfig) {

		$scope.editImage = function(img) {
			sessionStorage.editImg = JSON.stringify(img);
			$location.path("/images/edit");
		}

		$scope.ischoose = function(lb) {
			return $scope.label == lb.val ? 'label-primary' : 'label-default';
		}

		$scope.labeledit = false;
		$scope.modlabel = function() {
			$scope.labeledit = !$scope.labeledit;
		}

		$scope.filterLabel = function(lb) {
			if($scope.labeledit) {
				if (lb.val == undefined)
					return;
				// 删除操作
				$http.post(appConfig.main.server + '/services/images/category/delete', {
					label: lb.val
				}).success(function(data) {
					if(data.exception){
						logger.logError(data.exception);
					} else {
						$http.get(appConfig.main.server + "/services/images/categories").success(function(data) {
							$scope.categories = formatCateroty(data.image_categories);
						});
					}
				})
			} else {
				if($scope.label == lb.val) {
					$scope.label = undefined;
				} else {
					$scope.label = lb.val;
				}
				$scope.cate = lb.val;
				return $scope.searchCate();
			}
		}

		var init;

		$scope.searchKeywords = window.localStorage['imageNameKey'] == undefined ? '' : window.localStorage['imageNameKey'];

		$scope.filteredStores = [];

		$scope.row = '';

		$scope.typeNote = function(tp) {
			if(tp == "snapshot") {
				return "快照";
			} else if(tp == "image") {
				return "镜像";
			}
			return "";
		}

		$scope.labels = function(labs) {
			var ht = [];
			for(var i in labs) {
				ht.push('<span class="label label-primary">');
				ht.push(labs[i]);
				ht.push('</span> ');
			}
			return $sce.trustAsHtml(ht.join(''));
		}

		$scope.select = function(page) {
			var end, start;
			start = (page - 1) * $scope.numPerPage;
			end = start + $scope.numPerPage;
			$scope.currentPage = window.localStorage['currPage'] = page;
			return $scope.currentPageStores = $scope.filteredStores.slice(start, end);
		};

		$scope.onFilterChange = function() {
			$scope.select($scope.currentPage);
			return $scope.row = '';
		};

		$scope.onNumPerPageChange = function() {
			window.localStorage['numPerPage'] = $scope.numPerPage;
			$scope.select(1);
			return $scope.currentPage = 1;
		};

		$scope.onOrderChange = function() {
			$scope.select(1);
			return $scope.currentPage = 1;
		};

		$scope.search = function() {
			window.localStorage['imageNameKey'] = $scope.searchKeywords;
			$scope.filteredStores = $filter('filter')($scope.stores, {
				'categories': $scope.label,
				'name': $scope.searchKeywords
			});
			return $scope.onFilterChange();
		};

		$scope.order = function(rowName) {
			if($scope.row == rowName) {
				return;
			}
			$scope.row = rowName;
			$scope.filteredStores = $filter('orderBy')($scope.stores, rowName);
			return $scope.onOrderChange();
		};

		$scope.numPerPageOpt = [3, 5, 10, 20];
		
		var numPerPage = window.localStorage['numPerPage'];
    	$scope.numPerPage = numPerPage == undefined ? $scope.numPerPageOpt[2] : parseInt(numPerPage);

		$scope.currentPage = 1;

		$scope.currentPageStores = [];

		$scope.statusLabel = function(st) {
			return appConfig.contestStatus[st];
		}

		$scope.statusNote = function(v) {
			var note = {
				"active": "运行中",
				"saving": "保存中",
				"queued": "已排队",
				"pending delete": "等待删除完成",
				"killed": "已杀死",
				"deleted": "已删除"
			}
			return note[v]
		}

		$scope.statusCls = function(st) {
			var cls = {
				"saving": '#1C7EBB',
				"active": '#23AE89',
				"queued": '#2EC1CC',
				"pending delete": '#FFB61C',
				"deleted": '#E94B3B',
				"killed": '#DCDCDC'
			}
			return cls[st];
		}

		$scope.sizeNote = function(v) {
			if(v > 1024 * 1024 * 1024) {
				return(v / 1024 / 1024 / 1024).toFixed(2) + 'GB';
			} else if(v > 1024 * 1024) {
				return(v / 1024 / 1024).toFixed(2) + 'MB';
			} else {
				return(v / 1024).toFixed(2) + 'KB';
			}
		}

		$scope.searchCate = function() {
			$scope.currentPage = 1;
			$rootScope.$broadcast('preloader:active');
			$http.post(appConfig.main.server + "/services/images/list2all", {
				category: ($scope.cate || '')
			}).success(function(data) {
				$rootScope.$broadcast('preloader:hide');
				$scope.stores = data.images;
				$scope.search();
			}).error(function(err) {
				$rootScope.$broadcast('preloader:hide');
			});;
		}

		$scope.vm = function(id) {
			$state.go('images/vmlist', {
				imageId: id
			});
		}

		$scope.detail = function(id) {
			$http.get(appConfig.main.server + '/services/images/' + id).success(function(data) {
				$scope.image = data.image;
				var modalInstance = $uibModal.open({
					animation: true,
					templateUrl: 'imageDetail.html',
					controller: 'ImagesDetailCtrl',
					resolve: {
						datas: function() {
							return {
								image: $scope.image
							}
						}
					}
				});

				modalInstance.result.then(function(lessons) {

				}, function() {

				});
			});

		}
		
		$scope.delImage = function(imageId) {
			var modalInstance = $uibModal.open({
				animation: true, //$scope.animationsEnabled,
				templateUrl: 'confirm.html',
				controller: 'ConfirmCtrl'
			});
			
			modalInstance.result.then(function() {
				$http.post(appConfig.main.server + "/services/images/delete?id=" + imageId, {}).success(function(data) {
					if(data.exception){
						logger.logError(data.exception);
					} else {
						logger.logSuccess("镜像已删除！");
						var idx = -1;
						for(var i = 0, len = $scope.stores.length; i < len; i++) {
							if($scope.stores[i].id == imageId) {
								idx = i;
								break;
							}
						}
						$scope.stores.splice(idx, 1);
						$scope.search();
					}
				});
			});
		}
		
		var formatCateroty = function(list1){
			var list2 = [];
			list2.push({name: '全部', val:undefined});
			for (var i in list1){
				list2.push({name: list1[i], val:list1[i]});
			}
			return list2;
		}

		init = function() {
			$http.get(appConfig.main.server + '/services/images/listall').success(function(data) {
            	var currentPage = window.localStorage['currPage'];
                if(currentPage != undefined) $scope.currentPage = currentPage;
                $scope.stores = data.images;
				$scope.search();
				return $scope.select($scope.currentPage);
			});
			$http.get(appConfig.main.server + '/services/images/categories').success(function(data) {
				$scope.categories = formatCateroty(data.image_categories);
			});
		};

		init();
	}

	function ImagesDetailCtrl($scope, $sce, datas, $uibModalInstance, appConfig) {
		$scope.image = datas.image;
		$scope.cancel = function() {
			$uibModalInstance.dismiss("cancel");
		};
		$scope.labels = function(categories) {
			var labs = categories.split(",");
			var ht = [];
			for(var i in labs) {
				ht.push('<span class="label label-primary">');
				ht.push(labs[i]);
				ht.push('</span> ');
			}
			return $sce.trustAsHtml(ht.join(''));
		}
		$scope.sizeNote = function(v) {
			if(v > 1024 * 1024 * 1024) {
				return(v / 1024 / 1024 / 1024).toFixed(2) + 'GB';
			} else if(v > 1024 * 1024) {
				return(v / 1024 / 1024).toFixed(2) + 'MB';
			} else {
				return(v / 1024).toFixed(2) + 'KB';
			}
		}
		$scope.statusNote = function(v) {
			var note = {
				"active": "运行中",
				"saving": "保存中",
				"queued": "已排队",
				"pending delete": "等待删除完成",
				"killed": "已杀死",
				"deleted": "已删除"
			}
			return note[v]
		}
		$scope.typeNote = function(tp) {
			if(tp == "snapshot") {
				return "快照";
			} else if(tp == "image") {
				return "镜像";
			}
			return "";
		}
		$scope.statusCls = function(st) {
			var cls = {
				"saving": '#1C7EBB',
				"active": '#23AE89',
				"queued": '#2EC1CC',
				"pending delete": '#FFB61C',
				"deleted": '#E94B3B',
				"killed": '#DCDCDC'
			}
			return cls[st];
		}

	}

	function VmlistCtrl($scope, $http, $filter, $stateParams, $uibModal, $window, logger, appConfig) {
		var imageId = $stateParams.imageId;
		$http.get(appConfig.main.server + '/services/images/vm/list/' + imageId).success(function(data) {
			$scope.filteredStores = $scope.servers = data.servers;
		});
		$http.get(appConfig.main.server + '/services/questions/flavors').success(function(data) {
			$scope.flavors = data.flavors;
		});
		$scope.fdate = function(dt) {
			return dt ? dt.replace('T', ' ').replace('Z', '') : "--";
		}
		$scope.powerState = function(key) {
			return appConfig.vmPowerStatus[key.toLowerCase()];
		}
		$scope.vmState = function(key) {
			return appConfig.vmStatus[key.toLowerCase()];
		}
		$scope.back = function() {
			$window.history.back();
		}
		$scope.vnc = function(serverid) {
			$http.get(appConfig.main.server + '/services/images/vm/vnc/' + serverid).success(function(data) {
				$window.open(data.console.url, 'C-Sharpcorner', 'width=1024,height=800');
			})
		}
		$scope.startVm = function() {
			layer.load(1, {
				shade: [0.2, '#fff'] //0.1透明度的白色背景
			})
			$http.post(appConfig.main.server + '/services/images/' + imageId + '/verify', {
				flavorRef: $scope.flavor.id
			}).success(function(data) {
				layer.closeAll();
				$window.location.reload();
			}).error(function(data) {
				layer.closeAll();
			})
		}

		$scope.logdetail = function(id) {
			$http.get(appConfig.main.server + '/services/images/vm/log/' + id).success(function(data) {
				var modalInstance = $uibModal.open({
					animation: true,
					templateUrl: 'logDetail.html',
					controller: 'LogDetailCtrl',
					size: 'lg',
					resolve: {
						datas: function() {
							return {
								context: data.output
							}
						}
					}
				});
				modalInstance.result.then(function(lessons) {}, function() {});
			});
		}

		$scope.operate = function(server, action) {
            if (action == 'start' && server.power_state == 'Shutdown' || action == 'stop' && server.power_state != 'Shutdown' || action == 'suspend' || action == 'resume' || action == 'reboot' || action == 'pause' || action == 'unpause') {
                $http.get(appConfig.main.server + '/services/images/vm/operate/' + server.id + "?action=" + action).success(function(data) {
                    logger.logSuccess("操作成功");
                    $window.location.reload();
                });
			}
		};

		$scope.destroy = function(server) {
			$http.post(appConfig.main.server + '/services/images/vm/delete/' + server.id).success(function(data) {
				logger.logSuccess("操作成功");
				$window.location.reload();
			});
		}
		
		$scope.row  = '';
		$scope.order = function(rowName) {
			console.log($scope.row, rowName);
			if($scope.row == rowName) {
				return;
			}
			$scope.row = rowName;
			$scope.filteredStores = $filter('orderBy')($scope.servers, rowName);
		};
	}

	function LogDetailCtrl($scope, $sce, datas, $uibModalInstance, appConfig) {
		$scope.context = datas.context;
		$scope.cancel = function() {
			$uibModalInstance.dismiss("cancel");
		};
	}

	function ImageEditCtrl($scope, $http, $timeout, $location, appConfig, Upload, logger) {
		var init = function() {
			$http.get(appConfig.main.server + '/services/images/categories').success(function(data) {
				$scope.categoryList = data.image_categories;
				$scope.image = JSON.parse(sessionStorage.editImg);
				$http.get(appConfig.main.server + '/services/images/' + $scope.image.id).success(function(data) {
					$scope.image = data.image;
					if(typeof($scope.image.description) == "object"){
						$scope.image.description = JSON.stringify($scope.image.description);
						$scope.image.properties.description = JSON.stringify($scope.image.properties.description);
					}
					$scope.image.categories = ($scope.image.categories).split(',');
					if($scope.image.categories.length == 1) {
						$("#categories").val('string:' + $scope.image.categories[0]);
					}
					$("#categories").select2({
						placeholder: "请选择标签",
						tags: true,
						tokenSeparators: [',', ' ']
					});
				});
			});
		}
		init();
		$scope.back = function() {
			layer.closeAll();
			$location.url("/images", {
                reload: true
            });
		}
		var checkForm = function() {
			$scope.image.categories = [];
			for(var i in $("#categories").val()) {
				if($("#categories").val()[i]) {
					$scope.image.categories.push($("#categories").val()[i].replace("string:", ""));
				}
			}
			if(!$scope.image.name) {
				logger.logError("镜像名不能为空");
				return false;
			}
			return true;
		}

		$scope.save = function() {
			if(checkForm()) {
				$http.post(appConfig.main.server + '/services/images/' + $scope.image.id, {
					name: $scope.image.name,
					categories: $scope.image.categories,
					description: $scope.image.description
				}).success(function(data) {
					if(data.image) {
						logger.logSuccess("修改成功");
						$timeout(function() {
							$scope.back();
						}, 2000);
					}
				})
			}
		}
	}

})();