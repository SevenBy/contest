(function() {
	'use strict';

	angular.module('app.page')
		.directive('customPage', customPage)
		.directive('ngUploadForm', ['appConfig', function(appConfig) {
			return {
				restrict: 'E',
				templateUrl: './assets/js/fileform.html',
				scope: {
					allowed: '@',
					url: '@',
					autoUpload: '@',
					sizeLimit: '@',
					ngModel: '=',
					name: '@',
					label: '@',
					number: '@'
				},
				controller: ['$scope', '$element', 'fileUpload', function($scope, $element, fileUpload) {
					$scope.$on('fileuploaddone', function(e, data) {
						var file = {
							file: data.files[0],
							result: data._response.result
						}
						fileUpload.addFieldData($scope.name, file);
					});

					$scope.options = {
						url: appConfig.main.server + $scope.url,
						dropZone: $element,
						maxFileSize: $scope.sizeLimit,
						autoUpload: $scope.autoUpload
					};
					$scope.loadingFiles = false;

					if(!$scope.queue) {
						$scope.queue = [];
					}

					var generateFileObject = function generateFileObjects(objects) {
						angular.forEach(objects, function(value, key) {
							var fileObject = {
								name: value.file.name,
								size: value.file.size,
								url: value.result.url,
								thumbnailUrl: appConfig.main.upload + value.result.url,
								result: value.file
							};

							$scope.queue[key] = fileObject;
						});
					};
					fileUpload.registerField($scope.name);
					$scope.filequeue = fileUpload.fieldData[$scope.name];

					$scope.$watchCollection('filequeue', function(newval) {
						generateFileObject(newval);
					});
				}]
			};
		}]).controller('FileDestroyController', ['$scope', '$http', 'fileUpload', function($scope, $http, fileUpload) {
			var file = $scope.file;

			if($scope.$parent && $scope.$parent.$parent && $scope.$parent.$parent.$parent.name) {
				$scope.fieldname = $scope.$parent.$parent.$parent.name;
			}

			if(!fileUpload.fieldData[$scope.name]) {
				fileUpload.fieldData[$scope.name] = [];
			}

//			$scope.filequeue = fileUpload.fieldData;

			file.$destroy = function() {
				fileUpload.removeFieldData($scope.fieldname, file.result._id);
				for(var i in $scope.filequeue) {
					if(file.result._id == $scope.filequeue[i].result._id){
						$scope.filequeue.splice(i, 1);
					}
				}
				$scope.clear(file);
				
//				$scope.filequeue = fileUpload.fieldData;
			};
		}]);

	// add class for specific pages to achieve fullscreen, custom background etc.
	function customPage() {
		var directive = {
			restrict: 'A',
			controller: ['$scope', '$element', '$location', '$http', '$window', 'appConfig', customPageCtrl]
		};

		return directive;

		function customPageCtrl($scope, $element, $location, $http, $window, appConfig) {
			var addBg, path;

			path = function() {
				return $location.path();
			};

			addBg = function(path) {
				$element.removeClass('body-wide body-err body-lock body-auth');
				switch(path) {
					case '/dashboard3':
						$scope.contest = true;
						return $element.addClass('body-head');
					case '/404':
					case '/page/404':
					case '/page/500':
						return $element.addClass('body-wide body-err');
					case '/page/signin':
					case '/page/signin2':
                        return $element.addClass('body-wide');
					case '/page/forgot-password':
					case '/page/confirm-email':
						return $element.addClass('body-wide body-auth');
					case '/page/vnc':
						return $element.addClass('body-wide body-lock');
					case '/page/signout':
						var is_contest = window.localStorage['contest'];
						$window.localStorage.clear();
						$http.get(appConfig.main.server + "/services/logout").success(function(data) {
							$window.localStorage.clear();
							$location.url(is_contest ? '/page/signin2' : '/page/signin');
							return $element.addClass('body-wide');
						});
				}
			};

			addBg($location.path());

			$scope.$watch(path, function(newVal, oldVal) {
				if(newVal === oldVal) {
					return;
				}
				if(newVal.indexOf(oldVal) < 0 && oldVal.indexOf(newVal) < 0){
					window.localStorage['currPage'] = 1;
					window.localStorage['numPerPage'] = 10;
				}
				return addBg($location.path());
			});
		}
	}

})();