(function() {
	'use strict';

	angular.module('app.page')
		.controller('invoiceCtrl', ['$scope', invoiceCtrl])
		.controller('authCtrl', ['$scope', '$location', '$http', '$window', 'appConfig', authCtrl])
		.controller('authCtrl2', ['$scope', '$location', '$http', '$window', 'appConfig', authCtrl2]);

	function invoiceCtrl($scope) {
		var printContents, originalContents, popupWin;

		$scope.printInvoice = function() {
			printContents = document.getElementById('invoice').innerHTML;
			originalContents = document.body.innerHTML;
			popupWin = window.open();
			popupWin.document.open();
			popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="styles/main.css" /></head><body onload="window.print()">' + printContents + '</html>');
			popupWin.document.close();
		}
	}

	function authCtrl($scope, $location, $http, $window, appConfig) {
		console.log('登陆前', $window.localStorage);
		if(window.localStorage['contest']){
			$location.url('/dashboard3', {
				reload: true
			})
		} else if(window.localStorage['uname'] == appConfig.main.adminuser ||
				window.localStorage['role'] == 'admin') {
			$location.url('/dashboard', {
				reload: true
			})
		} else if(window.localStorage['role'] == 'user' || window.localStorage['role'] == 'group') {
			$location.url('/dashboard2', {
				reload: true
			})
		}
		$scope.login = function() {
			var uname = $scope.uname;
			var goto_page = decodeURI($location.search().goto_page);
			$http.post(appConfig.main.server + "/services/login", {
				username: $scope.uname,
				userpass: $scope.upass
			}).success(function(data) {
				if (data.status == 0) {
					$scope.error = data.message;
				} else {
					var usertype = data.message.type;
					window.localStorage['uname'] = uname;
					window.localStorage['displayname'] = data.message.realname;
					$("#nav li").show();
					$("#app").removeClass("nav-collapsed-min");
					appConfig.main.uname = window.localStorage['uname'];
					$("#user-header").html(window.localStorage['displayname']);
					if(window.localStorage['uname'] == appConfig.main.adminuser || usertype == 0) {
						window.localStorage['role'] = 'admin';
						if (goto_page != 'undefined'){
							$location.url(goto_page, {
								reload: true
							})
						}
						else{
							$location.url('/dashboard', {
								reload: true
							})
						}
					} else {
						window.localStorage['role'] = usertype == 1 ? 'group' : 'user';
						if (data.message.contest) {
							window.localStorage['contest'] = true;
							window.localStorage['currentContest'] = JSON.stringify(data.message.contest);
							if (goto_page != 'undefined'){
								$location.url(goto_page, {
									reload: true
								})
							}
							else{
								$location.url('/dashboard3', {
									reload: true
								})
							}
						} else {
							if (goto_page != 'undefined'){
								$location.url(goto_page, {
									reload: true
								})
							}
							else{
								$location.url('/dashboard2', {
									reload: true
								})
							}
						}
					}
					console.log('登陆后', window.localStorage);
				}
			}).error(function(data, status, response) {
				$scope.error = '登陆失败！';
			})
		}

		$scope.signup = function() {
			$location.url('/')
		}

		$scope.reset = function() {
			$location.url('/')
		}

		$scope.unlock = function() {
			$location.url('/')
		}
	}

	function authCtrl2($scope, $location, $http, $window, appConfig) {
		console.log('比赛登陆前', window.localStorage);
		if(window.localStorage['contest']){
			$location.url('/dashboard3', {
				reload: true
			})
		} 
		$scope.login = function() {
			var uname = $scope.uname;
			$http.post(appConfig.main.server + "/services/login2", {
				username: $scope.uname,
				userpass: $scope.upass
			}).success(function(data) {
				if (data.status == 0) {
					$scope.error = data.message;
				} else {
					$("#nav li").hide();
					$("#app").addClass("nav-collapsed-min");
					window.localStorage['uname'] = uname;
					window.localStorage['displayname'] = data.message.realname;
					appConfig.main.uname = window.localStorage['uname'];
					$("#user-header").html(window.localStorage['displayname']);
					window.localStorage['contest'] = true;
					window.localStorage['currentContest'] = JSON.stringify(data.message.contest);
					$location.url('/dashboard3', {
						reload: true
					})
				}
			}).error(function(data, status, response) {
				console.log(data, status, response);
				$scope.error = '登陆失败！';
			})
			console.log('比赛登陆后', window.localStorage);
		}
	}
	
})();