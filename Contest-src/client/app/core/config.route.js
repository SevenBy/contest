(function() {
	'use strict';

	angular.module('app')
		.config(['$stateProvider', '$urlRouterProvider', '$ocLazyLoadProvider', function($stateProvider, $urlRouterProvider, $ocLazyLoadProvider) {
			var routes, setRoutes;

			routes = [
				'ui/typography', 'ui/buttons', 'ui/icons', 'ui/grids', 'ui/widgets', 'ui/components', 'ui/boxes', 'ui/timeline', 'ui/pricing-tables', 'ui/cards',
				'table/static', 'table/dynamic', 'table/responsive',
				'form/layouts', 'form/validation', 'form/wizard',
				'chart/echarts', 'chart/echarts-line', 'chart/echarts-bar', 'chart/echarts-pie', 'chart/echarts-scatter', 'chart/echarts-more',
				'page/404', 'page/500', 'page/blank', 'page/forgot-password', 'page/confirm-email', 'page/invoice', 'page/vnc', 'page/profile', 'page/signin', 'page/signin2', 'page/signout',
				'app/mail/inbox', 'app/mail/single',
				'app/tasks'
			]

			setRoutes = function(route) {
				var config, url;
				url = '/' + route;
				var title;
				switch (route){
					case 'page/404': title = '页面未找到'; break;
					case 'page/500': title = '服务器拒绝'; break;
					case 'page/vnc': title = 'VNC连接'; break;
					case 'page/signin': title = '登录'; break;
					case 'page/signin2': title = '比赛登录'; break;
					default: title = '';
				}
				config = {
					title: title,
					url: url,
					templateUrl: 'app/' + route + '.html'
				};
				$stateProvider.state(route, config);
				return $stateProvider;
			};

			routes.forEach(function(route) {
				return setRoutes(route);
			});

			$urlRouterProvider
				.when('/', '/page/signin')
				.otherwise('/page/signin');

			$stateProvider
				.state('dashboard', {
					title: '首页（管理员）',
					url: '/dashboard',
					templateUrl: 'app/dashboard/dashboard.html'
				})
				.state('dashboard2', {
					title: '首页',
					url: '/dashboard2',
					templateUrl: 'app/dashboard/dashboard2.html'
				})
				.state('dashboard3', {
					title: '比赛页面',
					url: '/dashboard3',
					templateUrl: 'app/dashboard/dashboard3.html'
				})
				.state('courses', {
					title: '课程页面',
					url: '/courses',
					templateUrl: 'app/courses/courses.html'
				})
				.state('courses/add', {
					title: '添加课程',
					url: '/courses/add',
					templateUrl: 'app/courses/course-add.html'
				})
				.state('courses/editor', {
					title: '编辑课程',
					url: '/courses/:courseId',
					templateUrl: 'app/courses/course-editor.html'
				})
				.state('courses/learn', {
					title: '课程学习',
					url: '/courses/learn/:courseId',
					templateUrl: 'app/courses/course-learn.html'
				})
				.state('courses/catalog', {
					title: '课程编辑',
					url: '/courses/catalog',
					templateUrl: 'app/courses/courses-catalog.html'
				})
				.state('contests', {
					title: '比赛管理',
					url: '/contests',
					templateUrl: 'app/contests/contests.html'
				})
				.state('contests/add', {
					title: '比赛添加',
					url: '/contests/add',
					templateUrl: 'app/contests/contest-add.html'
				})
				.state('contests/more', {
					title: '比赛详情',
					url: '/contests/more',
					templateUrl: 'app/contests/contest-more.html'
				})
				.state('contests/edit', {
					title: '比赛编辑',
					url: '/contests/edit',
					templateUrl: 'app/contests/contest-edit.html'
				})
				.state('questions', {
					title: '题目页面',
					url: '/questions',
					templateUrl: 'app/questions/questions.html'
				})
				.state('questions/add', {
					title: '题目添加',
					url: '/questions/add',
					templateUrl: 'app/questions/question-add.html',
					resolve: {
						deps: ['$ocLazyLoad', function($ocLazyLoad) {
							return $ocLazyLoad.load([
								'textAngular',
							], {
								insertBefore: '#lazyload_placeholder'
							});
						}]
					}
				})
				.state('questions/edit', {
					title: '题目编辑',
					url: '/questions/edit',
					templateUrl: 'app/questions/question-edit.html',
					resolve: {
						deps: ['$ocLazyLoad', function($ocLazyLoad) {
							return $ocLazyLoad.load([
								'textAngular',
							], {
								insertBefore: '#lazyload_placeholder'
							});
						}]
					}
				})
				.state('questions/publish', {
					title: '题目发布',
					url: '/questions/publish',
					templateUrl: 'app/questions/question-publish.html'
				})
				.state('questions/topo', {
					title: '题目详情',
					url: '/questions/topo',
					templateUrl: 'app/questions/question-topo.html'
				})
                .state('achievements/users', {
                    title: '考核管理用户维度',
                    url: '/achievements/users',
                    templateUrl: 'app/achievements/achievements-users.html'
                })
                .state('achievements/questions', {
                    title: '考核管理题目维度',
                    url: '/achievements/questions',
                    templateUrl: 'app/achievements/achievements-questions.html'
                })
                .state('achievements/user', {
                    title: '考核管理用户',
                    url: '/achievements/user',
                    templateUrl: 'app/achievements/achievements-user.html'
                })
                .state('achievements/localuser', {
                    title: '我的答题',
                    url: '/achievements/localuser',
                    templateUrl: 'app/achievements/achievements-localuser.html'
                })
                .state('achievements/question', {
                    title: '考核管理题目',
                    url: '/achievements/question',
                    templateUrl: 'app/achievements/achievements-question.html'
                })
				.state('users', {
					title: '用户管理',
					url: '/users',
					templateUrl: 'app/users/users.html'
				})
				.state('users/add', {
					title: '用户添加',
					url: '/users/add',
					templateUrl: 'app/users/user-add.html'
				})
				.state('users/edit', {
					title: '用户编辑',
					url: '/users/edit',
					templateUrl: 'app/users/user-edit.html'
				})
				.state('users/publish', {
					title: '用户发布',
					url: '/users/publish',
					templateUrl: 'app/users/user-publish.html'
				})
				.state('users/publish/questions', {
					title: '用户题目发布',
					url: '/users/publish/questions',
					templateUrl: 'app/users/user-questions-publish.html'
				})
				.state('users/changepwd', {
					title: '密码修改',
					url: '/changepwd',
					templateUrl: 'app/users/change-password.html'
				})
				.state('images', {
					title: '镜像管理',
					url: '/images',
					templateUrl: 'app/images/images.html'
				})
				.state('images/vmlist', {
					title: '镜像验证',
					url: '/images/vmlist/:imageId',
					templateUrl: 'app/images/vmlist.html'
				})
				.state('images/edit', {
					title: '镜像编辑',
					url: '/images/edit',
					templateUrl: 'app/images/image-edit.html'
				})
				.state('resources', {
					title: '资源统计',
					url: '/resources',
					templateUrl: 'app/resources/resources.html'
				})
				.state('resources/instance', {
					title: '实例监控',
					url: '/resources/instance',
					templateUrl: 'app/resources/instance.html'
				})
				.state('resources/monitor', {
					title: '资源使用情况',
					url: '/resources/monitor',
					templateUrl: 'app/resources/monitor.html'
				})
				.state('resources/resources', {
					title: '资源统计',
					url: '/resources/resources',
					templateUrl: 'app/resources/resources.html'
				})
				.state('resources/server', {
					title: '服务器管理',
					url: '/resources/server',
					templateUrl: 'app/resources/server.html'
				})
				.state('system', {
					url: '/system',
					templateUrl: 'app/system/system.html'
				})
				.state('system/accounts', {
					title: '用户管理',
					url: '/system/accounts',
					templateUrl: 'app/system/accounts.html'
				})
				.state('system/logs', {
					title: '操作日志',
					url: '/system/logs',
					templateUrl: 'app/system/logs.html'
				})
				.state('system/password', {
					title: '密码修改',
					url: '/system/password',
					templateUrl: 'app/system/password.html'
				})
				.state('form/elements', {
					url: '/form/elements',
					templateUrl: 'app/form/elements.html',
					resolve: {
						deps: ['$ocLazyLoad', function($ocLazyLoad) {
							return $ocLazyLoad.load([
								'textAngular',
							], {
								insertBefore: '#lazyload_placeholder'
							});
						}]
					}
				})
				.state('app/mail/compose', {
					url: '/app/mail/compose',
					templateUrl: 'app/app/mail/compose.html',
					resolve: {
						deps: ['$ocLazyLoad', function($ocLazyLoad) {
							return $ocLazyLoad.load([
								'textAngular',
							], {
								insertBefore: '#lazyload_placeholder'
							});
						}]
					}
				})
				;
		}]);

})();