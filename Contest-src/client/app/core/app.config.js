(function() {
	'use strict';

	angular.module('app')
		.factory('appConfig', [appConfig]);

	function appConfig() {
		var pageTransitionOpts = [{
			name: 'Fade up',
			"class": 'animate-fade-up'
		}, {
			name: 'Scale up',
			"class": 'ainmate-scale-up'
		}, {
			name: 'Slide in from right',
			"class": 'ainmate-slide-in-right'
		}, {
			name: 'Flip Y',
			"class": 'animate-flip-y'
		}];
		var date = new Date();
		var year = date.getFullYear();
		var main = {
			brand: '训练演练系统',
			company: '公安部第三研究所',
			name: 'Lisa',
			server: '/Contest2',
			cloud: 'zstack',
			wsserver: 'ws://202.127.26.199:8082/v1.0',
			upload: '/upload/',
			year: year,
			adminuser: 'admin',
			layout: 'wide', // 'boxed', 'wide'
			isMenuCollapsed: false, // true, false
			fixedHeader: true, // true, false
			fixedSidebar: true, // true, false
			pageTransition: pageTransitionOpts[0], // 0, 1, 2, 3... and build your own
			skin: '12' // 11,12,13,14,15,16; 21,22,23,24,25,26; 31,32,33,34,35,36
		};
		var color = {
			primary: '#1C7EBB',
			success: '#23AE89',
			info: '#2EC1CC',
			infoAlt: '#6A55C2',
			warning: '#FFB61C',
			danger: '#E94B3B',
			gray: '#DCDCDC'
		};

		var logtype = {
			"1": "登录",
			"2": "注销登录",
			"3": "启动题目",
			"4": "关闭题目",
			"5": "启动攻击机",
			"6": "启动实例",
			"7": "销毁实例",
			"8": "发表评论",
			"9": "提交旗帜文件",
			"10": "登录比赛系统",
			"11": "登出比赛系统",
			"20": "创建题目",
			"21": "删除题目",
			"30": "创建用户",
			"31": "删除用户"
		}

		var difficultColor = {
			'1': 'success',
			'2': 'warning',
			'3': 'danger'
		}

		var contestStatus = {
			0: "就绪",
			1: "运行",
			2: "完成"
		}

		var vmPowerStatus = {
			blocked: "不通",
			building: "建立中",
			crashed: "崩溃",
			failed: "失败",
			nostate: "无状态",
			paused: "已暂停",
			running: "运行中",
			shutdown: "关闭",
			shutoff: "关机",
			suspended: "已挂起"
		}

		var vmStatus = {
			shutoff: "关机",
			active: "激活",
			crashed: "崩溃",
			failed: "失败",
			nostate: "无状态",
			paused: "已暂停",
			running: "运行中",
			shutdown: "关闭",
			suspended: "已挂起"
		}

		return {
			pageTransitionOpts: pageTransitionOpts,
			main: main,
			color: color,
			logtype: logtype,
			difficultColor: difficultColor,
			contestStatus: contestStatus,
			vmPowerStatus: vmPowerStatus,
			vmStatus: vmStatus
		}
	}

})();