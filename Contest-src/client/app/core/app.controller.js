(function() {
	'use strict';

	var app = angular.module('app')
		.controller('AppCtrl', ['$scope', '$rootScope', '$document', '$http', '$window', 'appConfig', AppCtrl])
		.factory('HttpInterceptor', ['$q', '$location', '$window', HttpInterceptor]); // overall control

	function AppCtrl($scope, $rootScope, $document, $http, $window, appConfig) {
		$scope.pageTransitionOpts = appConfig.pageTransitionOpts;
		$scope.main = appConfig.main;
		$scope.color = appConfig.color;
		
		var init = function() {
			$http.get(appConfig.main.server + "/services/accounts/current").success(function(data) {
				if (data.exception){
				} else {
					var utype = data.type;
					window.localStorage['uname'] = data.username;
					window.localStorage['displayname'] = data.realname;
					window.localStorage['role'] = (utype == 0) ? 'admin' : (utype == 1 ? 'group' : 'user');
					console.log('刷新后', window.localStorage);
					
					if (window.localStorage['contest'] || data.contest) {
						$("#nav li").hide();
						$("#app").addClass("nav-collapsed-min");
					} else {
						$("#app").removeClass("nav-collapsed-min");
						$("#nav li").show();
					}
					$scope.uname = window.localStorage['uname'];
					$scope.canedit = window.localStorage['uname'] == appConfig.main.adminuser;
					$scope.groupuser = window.localStorage['role'] == 'group';
				}
			}).error(function(data, status, response) {
				console.warn('没有登录或者token失效');
			});
	
			$scope.uname = window.localStorage['uname'];
		};

		$scope.$watch('main', function(newVal, oldVal) {
			$scope.uname = window.localStorage['uname'];
			$scope.canedit = window.localStorage['uname'] == appConfig.main.adminuser;
            $scope.groupuser = window.localStorage['role'] == 'group';
            $scope.localuser = window.localStorage['role'] == 'user';
			$scope.cloud = $scope.main.cloud;
			if(newVal.fixedHeader === false && newVal.fixedSidebar === true) {
				if(oldVal.fixedHeader === false && oldVal.fixedSidebar === false) {
					$scope.main.fixedHeader = true;
					$scope.main.fixedSidebar = true;
				}
				if(oldVal.fixedHeader === true && oldVal.fixedSidebar === true) {
					$scope.main.fixedHeader = false;
					$scope.main.fixedSidebar = false;
				}
			}
			if(newVal.fixedSidebar === true) {
				$scope.main.fixedHeader = true;
			}
			if(newVal.fixedHeader === false) {
				$scope.main.fixedSidebar = false;
			}
		}, true);

		$rootScope.$on("$stateChangeSuccess", function(event, currentRoute, previousRoute) {
			$document.scrollTo(0, 0);
		});
		
		init();
	}

	function HttpInterceptor($q, $location, $window) {
		return {
			responseError: function(err) {
				console.log(err);
				if(err.status == 401) {
					$window.localStorage.clear();
					var goto_page = $location.search().goto_page;
					if (goto_page == undefined){
						goto_page = $location.url();
						goto_page = goto_page.indexOf('sign') == -1 ? '?goto_page=' + encodeURI($location.url()) : '';
					} else {
						goto_page = '?goto_page=' + goto_page;
					}

					$location.url($location.path() == '/page/signin2' ? '/page/signin2' : '/page/signin' + goto_page);
				} 
				return $q.reject(err);
			}
		};
	}

	// 添加对应的 Interceptors
	app.config(['$httpProvider', function($httpProvider) {
		$httpProvider.interceptors.push(HttpInterceptor);
	}]);

	app.directive('errSrc', function() {
		return {
			link: function(scope, element, attrs) {
				element.bind('error', function() {
					if(attrs.src != attrs.errSrc) {
						attrs.$set('src', attrs.errSrc);
					}
				});
			}
		}
	});

	app.directive('uiFileUpload', uiFileUpload);

	// Dependency: https://github.com/grevory/bootstrap-file-input
	function uiFileUpload() {
		return {
			restrict: 'A',
			link: function(scope, ele) {
				ele.bootstrapFileInput();
			}
		}
	}

	app.directive('datePicker', function() {
		return {
			restrict: 'A',
			require: 'ngModel',
			scope: {
				minDate: '@'
			},
			link: function(scope, element, attr, ngModel) {

				element.val(ngModel.$viewValue);

				function onpicking(dp) {
					var date = dp.cal.getNewDateStr();
					scope.$apply(function() {
						ngModel.$setViewValue(date);
					});
				}

				element.bind('click', function() {
					WdatePicker({
						onpicking: onpicking,
						dateFmt: 'yyyy-MM-dd',
						minDate: (scope.minDate || '%y-%M-%d')
					})
				});
			}
		};
	});
})();