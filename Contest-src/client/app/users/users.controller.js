(function () {
    'use strict';
    angular.module('app.users', ['app.service'])
        .controller('UserCtrl', ['$scope', '$location', '$filter', '$uibModal', 'tool', 'link', 'logger', 'appConfig', UserCtrl])
		.controller('UserAddCtrl', ['$scope', '$timeout', '$location', 'tool', 'link', 'logger', UserAddCtrl])
		.controller('UserEditCtrl', ['$scope', '$timeout', '$location', 'tool', 'link', 'logger', UserEditCtrl])
		.controller('UserInfoCtrl', ['$scope', '$uibModalInstance', 'user', UserInfoCtrl])
		.controller('UserPublishCtrl', ['$scope', '$timeout', '$location', '$filter', 'tool', 'link', 'logger', UserPublishCtrl])
		.controller('UserQuesPublishCtrl', ['$scope', '$filter', '$sce', '$location', '$uibModal', '$rootScope', 'appConfig', 'logger', '$window', '$timeout', 'tool', 'link', UserQuesPublishCtrl])
		.controller('ChangePwdCtrl', ['$scope', 'tool', 'link', 'logger', ChangePwdCtrl])
		.controller('UserQuestionCtrl', ['datas', '$scope', '$http', '$uibModalInstance', '$window', '$filter', 'logger', 'appConfig', UserQuestionCtrl]);

    function UserCtrl($scope, $location, $filter, $uibModal, tool, link, logger, appConfig) {
        $scope.canedit = window.localStorage['uname'] == appConfig.main.adminuser;
		if ($scope.canedit) {
            $scope.roleList = [{
                id: undefined,
                name: '全部'
            }, {
                id: 0,
                name: '管理员'
            }, {
                id: 1,
                name: '组用户'
            }, {
                id: 2,
                name: '地方用户'
            }]
		} else {
            $scope.roleList = [{
                id: undefined,
                name: '全部'
            }, {
                id: 1,
                name: '组用户'
            }, {
                id: 2,
                name: '地方用户'
            }]
		}

		$scope.ischoose = function(lb) {
			return $scope.role == lb.id ? 'label-primary' : 'label-default';
		};
		$scope.filterRole = function(df) {
			if($scope.role == df.id ) {
				$scope.role = undefined;
			} else {
				$scope.role = df.id;
			}
			return $scope.search();
		};
        $scope.addUser = function () {
			$location.path("/users/add");
		};
		$scope.editUser = function (user) {
			sessionStorage.editUser = JSON.stringify(user);
			$location.path("/users/edit");
		};

		$scope.labels = function (labs) {
			var ht = [];
			for (var i in labs) {
				ht.push('<span class="label label-primary">');
				ht.push(labs[i]);
				ht.push('</span> ');
			}
			return $sce.trustAsHtml(ht.join(''));
		};

		$scope.select = function (page) {
			var end, start;
			start = (page - 1) * $scope.numPerPage;
			end = start + $scope.numPerPage;
			$scope.currentPage = window.localStorage['currPage'] = page;
			return $scope.currentPageStores = $scope.filteredStores.slice(start, end);
		};

		$scope.onFilterChange = function () {
			$scope.select($scope.currentPage);
			return $scope.row = '';
		};

		$scope.onNumPerPageChange = function () {
			window.localStorage['numPerPage'] = $scope.numPerPage;
			$scope.select(1);
			return $scope.currentPage = 1;
		};

		$scope.onOrderChange = function () {
			$scope.select(1);
			return $scope.currentPage = 1;
		};

		$scope.search = function () {
			window.localStorage['user_userNameKey'] = $scope.searchKeywords;
			$scope.filteredStores = $filter('filter')($scope.stores, {
				'username': $scope.searchKeywords,
				'type': $scope.role
			});
			return $scope.onFilterChange();
		};

		$scope.order = function (rowName) {
			if ($scope.row === rowName) {
				return;
			}
			$scope.row = rowName;
			$scope.filteredStores = $filter('orderBy')($scope.stores, rowName);
			return $scope.onOrderChange();
		};
		$scope.delUser = function (user) {
			var modalInstance = $uibModal.open({
				animation: true, //$scope.animationsEnabled,
				templateUrl: 'confirm.html',
				controller: 'ConfirmCtrl'
			});
			modalInstance.result.then(function () {
				tool.post(link.delUser, {
					id: user.username
				}, function (data) {
					logger.logSuccess("删除成功");
					var idx = -1;
					for (var i = 0, len = $scope.stores.length; i < len; i++) {
						if ($scope.stores[i].username == user.username) {
							idx = i;
							break;
						}
					}
					$scope.stores.splice(idx, 1);
					$scope.search();
				});
			})
		};
		$scope.showUser = function (user) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'userInfo.html',
                controller: 'UserInfoCtrl',
                resolve: {
                    user: function () {
                        return user;
                    }
                }
            });

            modalInstance.result.then(function () {

            }, function () {
            });
        };
		$scope.publishUser = function (user) {
			// sessionStorage.user = JSON.stringify(user);
			$location.path("/users/publish").search({ id: user.id, uname: user.username });
		};
		$scope.publishQues = function (user) {
			$location.path("/users/publish/questions").search({ id: user.id, uname: user.username, type: user.type });
		};
		$scope.questions = function(user) {
			var modalInstance = $uibModal.open({
				animation: true,
				templateUrl: 'userQuestion.html',
				controller: 'UserQuestionCtrl',
				resolve: {
					datas: function() {
						return {
							user: user
						}
					}
				}
			});
		};
		var init = function () {
			$scope.roleName = { 0: '管理员', 1: '组用户', 2: '地方用户' };
			$scope.searchKeywords = window.localStorage['user_userNameKey'] == undefined ? '' : window.localStorage['user_userNameKey'];
			$scope.filteredStores = [];
			$scope.row = '';
			$scope.numPerPageOpt = [3, 5, 10, 20];
			var numPerPage = window.localStorage['numPerPage'];
        	$scope.numPerPage = numPerPage == undefined ? $scope.numPerPageOpt[2] : parseInt(numPerPage);
            $scope.currentPage = 1;
			$scope.currentPageStores = [];
			if ($scope.canedit) {
                tool.get(link.getUsers, {}, function (data) {
                    var currentPage = window.localStorage['currPage'];
                    if(currentPage != undefined) $scope.currentPage = currentPage;
    				$scope.stores = data.users;
                    $scope.stores.push({
                        realname: 'admin',
                        type: 0,
                        username: appConfig.main.adminuser
                    });
                    $scope.search();
                });
			} else {
                tool.get(link.userPublish + "availableusers", {}, function (data) {
                	var currentPage = window.localStorage['currPage'];
                    if(currentPage != undefined) $scope.currentPage = currentPage;
    				$scope.stores = data.available_users;
                    $scope.search();
                });
			}
		};
		init();
    }
	function UserAddCtrl($scope, $timeout, $location, tool, link, logger) {
		var init = function () {
			$scope.user = {};
			$scope.confirmPwd = "";
			$scope.uname = window.localStorage['uname'];
			$("select").select2({
				minimumResultsForSearch: Infinity
			});
		};
		init();
		$scope.back = function () {
			$location.path("/users");
		};
		var checkForm = function () {
			if (!$scope.user.username) {
				logger.logError("用户名不能为空");
				return false;
			} else if (!$scope.user.realname) {
				logger.logError("真实姓名不能为空");
				return false;
			} else if (!$scope.user.password) {
				logger.logError("密码不能为空");
				return false;
			} else if ($scope.user.password != $scope.confirmPwd) {
				logger.logError("两次密码输入不一致");
				return false;
			} else if (!$scope.user.type) {
				logger.logError("请选择用户类型");
				return false;
			}
			return true;
		};

		$scope.save = function () {
			if (checkForm()) {
				tool.post(link.addUser, $scope.user, function (data) {
					if(data && data['exception']) {
						logger.logError(data['exception']);
					} else if (data && data.username) {
						logger.logSuccess("提交成功");
						$timeout(function () {
							$scope.back();
						}, 2000);
					}
				})
			}
		};
	}
	function UserEditCtrl($scope, $timeout, $location, tool, link, logger) {
		var init = function () {
			$scope.user = JSON.parse(sessionStorage.editUser);
			$scope.confirmPwd = "";
			$scope.roleList = [{ value: 1, text: '组用户' }, { value: 2, text: '地方用户' }];
			$("select").select2({
				minimumResultsForSearch: Infinity
			});
		};
		init();
		$scope.back = function () {
			$location.path("/users");
		};
		var checkForm = function () {
			if (!$scope.user.username) {
				logger.logError("用户名不能为空");
				return false;
			} else if (!$scope.user.realname) {
				logger.logError("真实姓名不能为空");
				return false;
			} else if (!$scope.user.newpassword) {
				logger.logError("密码不能为空");
				return false;
			} else if ($scope.user.newpassword != $scope.confirmPwd) {
				logger.logError("两次密码输入不一致");
				return false;
			} else if (!$scope.user.type) {
				logger.logError("请选择用户类型");
				return false;
			}
			$scope.user.oldpassword = $scope.user.password;
			return true;
		};

		$scope.save = function () {
			if (checkForm()) {
				tool.post(link.editUser, $scope.user, function (data) {
					if (data && data.username) {
						logger.logSuccess("更新成功");
						$timeout(function () {
							$scope.back();
						}, 2000);
					}
				});
			}
		};
	}
	function UserInfoCtrl($scope, $uibModalInstance, user) {
		$scope.user = user;
        $scope.ok = function () {
            $uibModalInstance.dismiss("cancel");
        };
	}
	function UserPublishCtrl($scope, $timeout, $location, $filter, tool, link, logger) {
		$scope.ischoose = function(lb) {
			return $scope.role == lb.id ? 'label-primary' : 'label-default';
		};

		$scope.filterRole = function(df) {
			if($scope.role == df.id ) {
				$scope.role = undefined;
			} else {
				$scope.role = df.id;
			}
			return $scope.search();
		};

		$scope.labels = function (labs) {
			var ht = [];
			for (var i in labs) {
				ht.push('<span class="label label-primary">');
				ht.push(labs[i]);
				ht.push('</span> ');
			}
			return $sce.trustAsHtml(ht.join(''));
		};

		$scope.select = function (page) {
			var end, start;
			var numPerPage = $scope.numPerPage == '全部' ? $scope.stores.length:$scope.numPerPage;
			start = (page - 1) * numPerPage;
			end = start + numPerPage;
			$scope.currentPageStores = $scope.filteredStores.slice(start, end);
			$scope.checkIsAllChecked();
		};

		$scope.onFilterChange = function () {
			$scope.select(1);
			$scope.currentPage = 1;
			return $scope.row = '';
		};

		$scope.onNumPerPageChange = function () {
			$scope.select(1);
			return $scope.currentPage = 1;
		};

		$scope.onOrderChange = function () {
			$scope.select(1);
			return $scope.currentPage = 1;
		};

		$scope.order = function (rowName) {
			if ($scope.row === rowName) {
				return;
			}
			$scope.row = rowName;
			$scope.filteredStores = $filter('orderBy')($scope.stores, rowName);
			return $scope.onOrderChange();
		};

		$scope.search = function () {
			window.localStorage['user_userNameKey'] = $scope.searchKeywords;
			$scope.filteredStores = $filter('filter')($scope.stores, {
				'username': $scope.searchKeywords,
				'type': $scope.role
			});
			return $scope.onFilterChange();
		};

		var init = function () {
			$scope.searchKeywords = window.localStorage['user_userNameKey'] == undefined ? '' : window.localStorage['user_userNameKey'];
			$scope.userId = $location.search().uname;
			$scope.username = $location.search().uname;
			$scope.roleName = { 1: '组用户', 2: '地方用户' };
			$scope.filteredStores = [];
			$scope.row = '';
			$scope.currentPage = 1;
			$scope.currentPageStores = [];
			tool.get(link.allUser, {}, function (data) {
				if (data && data.users) {
					$scope.users = $filter("filter")(data.users, { type: 2 }, true);
					tool.get(link.userPublish + $scope.userId + "/groupusers", {}, function (res) {
						if (res && res.groupusers) {
							if (res.groupusers.length > 0) {
								$scope.isAllChecked = $scope.users.length == res.groupusers.length;
								angular.forEach($scope.users, function (u) {
									for (var i = 0, len = res.groupusers.length; i < len; i++) {
										if (u.username == res.groupusers[i].username) {
											u.checked = true;
											u.hasPublish = true;
											break;
										}
									}
								})
							}
							$scope.stores = $scope.users;
							$scope.filteredStores = $scope.stores;
							$scope.numPerPageOpt = [3, 5, 10, 20, "全部"];
							$scope.numPerPage = $scope.numPerPageOpt[4];
							$scope.search();

							return $scope.select($scope.currentPage);
						} else {
							logger.logError("获取已发布用户失败");
						}
					})
				} else {
					logger.logError("获取用户列表失败");
				}
			});
		};
		init();
		$scope.publishUser = function () {
			var subUsers = [];
			angular.forEach($scope.users, function (u) {
				if (u.checked)
					subUsers.push(u.username);
			})
			tool.post(link.userPublish + $scope.userId + "/groupusers", { groupusers: subUsers }, function (data) {
				if (data && data.groupusers) {
					logger.logSuccess("发布成功");
					$timeout(function () {
						$location.path("/users");
					}, 2000);
				} else {
					logger.logError("发布用户失败");
				}
			})
		};

		$scope.checkAll = function() {
			if ($scope.currentPageStores.length == 0){
				$scope.isAllChecked = false;
			}
			angular.forEach($scope.currentPageStores, function(u) {
				u.checked = $scope.isAllChecked;
			});
		};

		$scope.checkIsAllChecked = function() {
			var checked_users = 0;
			angular.forEach($scope.currentPageStores, function(u) {
				if(u.checked)
					checked_users += 1;
			});
			$scope.isAllChecked = checked_users == $scope.currentPageStores.length && $scope.currentPageStores.length != 0;
		};

		$scope.back = function () {
			$location.path("/users");
		};
	}

	function UserQuesPublishCtrl($scope, $filter, $sce, $location, $uibModal, $rootScope, appConfig, logger, $window, $timeout, tool, link) {

		$scope.labelList = [];
		$scope.categoryList = [];

		var formatCateroty = function(labellist){
			var list = [];
			list.push({name: '全部', val:undefined});
			for (var i in labellist){
				list.push({name: labellist[i], val:labellist[i]});
			}
			return list;
		};

		$scope.difficultList = [{
			name: '全部',
			val: undefined
		}, {
			name: '低',
			val: 1
		}, {
			name: '中',
			val: 2
		}, {
			name: '高',
			val: 3
		}];

		$scope.ischoose = function(lb) {
			return $scope.label == lb.val ? 'label-primary' : 'label-default';
		}
		$scope.ischoose2 = function(df) {
			return $scope.difficult == df.val ? 'label-primary' : 'label-default';
		}
		$scope.filterLabel = function(lb) {
			if($scope.label == lb.val) {
				$scope.label = undefined;
			} else {
				$scope.label = lb.val;
			}
			return $scope.search();
		};

		$scope.filterDifficult = function(df) {
			if($scope.difficult == df.val) {
				$scope.difficult = undefined;
			} else {
				$scope.difficult = df.val;
			}
			return $scope.search();
		};

		$scope.labels = function(labs) {
			var ht = [];
			for(var i in labs) {
				ht.push('<span class="label label-primary">');
				ht.push(labs[i]);
				ht.push('</span> ');
			}
			return $sce.trustAsHtml(ht.join(''));
		};

		$scope.diffs = function(d) {
			var ht = [];
			for(var i in $scope.difficultList) {
				if(i == 0) continue;
				if($scope.difficultList[i].val == d) {
					ht.push('<span class="label label-primary">');
					ht.push($scope.difficultList[i].name);
					ht.push('</span> ');
				}
			}
			return $sce.trustAsHtml(ht.join(''));
		}

		$scope.select = function(page) {
			var end, start;
			var numPerPage = $scope.numPerPage == '全部' ? $scope.stores.length:$scope.numPerPage;
			start = (page - 1) * numPerPage;
			end = start + numPerPage;
			$scope.currentPageStores = $scope.filteredStores.slice(start, end);
			$scope.checkIsAllChecked();
		};

		$scope.onFilterChange = function() {
			$scope.select(1);
			$scope.currentPage = 1;
			return $scope.row = '';
		};

		$scope.onNumPerPageChange = function() {
			$scope.select(1);
			return $scope.currentPage = 1;
		};

		$scope.onOrderChange = function() {
			$scope.select(1);
			return $scope.currentPage = 1;
		};

		$scope.search = function() {
			window.localStorage['user_questionNameKey'] = $scope.searchKeywords;
			$scope.filteredStores = $filter('filter')($scope.stores, {
				"summary": $scope.searchKeywords,
				'categories': $scope.label,
				'difficult': $scope.difficult
			});
			//			$scope.filteredStores = $filter('filter')($scope.stores, $scope.searchKeywords);
			return $scope.onFilterChange();
		};

		$scope.order = function(rowName) {
			if($scope.row === rowName) {
				return;
			}
			$scope.row = rowName;
			$scope.filteredStores = $filter('orderBy')($scope.stores, rowName);
			return $scope.onOrderChange();
		};

		$scope.questionTopo = function(ques) {
			sessionStorage.questionSummary = ques.summary;
			sessionStorage.questionId = ques.id;
			$location.path("/questions/topo");
		};

		$scope.users = function(ques) {
			var modalInstance = $uibModal.open({
				animation: true,
				templateUrl: 'questionUser.html',
				controller: 'QuestionUserCtrl',
				resolve: {
					datas: function() {
						return {
							ques: ques
						}
					}
				}
			});
		};

		var init = function () {
			$scope.searchKeywords = window.localStorage['user_questionNameKey'] == undefined ? '' : window.localStorage['user_questionNameKey'];
			$scope.filteredStores = [];
			$scope.row = '';
			$scope.currentPage = 1;
			$scope.currentPageStores = [];
			// $scope.question = JSON.parse(sessionStorage.question);
			$scope.userId = $location.search().uname;
			$scope.username = $location.search().uname;
			$scope.roleName = $location.search().type == 1 ? '组用户' : '地方用户';
			// $scope.roleName = { 1: '组用户', 2: '地方用户' };
			tool.get("/services/questions/category/list", {}, function (data) {
				$scope.labelList = data.question_categories;
				$scope.categoryList = formatCateroty($scope.labelList);
			});
			tool.get(link.allQues, {}, function (data) {
				if (data && data.questions) {
					$scope.questions = data.questions;
					tool.get(link.userPublish + $scope.userId + "/questions", {}, function (res) {
						if (res && res.questions) {
							if (res.questions.length > 0) {
								$scope.isAllChecked = $scope.questions.length == res.questions.length;
								angular.forEach($scope.questions, function (u) {
									for (var i = 0, len = res.questions.length; i < len; i++) {
										if (u.id == res.questions[i].id) {
											u.checked = true;
											u.hasPublish = true;
											break;
										}
									}
								})
							}
							$scope.stores = $scope.questions;
							$scope.filteredStores = $scope.stores;
							$scope.numPerPageOpt = [3, 5, 10, 20, "全部"];
							$scope.numPerPage = $scope.numPerPageOpt[4];
							$scope.search();
							return $scope.select($scope.currentPage);
						} else {
							logger.logError("获取已发布题目失败");
						}
					})
				} else {
					logger.logError("获取题目列表失败");
				}
			});
		};
		init();
		$scope.publishQues = function () {
			var subQuestions = [];
			angular.forEach($scope.questions, function (u) {
				if (u.checked)
					subQuestions.push(u.id);
			})
			tool.post(link.userPublish + $scope.userId + "/questions", { questions: subQuestions }, function (data) {
				if (data) {
					logger.logSuccess("发布成功");
					$timeout(function () {
						$location.path("/users");
					}, 2000);
				}
			})
		};
		$scope.checkAll = function() {
			if ($scope.currentPageStores.length == 0){
				$scope.isAllChecked = false;
			}
			angular.forEach($scope.currentPageStores, function(u) {
				u.checked = $scope.isAllChecked;
			});
		};

		$scope.checkIsAllChecked = function() {
			var checked_users = 0;
			angular.forEach($scope.currentPageStores, function(u) {
				if(u.checked)
					checked_users += 1;
			});
			$scope.isAllChecked = checked_users == $scope.currentPageStores.length && $scope.currentPageStores.length != 0;
		};
		$scope.back = function () {
			$location.path("/users");
		};
	}
	function ChangePwdCtrl($scope, tool, link, logger) {
		var init = function () {
			$scope.oldPwd = "";
			$scope.pwd = "";
			$scope.confirmPwd = "";
		};
		init();
		$scope.save = function () {
			if (!$scope.oldPwd) {
				logger.logError("旧密码不能为空");
				return;
			} else if (!$scope.pwd) {
				logger.logError("新密码不能为空");
				return;
			} else if ($scope.pwd != $scope.confirmPwd) {
				logger.logError("两次新密码输入不一致");
				return;
			}
			tool.post(link.changePwd + localStorage.uname, { oldpassword: $scope.oldPwd, newpassword: $scope.pwd }, function (data) {
				if(data){
					if(data.exception){
						logger.logError(data.exception);
					}else{
						logger.logSuccess("密码修改成功");
					}
				}
			})
		};
	}
	function UserQuestionCtrl(datas, $scope, $http, $uibModalInstance, $window, $filter, logger, appConfig) {
		layer.load(1, {
			shade: [0.3, '#fff'] //0.1透明度的白色背景
		});
		$http.get(appConfig.main.server + "/services/questions/byuser/" + datas.user.username).success(function(data) {
			$scope.questions = data;
			layer.closeAll();
		});

		$scope.destroy = function(q) {
			layer.load(1, {
				shade: [0.3, '#fff'] //0.1透明度的白色背景
			});
			$http.post(appConfig.main.server + "/services/questions/stopall", {
				question: q.question_id,
				username: datas.user.username
			}).success(function(data) {
				layer.closeAll();
				if(data['exception']) {
					logger.logError(data['exception']);
				} else {
					var index = $scope.questions.indexOf(q);
					$scope.questions.splice(index, 1);
				}
			}).error(function(err) {
				layer.closeAll();
				logger.logError("销毁失败");
			});
		}
		$scope.destroyAll = function() {
			layer.load(1, {
				shade: [0.3, '#fff'] //0.1透明度的白色背景
			});
			var questions = [];
			for(var i in $scope.questions) {
				questions.push($scope.questions[i].question_id);
			}
			$http.post(appConfig.main.server + "/services/questions/stopall", {
				username: datas.user.username,
				questions: questions
			}).success(function(data) {
				layer.closeAll();
				if(data['exception']) {
					logger.logError(data['exception']);
				} else {
					$scope.questions = [];
				}
			}).error(function(err) {
				layer.closeAll();
				logger.logError("销毁失败");
			});
		}
		$scope.cancel = function() {
			$uibModalInstance.dismiss("cancel");
		};
	}
})();