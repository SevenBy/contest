(function () {
	'use strict';

	angular.module('app.resources')
		.factory("chartService", [chartService])
		.controller('ResourcesCtrl', ['$scope', '$rootScope', '$filter', '$http', '$location', 'appConfig', 'logger', 'chartService', '$uibModal', ResourcesCtrl])
		.controller('InstanceCtrl', ['$scope', '$http', '$location', '$interval', 'appConfig', 'logger', InstanceCtrl])
		.controller('ServerCtrl', ['$scope', '$rootScope', '$filter', '$http', '$location', 'appConfig', 'logger', 'chartService', ServerCtrl])
		.controller('MonitorCtrl', ['$scope', '$rootScope', '$filter', '$http', '$location', 'appConfig', 'logger', 'chartService', MonitorCtrl])
		.controller('ServerListCtrl', ['datas', '$scope', '$http', '$uibModalInstance', '$window', '$filter', 'logger', 'appConfig', ServerListCtrl])
		.filter('fTime', function () {
			return function (input) {
				var sec = parseInt(input * 3600);
				var res = "";
				res += sec > 3600 * 24 ? Math.floor(sec / (3600 * 24)) + '天' : '';
				sec = sec % (3600 * 24);
				res += sec > 3600 ? Math.floor(sec / 3600) + '小时' : '';
				sec = sec % 3600;
				res += sec > 60 ? Math.floor(sec / 60) + '分钟' : '';
				sec = sec % 60;
				res += sec + '秒';
				return res;
			}
		})
		.filter('fSizeMB', function () {
			return function (input) {
				var sz = parseInt(input);
				var res = "";
				if (sz >= 1024 * 1024) res += parseFloat(sz / (1024 * 1024)).toFixed(1) + 'TB';
				else if (sz >= 1024) res += Math.round(sz / 1024) + 'GB';
				else if (sz > 0) res += sz + 'MB';
				else res += '0字节';
				return res;
			}
		})
		.filter('fSizeGB', function () {
			return function (input) {
				var sz = parseInt(input);
				var res = "";
				if (sz >= 1024) res += parseFloat(sz / 1024).toFixed(1) + 'TB';
				else if (sz > 0) res += sz + 'GB';
				else res += '0字节';
				return res;
			}
		});

	function chartService() {
		return {
			pieChartModel: function (title, tip, formatter, dataList) {
				return {
					title: {
						text: title,
						subtext: tip,
						x: 'center'
					},
					tooltip: {
						trigger: 'item',
						formatter: formatter
					},
					toolbox: {
						show: false
					},
					calculable: true,
					series: [{
						type: 'pie',
						radius: '55%',
						center: ['50%', '60%'],
						itemStyle: {
							normal: {
								label: {
									show: false
								},
								labelLine: {
									show: false
								}
							},
							emphasis: {
								label: {
									show: false
								},
								labelLine: {
									show: false
								}
							}
						},
						data: dataList
					}]
				}
			}
		}
	}

	function InstanceCtrl($scope, $http, $location, $interval, appConfig, logger) {
		$scope.themes = ['vintage', 'default'];
		$scope.baseConfig = {
			theme: 'vintage',
			dataLoaded: true
		};
		$scope.themeChanged = function (tn) {
			//$scope.theme = tn;
		};
		$scope.lineConfig = {
			theme: 'vintage',
			dataLoaded: true
		};
		/*$scope.$watch('theme', function (v) {
		 $scope.lineConfig.theme = v;
		 });*/


		$scope.line1 = {};
		$scope.line2 = {};
		$scope.line3 = {};
		$scope.timespan = 30;
		$scope.width = 100;
		$scope.timespanList = [{
			value: 10,
			text: '10秒'
		}, {
			value: 30,
			text: '30秒'
		}, {
			value: 60,
			text: '60秒'
		}, {
			value: 600,
			text: '10分钟'
		}, {
			value: 3600,
			text: '1小时'
		}];
		$scope.host = {};
		$scope.user = {};
		$scope.question = {};
		$scope.hosts = [];
		$scope.refresh = function () {
			if ($location.path() != "/resources/instance") {
				$interval.cancel(timer);
				return;
			}
			$http.get(appConfig.main.server + "/services/host/monitor/" + $scope.host.id + "/" + $scope.timespan + "/1", {}).success(function (data) {
				if (data.meter != undefined && data.meter != null) {
					// cpu_util
					var info = data.meter.cpu_util.pop();
					var date = new Date(info.period_end * 1000);
					if ($scope.line1.options.xAxis[0].data.length >= $scope.width) {
						$scope.line1.options.xAxis[0].data.shift();
						$scope.line1.options.series[0].data.shift();
					}
					$scope.line1.options.xAxis[0].data.push([date.getHours(), date.getMinutes(), date.getSeconds()].join(':'));
					$scope.line1.options.series[0].data.push(info.avg);

					// disk_read_rate disk_write_rate
					info = data.meter.disk_read_rate.pop();
					date = new Date(info.period_end * 1000);
					if ($scope.line2.options.xAxis[0].data.length >= $scope.width) {
						$scope.line2.options.xAxis[0].data.shift();
						$scope.line2.options.series[0].data.shift();
						$scope.line2.options.series[1].data.shift();
					}
					$scope.line2.options.xAxis[0].data.push([date.getHours(), date.getMinutes(), date.getSeconds()].join(':'));
					$scope.line2.options.series[0].data.push(info.sum);
					info = data.meter.disk_write_rate.pop();
					$scope.line2.options.series[1].data.push(info.sum);

					// nic_incoming_rate nic_outgoing_rate
					info = data.meter.nic_incoming_rate.pop();
					date = new Date(info.period_end * 1000);
					if ($scope.line3.options.xAxis[0].data.length >= $scope.width) {
						$scope.line3.options.xAxis[0].data.shift();
						$scope.line3.options.series[0].data.shift();
						$scope.line3.options.series[1].data.shift();
					}
					$scope.line3.options.xAxis[0].data.push([date.getHours(), date.getMinutes(), date.getSeconds()].join(':'));
					$scope.line3.options.series[0].data.push(info.sum);
					info = data.meter.nic_outgoing_rate.pop();
					$scope.line3.options.series[1].data.push(info.sum);
				}

			}).error(function (err) {
				console.info(err);
				logger.logError("服务器错误");
			});
		};
		$scope.timespanChange = function () {
			$interval.cancel(timer);
			$scope.line1.options.xAxis[0].data = [];
			$scope.line1.options.series[0].data = [];
			$scope.line2.options.xAxis[0].data = [];
			$scope.line2.options.series[0].data = [];
			$scope.line2.options.series[1].data = [];
			$scope.line3.options.xAxis[0].data = [];
			$scope.line3.options.series[0].data = [];
			$scope.line3.options.series[1].data = [];
			if ($scope.host == undefined){
				return;
			}
			$http.get(appConfig.main.server + "/services/host/monitor/" + $scope.host.id + "/" + $scope.timespan + "/" + $scope.width, {}).success(function (data) {
				if (data.meter != undefined && data.meter != null) {
					var info;
					var date;
					data.meter.cpu_util.pop();
					data.meter.disk_read_rate.pop();
					data.meter.disk_write_rate.pop();
					data.meter.nic_incoming_rate.pop();
					data.meter.nic_outgoing_rate.pop();
					for (var i in data.meter.cpu_util) {
						// cpu_util
						info = data.meter.cpu_util[i];
						date = new Date(info.period_end * 1000);
						$scope.line1.options.xAxis[0].data.push([date.getHours(), date.getMinutes(), date.getSeconds()].join(':'));
						$scope.line1.options.series[0].data.push(info.avg);

						// disk_read_rate disk_write_rate
						info = data.meter.disk_read_rate[i];
						date = new Date(info.period_end * 1000);
						$scope.line2.options.xAxis[0].data.push([date.getHours(), date.getMinutes(), date.getSeconds()].join(':'));
						$scope.line2.options.series[0].data.push(info.sum);
						info = data.meter.disk_write_rate[i];
						$scope.line2.options.series[1].data.push(info.sum);

						// nic_incoming_rate nic_outgoing_rate
						info = data.meter.nic_incoming_rate[i];
						date = new Date(info.period_end * 1000);
						$scope.line3.options.xAxis[0].data.push([date.getHours(), date.getMinutes(), date.getSeconds()].join(':'));
						$scope.line3.options.series[0].data.push(info.sum);
						info = data.meter.nic_outgoing_rate[i];
						$scope.line3.options.series[1].data.push(info.sum);
					}
				}
				$scope.refresh();
			}).error(function (err) {
				console.info(err);
				logger.logError("服务器错误");
			});
			timer = $interval(function () {
				$scope.refresh();
			}, $scope.timespan * 1000);
		};
		var timer;
		var init = function () {
			$scope.ordinaryUser = window.localStorage['role'] == 'user';
			$interval.cancel(timer);
			$http.get(appConfig.main.server + "/services/questions/servers").success(function (data) {
				for (var i in data.servers) {
					if (data.servers[i].question_name == '')
						data.servers[i].question_name = '未知题目';
					if (!$scope.user[data.servers[i].username]) {
						$scope.user[data.servers[i].username] = {}
					}
					if (!$scope.user[data.servers[i].username][data.servers[i].question_name]) {
						$scope.user[data.servers[i].username][data.servers[i].question_name] = [];
					}
					$scope.user[data.servers[i].username][data.servers[i].question_name].push(data.servers[i]);
				}
				$scope.question = $scope.user[window.localStorage['uname']];
			}).error(function (err) {
				console.info(err);
				logger.logError("服务器错误");
			});

			function format(value) {
				return value > 1000000 ? (value / 1000000).toFixed(1) + 'MB/s' :
					value > 1000 ? (value / 1000).toFixed(1) + 'KB/s' : (value / 1).toFixed(0) + 'B/s';
			}

			function formatTooltip(params, ticket, callback) {
				var res = params[0].name;
				for (var i in params) {
					res += "<div><span style='color: " + params[i].color + "'>● <span/> <span style='color: #ffffff'>" + params[i].seriesName + ": " + format(params[i].data) + '</div>';
				}
				return res;
			}

			var colors1 = ['#3280a8'];
			var colors2 = ['#64d45c', '#f85058'];
			$scope.line1.options = {
				color: colors1,
				tooltip: {
					trigger: 'axis',
					formatter: '{b}<br />{a}: {c} %'
				},
				legend: {
					data: ['cpu利用率']
				},
				toolbox: {
					show: true,
					feature: {
						saveAsImage: {
							show: true,
							title: "保存为图片"
						}
					}
				},
				calculable: true,
				xAxis: [{
					type: 'category',
					boundaryGap: false,
					data: []
				}],
				yAxis: [{
					type: 'value',
					axisLabel: {
						formatter: '{value} %'
					}
				}],
				series: [{
					name: 'cpu利用率',
					type: 'line',
					data: []
				}]
			};
			$scope.line2.options = {
				color: colors2,
				tooltip: {
					trigger: 'axis',
					formatter: formatTooltip
				},
				legend: {
					data: ['磁盘读速率', '磁盘写速率']
				},
				toolbox: {
					show: true,
					feature: {
						saveAsImage: {
							show: true,
							title: "保存为图片"
						}
					}
				},
				calculable: true,
				xAxis: [{
					type: 'category',
					boundaryGap: false,
					data: []
				}],
				yAxis: [{
					type: 'value',
					axisLabel: {
						formatter: format
					}
				}],
				series: [{
					name: '磁盘读速率',
					type: 'line',
					// stack: 'Sum',
					data: []
				}, {
					name: '磁盘写速率',
					type: 'line',
					// stack: 'Sum',
					data: []
				}]
			};
			$scope.line3.options = {
				color: colors2,
				tooltip: {
					trigger: 'axis',
					formatter: formatTooltip
				},
				legend: {
					data: ['内网接收速率', '内网发送速率']
				},
				toolbox: {
					show: true,
					feature: {
						saveAsImage: {
							show: true,
							title: "保存为图片"
						}
					}
				},
				calculable: true,
				xAxis: [{
					type: 'category',
					boundaryGap: false,
					data: []
				}],
				yAxis: [{
					type: 'value',
					axisLabel: {
						formatter: format
					}
				}],
				series: [{
					name: '内网接收速率',
					type: 'line',
					// stack: 'Sum',
					data: []
				}, {
					name: '内网发送速率',
					type: 'line',
					// stack: 'Sum',
					data: []
				}]
			};
		};
		init();
	}

	function kmgfmt(v) {
		if (v >= 1024) {
			return parseFloat(v / 1024).toFixed(1) + 'GB';
		} else if (v > 0) {
			return v + 'MB';
		} else {
			return '0字节';
		}
	}

	function gtfmt(v) {
		if (v >= 1024) {
			return parseFloat(v / 1024).toFixed(1) + 'TB';
		} else if (v > 0) {
			return v + 'GB';
		} else {
			return '0字节';
		}
	}

	function MonitorCtrl($scope, $rootScope, $filter, $http, $location, appConfig, logger, chartService) {
		$scope.pie1 = {};
		$scope.pie2 = {};
		$scope.pie3 = {};
		$scope.pie4 = {};
		$http.get(appConfig.main.server + "/services/dash2/absolutelimits").success(function (data) {
			if (data.absolutelimits) {
				$scope.pie1.options = chartService.pieChartModel("题目", data.absolutelimits.maxTotalQuestions + ' 中的 ' + data.absolutelimits.totalQuestionsUsed + ' 已使用', '{b}<br/>实例：{c}', [{
					value: data.absolutelimits.maxTotalQuestions - data.absolutelimits.totalQuestionsUsed,
					name: '未使用'
				}, {
					value: data.absolutelimits.totalQuestionsUsed,
					name: '已使用'
				}]);
				// $scope.pie1.refresh();
				$scope.pie2.options = chartService.pieChartModel("实例", data.absolutelimits.maxTotalInstances + ' 中的 ' + data.absolutelimits.totalInstancesUsed + ' 已使用', '{b}<br/>实例：{c}', [{
					value: data.absolutelimits.maxTotalInstances - data.absolutelimits.totalInstancesUsed,
					name: '未使用'
				}, {
					value: data.absolutelimits.totalInstancesUsed,
					name: '已使用'
				}]);
				$scope.pie3.options = chartService.pieChartModel("虚拟内核", data.absolutelimits.maxTotalCores + ' 中的 ' + data.absolutelimits.totalCoresUsed + ' 已使用', '{b}<br/>虚拟内核：{c}', [{
					value: data.absolutelimits.maxTotalCores - data.absolutelimits.totalCoresUsed,
					name: '未使用'
				}, {
					value: data.absolutelimits.totalCoresUsed,
					name: '已使用'
				}]);
				$scope.pie4.options = chartService.pieChartModel("内存", kmgfmt(data.absolutelimits.maxTotalRAMSize) + ' 中的 ' + kmgfmt(data.absolutelimits.totalRAMUsed) + ' 已使用', function (v) {
					return v.data.name + '<br/>内存：' + kmgfmt(v.data.value)
				}, [{
					value: data.absolutelimits.maxTotalRAMSize - data.absolutelimits.totalRAMUsed,
					name: '未使用'
				}, {
					value: data.absolutelimits.totalRAMUsed,
					name: '已使用'
				}]);
			}
		}).error(function (err) {
			layer.closeAll();
			logger.logError("内部错误");
		});
		$scope.filteredStores = [];
		$scope.select = function (page) {
			var end, start;
			start = (page - 1) * $scope.numPerPage;
			end = start + $scope.numPerPage;
			return $scope.currentPageStores = $scope.filteredStores.slice(start, end);
		};

		$scope.onFilterChange = function () {
			$scope.select(1);
			$scope.currentPage = 1;
			return $scope.row = '';
		};

		$scope.onNumPerPageChange = function () {
			$scope.select(1);
			return $scope.currentPage = 1;
		};

		$scope.onOrderChange = function () {
			$scope.select(1);
			return $scope.currentPage = 1;
		};

		$scope.order = function (rowName) {
			if ($scope.row === rowName) {
				return;
			}
			$scope.row = rowName;
			$scope.filteredStores = $filter('orderBy')($scope.stores, rowName);
			return $scope.onOrderChange();
		};
		$scope.search1 = function () {
			$scope.filteredStores = [];
			$scope.row = '';
			$scope.numPerPageOpt = [3, 5, 10, 20];
			$scope.numPerPage = $scope.numPerPageOpt[2];
			$scope.currentPage = 1;
			$scope.active_memory_mb = 0;
			$scope.currentPageStores = [];
			$rootScope.$broadcast('preloader:active');
			var nowstamp = Date.parse(new Date());
			if (nowstamp + 1000 < $scope.start) {
				$rootScope.$broadcast('preloader:hide');
				logger.logError("无效的时间范围，你正在查询未来时间段的数据。");
				return;
			}

			$http.get(appConfig.main.server + "/services/dash2/usage?start=" + moment($scope.start).format('YYYY-MM-DD') + "&end=" + moment($scope.end).format('YYYY-MM-DD'), {}).success(function (data) {
				$rootScope.$broadcast('preloader:hide');
				if (data && data.usage) {
					$scope.server_usages = data.usage.server_usages;
					angular.forEach(data.usage.server_usages, function (d) {
						$scope.active_memory_mb += d.memory_mb;
					})
					//$scope.active_memory_mb = 0;
					//for(var i=0;i<data.usage.server_usages.length;i++){
					//	$scope.active_memory_mb += data.usage.server_usages[i].memory_mb;
					//}
					$scope.total_local_gb_usage = data.usage.total_local_gb_usage;
					$scope.total_memory_mb_usage = data.usage.total_memory_mb_usage;
					$scope.total_vcpus_usage = data.usage.total_vcpus_usage;

					$scope.stores = data.usage.server_usages;
					$scope.filteredStores = $scope.stores;
					$scope.currentPage = 1;
					$scope.row = '';
					return $scope.select($scope.currentPage);
				}
			}).error(function (err) {
				$rootScope.$broadcast('preloader:hide');
				console.info(err);
			});
			// $scope.logtype = appConfig.logtype;
			$http.get(appConfig.main.server + '/services/logtype/list').success(function (data) {
				$scope.logtypes = {};
				for (var i in data) {
					$scope.logtypes[data[i].value] = data[i].name;
				}
			});
			$http.post(appConfig.main.server + '/services/log/list', {
				rows: 7,
				sort: 'logtime',
				order: 'desc'
			}).success(function (data) {
				$scope.logs = data.content;
			});
		}
		$scope.questionTopo = function (usage) {
			sessionStorage.questionSummary = usage.question_name;
			sessionStorage.questionId = usage.question_id;
			$location.path("/questions/topo");
		}

		var init = function () {
			$scope.end = new Date().getTime();
			$scope.start = $scope.end - (3600000 * 24 * 30);
			$('.daterange span').html(moment($scope.start).format('YYYY-MM-DD') + ' - ' + moment($scope.end).format('YYYY-MM-DD'));
			$(".daterange").daterangepicker({
				timePicker: false,
				// timePickerIncrement: 30,
				"startDate": $scope.start,
				"endDate": $scope.end //,
				// "minDate": new Date()
			}, function (start, end, label) {
				$scope.start = new Date(start).getTime();
				$scope.end = new Date(end).getTime();
				$('.daterange span').html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
				// console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
			});
			$scope.search1();
		};
		setTimeout(function () {
			init();
		}, 100);
		//init();
	}

	function ResourcesCtrl($scope, $rootScope, $filter, $http, $location, appConfig, logger, chartService, $uibModal) {
		$scope.filteredStores = [];
		$scope.select = function (page) {
			var end, start;
			start = (page - 1) * $scope.numPerPage;
			end = start + $scope.numPerPage;
			return $scope.currentPageStores = $scope.filteredStores.slice(start, end);
		};

		$scope.onFilterChange = function () {
			$scope.select(1);
			$scope.currentPage = 1;
			return $scope.row = '';
		};

		$scope.onNumPerPageChange = function () {
			$scope.select(1);
			return $scope.currentPage = 1;
		};

		$scope.onOrderChange = function () {
			$scope.select(1);
			return $scope.currentPage = 1;
		};

		$scope.order = function (rowName) {
			if ($scope.row === rowName) {
				return;
			}
			$scope.row = rowName;
			$scope.filteredStores = $filter('orderBy')($scope.stores, rowName);
			return $scope.onOrderChange();
		};
		$scope.search1 = function () {
			$scope.filteredStores = [];
			$scope.row = '';
			$scope.numPerPageOpt = [3, 5, 10, 20];
			$scope.numPerPage = $scope.numPerPageOpt[2];
			$scope.currentPage = 1;
			$scope.currentPageStores = [];
			$rootScope.$broadcast('preloader:active');

			var nowstamp = Date.parse(new Date());
			if (nowstamp + 1000 < $scope.start) {
				$rootScope.$broadcast('preloader:hide');
				logger.logError("无效的时间范围，你正在查询未来时间段的数据。");
				return;
			}

			$scope.total_local_gb_usage = 0;
			$scope.total_memory_mb_usage = 0;
			$scope.total_vcpus_usage = 0;
			$scope.total_active_servers = 0;
			$scope.total_active_memory_mb = 0;
			$http.get(appConfig.main.server + "/services/dash2/resourses?start=" + moment($scope.start).format('YYYY-MM-DD') + "&end=" + moment($scope.end).format('YYYY-MM-DD'), {}).success(function (data) {
				if (data && data.usages) {
					$scope.server_usages = data.usages;
					angular.forEach(data.usages, function (d) {
						$scope.total_local_gb_usage += d.total_local_gb_usage;
						$scope.total_memory_mb_usage += d.total_memory_mb_usage;
						$scope.total_vcpus_usage += d.total_vcpus_usage;
						$scope.total_active_servers += d.acive_servers;
						angular.forEach(d.server_usages, function (d1) {
							$scope.total_active_memory_mb += d1.memory_mb;
						})
					})
					$rootScope.$broadcast('preloader:hide');
					$scope.stores = data.usages;
					$scope.filteredStores = $scope.stores;
					$scope.currentPage = 1;
					$scope.row = '';
					return $scope.select($scope.currentPage);
				}
			}).error(function (err) {
				logger.logError("内部错误");
			})
		}
		var init = function () {
			$scope.end = new Date().getTime();
			$scope.start = $scope.end - (3600000 * 24 * 30);
			$('.daterange span').html(moment($scope.start).format('YYYY-MM-DD') + ' - ' + moment($scope.end).format('YYYY-MM-DD'));
			$(".daterange").daterangepicker({
				timePicker: false,
				// timePickerIncrement: 30,
				"startDate": $scope.start,
				"endDate": $scope.end //,
				// "minDate": new Date()
			}, function (start, end, label) {
				$scope.start = new Date(start).getTime();
				$scope.end = new Date(end).getTime();
				$('.daterange span').html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
				// console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
			});
			$scope.search1();
		};
		setTimeout(function () {
			init();
		}, 100);
		//init();

		$scope.showServers = function (servers) {
			var modalInstance = $uibModal.open({
				animation: true,
				size: 'lg',
				templateUrl: 'server-tpl.html',
				controller: 'ServerListCtrl',
				resolve: {
					datas: function () {
						return {
							servers: servers
						}
					}
				}
			});
		}
	}

	function ServerListCtrl(datas, $scope, $http, $uibModalInstance, $window, $filter, logger, appConfig) {
		$scope.servers = datas.servers;
		$scope.cancel = function () {
			$uibModalInstance.dismiss("cancel");
		};
	}

	function ServerCtrl($scope, $rootScope, $filter, $http, $location, appConfig, logger, chartService) {
		$scope.select = function (page) {
			var end, start;
			start = (page - 1) * $scope.numPerPage;
			end = start + $scope.numPerPage;
			return $scope.currentPageStores = $scope.filteredStores.slice(start, end);
		};

		$scope.onFilterChange = function () {
			$scope.select(1);
			$scope.currentPage = 1;
			return $scope.row = '';
		};

		$scope.onNumPerPageChange = function () {
			$scope.select(1);
			return $scope.currentPage = 1;
		};

		$scope.onOrderChange = function () {
			$scope.select(1);
			return $scope.currentPage = 1;
		};

		$scope.order = function (rowName) {
			if ($scope.row === rowName) {
				return;
			}
			$scope.row = rowName;
			$scope.filteredStores = $filter('orderBy')($scope.stores, rowName);
			return $scope.onOrderChange();
		};

		$scope.pie5 = {};
		$scope.pie6 = {};
		$scope.pie7 = {};
		$scope.init = function () {
			$scope.filteredStores = [];
			$scope.row = '';
			$scope.numPerPageOpt = [3, 5, 10, 20];
			$scope.numPerPage = $scope.numPerPageOpt[2];
			$scope.currentPage = 1;
			$scope.currentPageStores = [];
			$rootScope.$broadcast('preloader:active');
			$http.get(appConfig.main.server + "/services/dash2/vmlist").success(function (data) {
				if (data.hypervisors) {
					$scope.pie5.options = chartService.pieChartModel("虚拟内核使用情况", data.hypervisors.vcpus + ' 中的 ' + data.hypervisors.vcpus_used + ' 已使用', '{b}<br/>虚拟内核：{c}', [{
						value: data.hypervisors.vcpus - data.hypervisors.vcpus_used,
						name: '未使用'
					}, {
						value: data.hypervisors.vcpus_used,
						name: '已使用'
					}]);
					$scope.pie6.options = chartService.pieChartModel("内存使用情况", kmgfmt(data.hypervisors.memory_mb) + ' 中的 ' + kmgfmt(data.hypervisors.memory_mb_used) + ' 已使用', function (v) {
						return v.data.name + '<br/>内存：' + kmgfmt(v.data.value)
					}, [{
						value: data.hypervisors.memory_mb - data.hypervisors.memory_mb_used,
						name: '未使用'
					}, {
						value: data.hypervisors.memory_mb_used,
						name: '已使用'
					}]);
					$scope.pie7.options = chartService.pieChartModel("磁盘使用情况", gtfmt(data.hypervisors.local_gb) + ' 中的 ' + gtfmt(data.hypervisors.local_gb_used) + ' 已使用', function (v) {
						return v.data.name + '<br/>磁盘：' + gtfmt(v.data.value)
					}, [{
						value: data.hypervisors.local_gb - data.hypervisors.local_gb_used,
						name: '未使用'
					}, {
						value: data.hypervisors.local_gb_used,
						name: '已使用'
					}]);
					$rootScope.$broadcast('preloader:hide');
					$scope.stores = data.hypervisors.hostnames;
					$scope.filteredStores = $scope.stores;
					$scope.currentPage = 1;
					$scope.row = '';
					return $scope.select($scope.currentPage);
				}
			}).error(function (err) {
				logger.logError("内部错误");
			});
		};

		setTimeout(function () {
			$scope.init();
		}, 100);
		//$scope.init();
	}
})();