(function() {
	'use strict';

	angular.module('app')
		.controller('DashboardCtrl', ['$scope', '$http', '$filter', '$sce', '$location', 'appConfig', DashboardCtrl])
		.controller('Dashboard3Ctrl', ['$scope', '$location', '$http', 'appConfig', '$interval', Dashboard3Ctrl])
		.filter('fmtTime', fmtTime).filter('dtFormat', dtFormat).filter('eventMessage', eventMessage);

	function DashboardCtrl($scope, $http, $filter, $sce, $location, appConfig) {
		var init = function () {
			$scope.overview = {};
			$http.get(appConfig.main.server + '/services/logtype/list').success(function(data) {
				$scope.logtypes = {};
				for (var i in data){
					$scope.logtypes[data[i].value] = data[i].name;
				}
			});
			$http.get(appConfig.main.server + '/services/dashboard').success(function(data) {
				$scope.overview.users = data.overview.users;
				$scope.overview.questions = data.overview.questions;
				$scope.overview.servers = data.overview.hypervisors;
				$scope.overview.courses = data.lessons.lessons.length;
				$scope.overview.lesson3 = 0;
				$scope.overview.lesson2 = 0;
				$scope.overview.lesson1 = 0;

				for(var i in data.lessons.lessons) {
					if(data.lessons.lessons[i].difficult == 1) {
						$scope.overview.lesson1++;
					} else if(data.lessons.lessons[i].difficult == 2) {
						$scope.overview.lesson2++;
					} else if(data.lessons.lessons[i].difficult == 3) {
						$scope.overview.lesson3++;
					}
				}
				for(var i in $scope.overview.users.detail){
					var val = $scope.overview.users.detail[i];
					for(var j in val){
						val[j].username = i;
					}
					$scope.overview.users.detail[i] = val;
				}
				logInit();
				userInit();
				courseInit();
				questionInit();
				serverInit();
			});
			$scope.numPerPage = 5;
			$scope.showUsers();
		};
		var colorPrimaryLight = '#333',
			colorInfoLight = '#60A7F5',
			colorInfoAltLight = '#C39AE2',
			colorSuccessLight = '#00CDCD',
			colorWarningLight = '#FACC65',
			colorDangerLight = '#DD7078';
		
		$scope.fmtMemory = function(v) {
			if(v >= 1024) {
				return parseFloat(v / 1024).toFixed(1) + 'GB';
			} else if (v > 0){
				return v +'MB';
			} else {
				return '0字节';
			}
		}
		
		$scope.fmtDisk = function(v) {
			if(v >= 1024) {
				return parseFloat(v / 1024).toFixed(1) + 'TB';
			} else if (v > 0){
				return v +'GB';
			} else {
				return '0字节';
			}
		}
		
		$scope.showUsers = function() {
			$scope.userShow = true;
			$scope.courseShow = false;
			$scope.questionShow = false;
			$scope.serverShow = false;
		}
		$scope.showCourses = function() {
			$scope.userShow = false;
			$scope.courseShow = true;
			$scope.questionShow = false;
			$scope.serverShow = false;
		}
		$scope.showQuestions = function() {
			$scope.userShow = false;
			$scope.courseShow = false;
			$scope.questionShow = true;
			$scope.serverShow = false;
		}
		$scope.showServers = function() {
			$scope.userShow = false;
			$scope.courseShow = false;
			$scope.questionShow = false;
			$scope.serverShow = true;
		}

		//日志列表
		var logInit = function () {
			$scope.logFilteredStores = [];
			$scope.logRow = 'logtime';
			$scope.logDesc = 'desc';
			$scope.logSelect = function (page) {
				$scope.logCurrentPage = page;
				$http.post(appConfig.main.server + '/services/log/list', {
					page: $scope.logCurrentPage,
					rows: $scope.numPerPage,
					sort: $scope.logRow,
					order: $scope.logDesc
				}).success(function (data) {
					$scope.logTotal = data.totalElements;
					$scope.logs = data.content;
				});
			};
			$scope.logSelect(1);
		};

		// 用户列表
		$scope.userSelect = function (page) {
			var end, start;
			start = (page - 1) * $scope.numPerPage;
			end = start + $scope.numPerPage;
			return $scope.userCurrentPageStores = $scope.userFilteredStores.slice(start, end);
		};

		$scope.userSearch = function () {
			$scope.userFilteredStores = $filter('filter')($scope.userStores, $scope.userSearchKeywords);
			return $scope.onUserFilterChange();
		};

		$scope.onUserFilterChange = function () {
			$scope.userSelect(1);
			$scope.userCurrentPage = 1;
			return $scope.userRow = '';
		};

		var userInit = function () {
			$scope.userSearchKeywords = '';
			$scope.userFilteredStores = [];
			$scope.userRow = '';
			$scope.userCurrentPage = 1;
			$scope.userCurrentPageStores = [];

			$scope.userStores = [];
			for(var k in $scope.overview.users.detail) {
				$scope.userStores.push({'username': k, 'questions': $scope.overview.users.detail[k]});
			}
			$scope.userSearch();
			$scope.userSelect($scope.userCurrentPage)
		};

		$scope.questionTopo = function(ques) {
			sessionStorage.questionSummary = ques.question_name;
			sessionStorage.questionId = ques.question_id;
			if(ques.username) {
				sessionStorage.username = ques.username;
			}
			$location.path("/questions/topo");
		};
		
		// 课程列表
		$scope.courseSelect = function (page) {
			var end, start;
			start = (page - 1) * $scope.numPerPage;
			end = start + $scope.numPerPage;
			return $scope.courseCurrentPageStores = $scope.courseFilteredStores.slice(start, end);
		};
		
		$scope.courseSearch = function () {
			$scope.courseFilteredStores = $filter('filter')($scope.courseStores, $scope.courseSearchKeywords);
			return $scope.onCourseFilterChange();
		};
		
		$scope.onCourseFilterChange = function () {
			$scope.courseSelect(1);
			$scope.courseCurrentPage = 1;
			return $scope.courseRow = '';
		};

		var courseInit = function () {
			$scope.courseSearchKeywords = '';
			$scope.courseFilteredStores = [];
			$scope.courseRow = '';
			$scope.courseCurrentPage = 1;
			$scope.courseCurrentPageStores = [];
			$http.get(appConfig.main.server + '/services/courses/listall').success(function (data) {
				$scope.courseStores = data.lessons;
				$scope.courseSearch();
				return $scope.courseSelect($scope.courseCurrentPage);
			});
		};

		// 难度列表
		$scope.diffs = function(d) {
			switch(d) {
				case 1: return '低';
				case 2: return '中';
				case 3: return '高';
			}
		};

		$scope.labels = function(labs) {
			var ht = [];
			for(var i in labs) {
				ht.push('<span class="label label-primary">');
				ht.push(labs[i]);
				ht.push('</span> ');
			}
			return $sce.trustAsHtml(ht.join(''));
		};

		$scope.questionSelect = function (page) {
			var end, start;
			start = (page - 1) * $scope.numPerPage;
			end = start + $scope.numPerPage;
			return $scope.questionCurrentPageStores = $scope.questionFilteredStores.slice(start, end);
		};
		
		$scope.questionSearch = function () {
			$scope.questionFilteredStores = $filter('filter')($scope.questionStores, $scope.questionSearchKeywords);
			return $scope.onQuestionFilterChange();
		};
		
		$scope.onQuestionFilterChange = function () {
			$scope.questionSelect(1);
			$scope.questionCurrentPage = 1;
			return $scope.questionRow = '';
		};

		var questionInit = function () {
			$scope.questionSearchKeywords = '';
			$scope.questionFilteredStores = [];
			$scope.questionRow = '';
			$scope.questionCurrentPage = 1;
			$scope.questionCurrentPageStores = [];
			// $http.get(appConfig.main.server + '/services/questions/listall').success(function(data) {
			// 	data.questions.reverse();
			// 	$scope.questionStores = data.questions;
			// 	$scope.questionSearch();
			// 	return $scope.questionSelect($scope.questionCurrentPage);
			// });
			$scope.questionStores = [];
			for(var k in $scope.overview.questions.detail) {
				$scope.questionStores.push({'question_id':k, 'question_name': $scope.overview.questions.detail[k][0].question_name, 'users': $scope.overview.questions.detail[k]});
			}

			$scope.questionSearch();
			$scope.questionSelect($scope.questionCurrentPage)
		};

		// 服务器列表
		$scope.serverSelect = function (page) {
			var end, start;
			start = (page - 1) * $scope.numPerPage;
			end = start + $scope.numPerPage;
			return $scope.serverCurrentPageStores = $scope.serverFilteredStores.slice(start, end);
		};
		
		$scope.serverSearch = function () {
			$scope.serverFilteredStores = $filter('filter')($scope.serverStores, $scope.serverSearchKeywords);
			return $scope.onserverFilterChange();
		};
		
		$scope.onserverFilterChange = function () {
			$scope.serverSelect(1);
			$scope.serverCurrentPage = 1;
			return $scope.serverRow = '';
		};

		var serverInit = function () {
			$scope.serverSearchKeywords = '';
			$scope.serverFilteredStores = [];
			$scope.serverRow = '';
			$scope.serverCurrentPage = 1;
			$scope.serverCurrentPageStores = [];
			$http.get(appConfig.main.server + '/services/dash2/vmlist').success(function (data) {
				$scope.serverStores = data.hypervisors.hostnames;
				$scope.serverSearch();
				return $scope.serverSelect($scope.serverCurrentPage);
			});
		};

		init();

		$scope.line2 = {};
		$scope.bar1 = {};
		$scope.scatter2 = {};

		$scope.line2.options = {
			tooltip: {
				trigger: 'axis'
			},
			legend: {
				data: ['Email', 'Affiliate', 'Video Ads', 'Direct', 'Search']
			},
			toolbox: {
				show: true,
				feature: {
					saveAsImage: {
						show: true,
						title: "save as image"
					}
				}
			},
			calculable: true,
			xAxis: [{
				type: 'category',
				boundaryGap: false,
				data: ['Mon.', 'Tue.', 'Wed.', 'Thu.', 'Fri.', 'Sat.', 'Sun.']
			}],
			yAxis: [{
				type: 'value'
			}],
			series: [{
				name: 'Email',
				type: 'line',
				stack: 'Sum',
				data: [120, 132, 101, 134, 90, 230, 210],
				itemStyle: {
					normal: {
						color: colorSuccessLight
					}
				}
			}, {
				name: 'Affiliate',
				type: 'line',
				stack: 'Sum',
				data: [220, 182, 191, 234, 290, 330, 310],
				itemStyle: {
					normal: {
						color: colorInfoAltLight
					}
				}
			}, {
				name: 'Video Ads',
				type: 'line',
				stack: 'Sum',
				data: [150, 232, 201, 154, 190, 330, 410],
				itemStyle: {
					normal: {
						color: colorInfoLight
					}
				}
			}, {
				name: 'Direct',
				type: 'line',
				stack: 'Sum',
				data: [320, 332, 301, 334, 390, 330, 320],
				itemStyle: {
					normal: {
						color: colorWarningLight
					}
				}
			}, {
				name: 'Search',
				type: 'line',
				stack: 'Sum',
				data: [820, 932, 901, 934, 1290, 1330, 1320],
				itemStyle: {
					normal: {
						color: colorDangerLight
					}
				}
			}]
		};
		$scope.bar1.options = {
			tooltip: {
				trigger: 'axis'
			},
			legend: {
				data: ['Predict', 'Actual']
			},
			toolbox: {
				show: true,
				feature: {
					saveAsImage: {
						show: true,
						title: "save as image"
					}
				}
			},
			calculable: true,
			xAxis: [{
				type: 'category',
				data: ['Jan.', 'Feb.', 'Mar.', 'Apr.', 'May', 'Jun.', 'Jul.', 'Aug.', 'Sep.', 'Oct.', 'Nov.', 'Dec.']
			}],
			yAxis: [{
				type: 'value'
			}],
			series: [{
				name: 'Predict',
				type: 'bar',
				data: [2.0, 4.9, 7.0, 23.2, 25.6, 76.7, 135.6, 162.2, 32.6, 20.0, 6.4, 3.3],
				itemStyle: {
					normal: {
						color: colorSuccessLight
					}
				},
				markPoint: {
					data: [{
						type: 'max',
						name: 'Max'
					}, {
						type: 'min',
						name: 'Min'
					}]
				},
				markLine: {
					data: [{
						type: 'average',
						name: 'Average'
					}]
				}
			}, {
				name: 'Actual',
				type: 'bar',
				data: [2.6, 5.9, 9.0, 26.4, 28.7, 70.7, 175.6, 182.2, 48.7, 18.8, 6.0, 2.3],
				itemStyle: {
					normal: {
						color: colorInfoAltLight
					}
				},
				markPoint: {
					data: [{
						name: 'Highest',
						value: 182.2,
						xAxis: 7,
						yAxis: 183,
						symbolSize: 18
					}, {
						name: 'Lowest',
						value: 2.3,
						xAxis: 11,
						yAxis: 3
					}]
				},
				markLine: {
					data: [{
						type: 'average',
						name: 'Average'
					}]
				}
			}]
		};
	}

	function fmtTime() {
		return function(v) {
			var r = ''
			if(v > 3600 * 24) {
				r += parseInt(v / 3600 / 24) + '天';
				v = v % (3600 * 24);
			}
			r += parseInt(v / 3600) + '小时';
			v = v % 3600;
			r += parseInt(v / 60) + '分钟';
			v = v % 60;
			r += v + '秒';
			return r;
		}
	}
	
	function eventMessage() {
		return function(v) {
			switch(v.type+'') {
				case '1': return '比赛开始';
				case '2': return '比赛结束';
				case '3': return '用户' + v.realname + '拿到旗帜文件,得' + v.score + '分';
				case '4': return '用户' + v.realname + '未拿到旗帜文件';
			}
		}
	}

	function dtFormat() {
		return function(v) { //author: meizz 
			v = new Date(v);
			var fmt = 'yyyy-MM-dd hh:mm:ss';
			var o = {
				"M+": v.getMonth() + 1, //月份 
				"d+": v.getDate(), //日 
				"h+": v.getHours(), //小时 
				"m+": v.getMinutes(), //分 
				"s+": v.getSeconds(), //秒 
				"q+": Math.floor((v.getMonth() + 3) / 3), //季度 
				"S": v.getMilliseconds() //毫秒 
			};
			if(/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (v.getFullYear() + "").substr(4 - RegExp.$1.length));
			for(var k in o)
				if(new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
			return fmt;
		}
	}

	function Dashboard3Ctrl($scope, $location, $http, appConfig, $interval) {
		$scope.$on('$destroy', function() {
			$interval.cancel($scope.timer);
			if($scope.socket) {
				$scope.socket.close();
				$scope.socket = null;
			}
		});
		
		$("#nav li").hide();
		$("#app").addClass("nav-collapsed-min");

		var init = function() {
			if(window.localStorage['currentContest']){
				$scope.currentContest = JSON.parse(window.localStorage['currentContest']);
				$scope.remainTime = $scope.currentContest.end - (parseInt(new Date().getTime() / 1000));
				$http.get(appConfig.main.server + '/services/contests/' + $scope.currentContest.id).success(function(data) {
					$scope.mine = data.contest;
				});
				return true;
			} else {
				window.localStorage.clear();
				$location.url('/page/signin2'); 
				return false;
			}
		}
		
		$scope.cancelInterval = function() {
			if($scope.timer) {
				$interval.cancel($scope.timer);
				$scope.timer = null;
			}
		}

		$scope.timer = $interval(function() {
			$scope.remainTime -= 1;
			if($scope.remainTime < 0) {
				$scope.remainTime = 0;
                layer.open({
                    type: 1,
					title: false,
					shade: 0.8,
					anim: 1,
					content: '<div style="padding:10px 50px 10px 50px;font-size: xx-large">比赛结束</div>',
					btn: '确定',
					btnAlign: 'c',
					closeBtn: false
                });
                $location.url('page/signout', {
                    reload: true
                });
                $scope.cancelInterval();
			}
		}, 1000);

		$scope.questionTopo = function(ques) {
			sessionStorage.questionSummary = ques.summary;
			sessionStorage.questionId = ques.id;
			$location.path("/questions/topo");
		}
		$scope.events = [];
		var initWebSocket = function() {
			if(window.WebSocket) {
				$scope.socket = new WebSocket(appConfig.main.wsserver + "/contest/event");
				$scope.socket.onmessage = function(event) {
					var json = JSON.parse(event.data);
					if(json) {
						$scope.events.unshift(json);
					}
                    $http.get(appConfig.main.server + '/services/contests/' + $scope.currentContest.id).success(function(data) {
                        $scope.mine = data.contest;
                    });
				};
				$scope.socket.onopen = function(event) {
					console.log("Web Socket opened!");
					$scope.socket.send(JSON.stringify({
						username: window.localStorage['uname'],
						contest_id: $scope.currentContest.id
					}))
				};
				$scope.socket.onclose = function(event) {
					console.log("Web Socket closed.");
					$scope.socket = null;
				};
				$scope.socket.onerror = function(event) {
					console.log("Web Socket error.");
					$scope.socket = null;
				};
			} else {
				console.log("Your browser does not support Web Socket.");
			}
		}
		if(init()) {
			initWebSocket();
		}
	}

})();