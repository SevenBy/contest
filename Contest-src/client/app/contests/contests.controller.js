(function() {
	'use strict';

	angular.module('app.contests', ['app.service'])
		.controller('ContestsCtrl', ['$scope', '$http', '$filter', '$uibModal', 'appConfig', 'logger', '$location', ContestsCtrl])
		.controller('ContestsAddCtrl', ['$scope', '$http', '$location', '$uibModal', '$timeout', 'appConfig', 'logger', ContestsAddCtrl])
		.controller('ContestsEditCtrl', ['$scope', '$http', '$location', '$uibModal', '$timeout', 'appConfig', 'logger', 'tool', 'link', ContestsEditCtrl])
		.controller('ContestsMoreCtrl', ['$scope', '$http', '$location', '$uibModal', 'appConfig', 'logger', '$filter', '$interval', '$window', ContestsMoreCtrl])
		.controller('AddUserContentCtrl', ['$scope', '$http', '$filter', 'datas', '$uibModalInstance', 'appConfig', AddUserContentCtrl])
		.controller('AddQuesContentCtrl', ['$scope', '$http', 'datas', '$uibModalInstance', 'appConfig', AddQuesContentCtrl]).filter('fmtTime', fmtTime);

	function ContestsCtrl($scope, $http, $filter, $uibModal, appConfig, logger, $location) {
		var init;

		$scope.searchKeywords = window.localStorage['contestNameKey'] == undefined ? '' : window.localStorage['contestNameKey'];

		$scope.filteredStores = [];

		$scope.row = '-start';
		
		$scope.select = function(page) {
			var end, start;
			start = (page - 1) * $scope.numPerPage;
			end = start + $scope.numPerPage;
			$scope.currentPage = window.localStorage['currPage'] = page;
			return $scope.currentPageStores = $scope.filteredStores.slice(start, end);
		};

		$scope.onFilterChange = function() {
			$scope.select($scope.currentPage);
			return $scope.row = '-start';
		};

		$scope.onNumPerPageChange = function() {
			window.localStorage['numPerPage'] = $scope.numPerPage;
			$scope.select(1);
			return $scope.currentPage = 1;
		};

		$scope.onOrderChange = function() {
			$scope.select(1);
			return $scope.currentPage = 1;
		};

		$scope.search = function() {
			window.localStorage['contestNameKey'] = $scope.searchKeywords;
			$scope.filteredStores = $filter('filter')($scope.stores, $scope.searchKeywords);
			if($scope.row) {
				$scope.filteredStores = $filter('orderBy')($scope.filteredStores, $scope.row);
			}
			return $scope.onFilterChange();
		};

		$scope.order = function(rowName) {
			if($scope.row === rowName) {
				return;
			}
			$scope.row = rowName;
			$scope.filteredStores = $filter('orderBy')($scope.stores, rowName);
			return $scope.onOrderChange();
		};

		$scope.numPerPageOpt = [3, 5, 10, 20];

		var numPerPage = window.localStorage['numPerPage'];
    	$scope.numPerPage = numPerPage == undefined ? $scope.numPerPageOpt[2] : parseInt(numPerPage);

		$scope.currentPage = 1;

		$scope.currentPageStores = [];

		$scope.statusLabel = function(st) {
			return appConfig.contestStatus[st];
		}

		$scope.statusCls = function(st) {
			var cls = {
				0: '#FFB61C',
				1: '#1C7EBB',
				2: '#23AE89'
			}
			return cls[st];
		}

		$scope.addContest = function() {
			$location.url('/contests/add');
		}
		$scope.contestMore = function(ctst) {
			sessionStorage.contest = JSON.stringify(ctst);
			$location.url('/contests/more');
		}

		$scope.contestEditor = function(ctst) {
			sessionStorage.contests = JSON.stringify(ctst);
			$location.url('/contests/edit');
		}

		$scope.contenstRemove = function(ctst) {
			var modalInstance = $uibModal.open({
				animation: true, //$scope.animationsEnabled,
				templateUrl: 'confirm.html',
				controller: 'ConfirmCtrl'
			});
			modalInstance.result.then(function() {
				$http.post(appConfig.main.server + "/services/contests/delete?id=" + ctst.id, {}).success(function(data) {
					logger.logSuccess("记录已删除！");
					var idx = -1;
					for(var i = 0, len = $scope.stores.length; i < len; i++) {
						if($scope.stores[i].id == ctst.id) {
							idx = i;
							break;
						}
					}
					$scope.stores.splice(idx, 1);
					$scope.search();
				}).error(function(err) {
					logger.logError("删除失败");
				});
			});
		}

		init = function() {
			$http.get(appConfig.main.server + '/services/contests/listall').success(function(data) {
            	var currentPage = window.localStorage['currPage'];
                if(currentPage != undefined) $scope.currentPage = currentPage;
				$scope.stores = data.contests;
				$scope.search();
			});
		};

		init();
	}

	function ContestsAddCtrl($scope, $http, $location, $uibModal, $timeout, appConfig, logger) {
		$http.get(appConfig.main.server + '/services/courses/lesson/category/list')
			.success(function(data) {
				$scope.cates = data.question_categories;
			});
		$http.get(appConfig.main.server + '/services/questions/listall')
			.success(function(data) {
                $scope.questions = data.questions;
                $scope.questions2 = [];
			});
		$http.get(appConfig.main.server + '/services/accounts/listall')
			.success(function(data) {
                $scope.users = data.users;
                $scope.users2 = [];
			});

		$scope.userManage = function() {
			var modalInstance = $uibModal.open({
				animation: true,
				templateUrl: 'addUserContent.html',
				controller: 'AddUserContentCtrl',
				resolve: {
					datas: function() {
						return {
							users: $scope.users,
							users2: $scope.users2
						}
					}
				}
			});

			modalInstance.result.then(function(users) {
				$scope.userSel = users;
			}, function() {});
		};
		$scope.quesManage = function() {
			var modalInstance = $uibModal.open({
				animation: true,
				templateUrl: 'addQuesContent.html',
				controller: 'AddQuesContentCtrl',
				resolve: {
					datas: function() {
						return {
							questions: $scope.questions,
							questions2: $scope.questions2,
							cates: $scope.cates
						}
					}
				}
			});

			modalInstance.result.then(function(lessons) {
				$scope.questionSel = lessons;
			}, function() {

			});
		};
		var init = function() {
			$scope.contests = {};
			$scope.contests.users = [];
			$scope.contests.questions = [];
		};
		init();
		var checkForm = function() {
			if(!$scope.contests.title) {
				logger.logError("比赛名称不能为空");
				return false;
			} else if(!$scope.questionSel || $scope.questionSel.length == 0) {
				logger.logError("请选择题目");
				return false;
			} else if(!$scope.userSel || $scope.userSel.length == 0) {
				logger.logError("请选择用户");
				return false;
			}
            $scope.contests.startstr = $(".startstr").val();
            $scope.contests.endstr = $(".endstr").val();
            if(!$scope.contests.startstr || !$scope.contests.endstr){
            	logger.logError("请指定比赛的起始时间");
				return false;
            }
            return true;
		};
		$scope.save = function() {
			if(checkForm()) {
				$scope.contests.users = [];
				$scope.contests.questions = [];
				for(var i in $scope.userSel) {
					$scope.contests.users.push($scope.userSel[i].username);
				}
				for(var i in $scope.questionSel) {
					$scope.contests.questions.push({
						question_id: $scope.questionSel[i].id,
						question_score: $scope.questionSel[i].flag_score
					});
				}
				$http.post(appConfig.main.server + "/services/contests/save", $scope.contests)
					.success(function(data) {
						if(data && data.contest) {
							logger.logSuccess("添加成功");
							$timeout(function() {
								$scope.back();
							}, 2000);
						} else if (data && data.exception) {
							logger.logError(data.exception);
						} else logger.logError("添加失败");
					}).error(function(err) {
						logger.logError("添加失败");
					})
			}
		};
		$scope.back = function() {
			$location.path("/contests");
		}
	}

	function ContestsEditCtrl($scope, $http, $location, $uibModal, $timeout, appConfig, logger, tool, link) {
        $http.get(appConfig.main.server + '/services/courses/lesson/category/list')
            .success(function(data) {
                $scope.cates = data.question_categories;
            });

		$scope.userManage = function() {
			var modalInstance = $uibModal.open({
				animation: true,
				templateUrl: 'addUserContent.html',
				controller: 'AddUserContentCtrl',
				resolve: {
					datas: function() {
						return {
							users: $scope.users,
                            users2: $scope.users2
						}
					}
				}
			});

			modalInstance.result.then(function(users) {
				$scope.userSel = users;
			}, function() {});
		};
		$scope.quesManage = function() {
			var modalInstance = $uibModal.open({
				animation: true,
				templateUrl: 'addQuesContent.html',
				controller: 'AddQuesContentCtrl',
				resolve: {
					datas: function() {
						return {
                            questions: $scope.questions,
                            questions2: $scope.questions2,
							cates: $scope.cates
						}
					}
				}
			});

			modalInstance.result.then(function(lessons) {
				$scope.questionSel = lessons;
			}, function() {

			});
		};
		var init = function() {
			$scope.contests = JSON.parse(sessionStorage.contests);
			$scope.contests.users = [];
			$scope.contests.questions = [];
			$http.get(appConfig.main.server + '/services/contests/' + $scope.contests.id).success(function(data) {
				$scope.contests = data.contest;
				$scope.questionSel = data.contest.questions;
				$scope.userSel = data.contest.scoreboard;

                $http.get(appConfig.main.server + '/services/questions/listall')
                    .success(function(data) {
                        $scope.questions = data.questions;
                        $scope.questions2 = $scope.questionSel;
                    });
                $http.get(appConfig.main.server + '/services/accounts/listall')
                    .success(function(data) {
                        $scope.users = data.users;
                        $scope.users2 = $scope.userSel;
                    });
			});

			$(".startstr").val($scope.contests.startstr)
		};
		init();
		var checkForm = function() {
			if(!$scope.contests.title) {
				logger.logError("比赛名称不能为空");
				return false;
			} else if(!$scope.questionSel || $scope.questionSel.length == 0) {
				logger.logError("请选择题目");
				return false;
			} else if(!$scope.userSel || $scope.userSel.length == 0) {
				logger.logError("请选择用户");
				return false;
			}
            $scope.contests.startstr = $(".startstr").val();
            $scope.contests.endstr = $(".endstr").val();
            if(!$scope.contests.startstr || !$scope.contests.endstr){
            	logger.logError("请指定比赛的起始时间");
				return false;
            }
			return true;
		};
		$scope.save = function() {
			if(checkForm()) {
				var questions = [];
				for(var i in $scope.questionSel) {
					questions.push({
						question_id: $scope.questionSel[i].id,
						question_score: $scope.questionSel[i].flag_score //parseInt($scope.questionSel[i].question_score)
					});
				}
				var users = [];
				for(var i in $scope.userSel) {
					users.push($scope.userSel[i].username);
				}
				$http.post(appConfig.main.server + "/services/contests/update", {
					id: $scope.contests.id,
					title: $scope.contests.title,
					questions: questions,
					users: users,
					startstr: $scope.contests.startstr,
					endstr: $scope.contests.endstr,
				}).success(function(data) {
					if(data && data.exception) {
						logger.logError(data.exception);
					} else {
						logger.logSuccess("更新成功");
						$timeout(function() {
							$scope.back();
						}, 2000);
					}
				}).error(function(err) {
					logger.logError("更新失败");
				})
			}
		};
		$scope.back = function() {
			$location.path("/contests");
		}
	}

	function ContestsMoreCtrl($scope, $http, $location, $uibModal, appConfig, logger, $filter, $interval, $window) {
		var qmap = {};
		$scope.$on('$destroy', function() {
			$interval.cancel($scope.timer);
            if($scope.websocket) {
                $scope.websocket.close();
                $scope.websocket = null;
            }
		});
		$scope.questionscore = function(idx){
			return idx + '('+ $scope.contest.question_scores[idx - 1] + ')';
		}
		var init = function() {
			$scope.contest = JSON.parse(sessionStorage.contest);
			$http.get(appConfig.main.server + '/services/contests/' + $scope.contest.id).success(function(data) {
				$scope.contest = data.contest;
				$scope.quesCntList = [];
				for(var i = 1; i <= $scope.contest.questions.length; i++) {
					$scope.quesCntList.push(i);
					qmap[$scope.contest.questions[i - 1].id] = i;
				}
				var now = parseInt(new Date().getTime() / 1000);
				if(data.contest.end > now && data.contest.start < now) {
					// 已开始
					$scope.remainTime = data.contest.end - parseInt(new Date().getTime() / 1000);
					$scope.begin = true;
					$scope.timer = $interval(function() {
						$scope.remainTime -= 1;
						if($scope.remainTime <= 0) {
							$scope.remainTime = 0;
							$window.location.reload();
						}
					}, 1000);
				} else if(data.contest.start > now) {
					// 未开始
					$scope.beforeTime = data.contest.start - parseInt(new Date().getTime() / 1000);
					$scope.beforebegin = true;
					$scope.timer = $interval(function() {
						$scope.beforeTime -= 1;
						if($scope.beforeTime <= 0) {
							$scope.beforeTime = 0;
							$window.location.reload();
						}
					}, 1000);
				}
				
				initWS();
			});
			$(".auto-height").height($(window).height() - 290);
		};
		$scope.iscomplete = function(arr, i) {
			return("," + arr.join(",") + ",").indexOf("," + i + ",") == -1 ? 'btn-gray' : 'btn-success';
		};
		var initWS = function() {
			window.WebSocket = window.WebSocket || window.MozWebSocket;
            $scope.websocket = new WebSocket(appConfig.main.wsserver + '/contest/event');
            $scope.websocket.onopen = function() {
                console.log("Web Socket opened!");
				var wsdata = {
					username: window.localStorage['uname'],
					contest_id: $scope.contest.id
				};
                $scope.websocket.send(JSON.stringify(wsdata))
			};
            $scope.websocket.onclose = function(event) {
				console.log("Web Socket closed.");
                $scope.websocket = null;
				// $window.location.reload();
			};
            $scope.websocket.onerror = function(event) {
				console.log("Web Socket error.");
                $scope.websocket = null;
			};
            $scope.websocket.onmessage = function(evt) {
				var data = JSON.parse(evt.data);
				$("#events").prepend('<tr><td>' + $filter("date")(data.time * 1000, "yyyy-MM-dd HH:mm:ss") + '</td><td>' + detail(data) + '</td></tr>')
                $http.get(appConfig.main.server + '/services/contests/' + $scope.contest.id).success(function(data) {
                    $scope.contest = data.contest;
                    $scope.quesCntList = [];
                    for(var i = 1; i <= $scope.contest.questions.length; i++) {
                        $scope.quesCntList.push(i);
                        qmap[$scope.contest.questions[i - 1].id] = i;
                    }
                });
			};
			var detail = function(evt) {
				switch(evt.type) {
					case 1:
						return '<span class="color-primary">比赛开始</span>';
					case 2:
						return '<span class="color-primary">比赛结束</span>';
					case 3:
						return '<span class="color-danger">' + evt.realname + '夺取题目' + qmap[evt.question_id] + '的旗帜文件成功，获得' + evt.score + '分</span>';
					case 4:
						return evt.realname + '夺取题目' + qmap[evt.question_id] + '的旗帜文件失败。';
				}
			};
		};

		init();
		$scope.back = function() {
			$location.path("/contests");
		}
	}

	function AddUserContentCtrl($scope, $http, $filter, datas, $uibModalInstance, appConfig) {
		$scope.users2 = datas.users2;
		$scope.users = datas.users.filter(function (u1) {
            return $scope.users2.map(function (u2) {return u2.username}).indexOf(u1.username) === -1
        });

		$scope.cates = [{
			value: 1,
			text: '组用户'
		}, {
			value: 2,
			text: '地方用户'
		}];

		$scope.changeCate = function(cate) {
            $scope.users = function () {
				if (cate === null) {
					return datas.users
				} else {
					return $filter("filter")(datas.users, {
                        type: cate
                    }, true)
				}
            }().filter(function (u1) {
                return $scope.users2.map(function (u2) {return u2.username}).indexOf(u1.username) === -1
            });
		};

		$scope.ltor = function() {
			for(var i in $scope.userSel1) {
				$scope.users.splice($scope.users.indexOf($scope.userSel1[i]), 1);
				$scope.users2.push($scope.userSel1[i]);
			}
		};

		$scope.lator = function() {
			for(var i in $scope.users) {
				$scope.users2.push($scope.users[i]);
			}
			$scope.users = [];
		};

		$scope.rtol = function() {
			for(var i in $scope.userSel2) {
				$scope.users2.splice($scope.users2.indexOf($scope.userSel2[i]), 1);
				$scope.users.push($scope.userSel2[i]);
			}
		};

		$scope.ratol = function() {
			for(var i in $scope.users2) {
				$scope.users.push($scope.users2[i]);
			}
            $scope.users2.splice(0, $scope.users2.length);
		};

		$scope.ok = function() {
			$uibModalInstance.close($scope.users2);
		};

		$scope.cancel = function() {
			$uibModalInstance.dismiss("cancel");
		};
	}

	function AddQuesContentCtrl($scope, $http, datas, $uibModalInstance, appConfig) {
		$scope.cates = datas.cates;
		$scope.questions2 = datas.questions2;
        $scope.questions = datas.questions.filter(function (q1) {
            return $scope.questions2.map(function (q2) {
                    return q2.id
                }).indexOf(q1.id) === -1
        });

		$scope.changeCate = function(cate) {
            $scope.questions = function () {
            	if (cate === null) {
					return datas.questions;
				} else {
					return datas.questions.filter(function (q) {
						return q.categories.indexOf(cate) !== -1
					})
				}
            }().filter(function (q1) {
					return $scope.questions2.map(function (q2) {
						return q2.id
					}).indexOf(q1.id) === -1
				});
		};

		$scope.ltor = function() {
			for(var i in $scope.lesson) {
				$scope.questions.splice($scope.questions.indexOf($scope.lesson[i]), 1);
				$scope.questions2.push($scope.lesson[i]);
			}
		};

		$scope.lator = function() {
			for(var i in $scope.questions) {
				$scope.questions2.push($scope.questions[i]);
			}
			$scope.questions = [];
		};

		$scope.rtol = function() {
			for(var i in $scope.lesson2) {
				$scope.questions2.splice($scope.questions2.indexOf($scope.lesson2[i]), 1);
				$scope.questions.push($scope.lesson2[i]);
			}
		};

		$scope.ratol = function() {
			for(var i in $scope.questions2) {
				$scope.questions.push($scope.questions2[i]);
			}
			$scope.questions2.splice(0, $scope.questions2.length);
		};

		$scope.ok = function() {
			$uibModalInstance.close($scope.questions2);
		};

		$scope.cancel = function() {
			$uibModalInstance.dismiss("cancel");
		};
	}
	
	function fmtTime() {
		return function(v) {
			var r = ''
			if(v > 3600 * 24) {
				r += parseInt(v / 3600 / 24) + '天';
				v = v % (3600 * 24);
			}
			r += parseInt(v / 3600) + '小时';
			v = v % 3600;
			r += parseInt(v / 60) + '分钟';
			v = v % 60;
			r += v + '秒';
			return r;
		}
	}

})();