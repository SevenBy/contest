(function () {
    'use strict';

    angular.module('app', [
        // Angular modules
        'ui.router',
        'ngAnimate',
        'ngAria',

        // 3rd Party Modules
        'oc.lazyLoad',
        'ui.bootstrap',
        'ngTagsInput',
        'duScroll',
        'mgo-angular-wizard',
	    'chieffancypants.loadingBar',

        // Custom modules
        'app.nav',
        'app.i18n',
        'app.chart',
        'app.ui',
        'app.ui.form',
        'app.ui.form.validation',
        'app.page',
        'app.table',
        'app.task',
        'app.courses',
        'app.contests',
        'app.questions',
        'app.achievements',
        'app.users',
        'app.images',
        'app.resources',
        'app.system',
        'app.service'
    ]).run(['$location','$rootScope',function($location, $rootScope){
	    $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
		    $rootScope.title = '训练演练系统-' + toState.title
	    });
    }]);

})();






    

