package com.shzejing.spring.controller;

import com.shzejing.spring.common.HttpUtils;
import com.shzejing.spring.entities.Account;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.http.client.ClientProtocolException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Calendar;

@Controller
@Transactional(readOnly = true)
@RequestMapping({ "/services" })
public class DashboardController {

	@Autowired
	private Environment env;

	@RequestMapping(value = { "/dashboard" }, method = { RequestMethod.GET })
	@ResponseBody
	public String dashboard(HttpSession session) throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		JSONObject result = HttpUtils.get("v1.0/overview", account, this.env, token);
		JSONObject lessons = HttpUtils.get("v1.0/lessons", account, this.env, token);
		result.put("lessons", lessons);
		return result.toString();
	}

	@RequestMapping(value = { "/dash2/overview" }, method = { RequestMethod.GET })
	@ResponseBody
	public String overview(HttpSession session) throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		return HttpUtils.get("v1.0/overview", account, this.env, token).toString();
	}

	@RequestMapping(value = { "/dashboard2" }, method = { RequestMethod.GET })
	@ResponseBody
	public String dashboard2(HttpSession session) throws ClientProtocolException, IOException {
		Calendar cal = Calendar.getInstance();
		String start = DateFormatUtils.format(Calendar.getInstance(), "yyyy-MM-dd");
		cal.add(2, -1);
		String end = DateFormatUtils.format(cal, "yyyy-MM-dd");
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		JSONObject result = HttpUtils.get("v1.0/absolutelimits", account, this.env, token);
		result.put("usage",
				HttpUtils.get("v1.0/usage?start=" + start + "&end=" + end, account, this.env, token).getJSONObject("usage"));
		return result.toString();
	}

	@RequestMapping(value = { "/dash2/absolutelimits" }, method = { RequestMethod.GET })
	@ResponseBody
	public String absolutelimits(HttpSession session) throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		return HttpUtils.get("v1.0/absolutelimits", account, this.env, token).toString();
	}

	@RequestMapping(value = { "/dash2/usage" }, method = { RequestMethod.GET })
	@ResponseBody
	public String usage(@RequestParam("start") String start, @RequestParam("end") String end, HttpSession session)
			throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		return HttpUtils.get("v1.0/usage?start=" + start + "&end=" + end, account, this.env, token).toString();
	}

	@RequestMapping(value = { "/host/monitor/{vmid}/{period}/{times}" }, method = { RequestMethod.GET })
	@ResponseBody
	public String hostMonitor(@PathVariable("vmid") String vmid, @PathVariable("period") String period, @PathVariable("times") String times, HttpSession session)
			throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		return HttpUtils.get("v1.0/meter/" + vmid + "?period=" + period + "&times=" + times, account, this.env, token).toString();
	}

	@RequestMapping(value = { "/host/list" }, method = { RequestMethod.GET })
	@ResponseBody
	public String hostlist(HttpSession session) throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		JSONObject jo = HttpUtils.get("v1.0/questions", account, this.env, token);
		if (jo.has("exception")) {
			return jo.toString();
		}
		JSONArray questions = jo.getJSONArray("questions");
		for (int i = 0; i < questions.size(); i++) {
			JSONObject q = questions.getJSONObject(i);
			JSONObject servers = HttpUtils.get("v1.0/servers?question_id=" + q.getString("id"), account, 
					this.env, token);
			if (!servers.has("exception")) {
				JSONArray jarr = servers.getJSONArray("servers");
				for (int j = 0; j < jarr.size(); j++) {
					JSONObject server = HttpUtils.get("v1.0/server/" + jarr.getJSONObject(j).get("id"), account,
							this.env, token);
					jarr.getJSONObject(j).put("name", server.getJSONObject("server").get("name"));
				}
				q.put("hosts", jarr);
			}
		}
		return jo.toString();
	}

	@RequestMapping(value = { "/dash2/vmlist" }, method = { RequestMethod.GET })
	@ResponseBody
	public String vmlist(HttpSession session) throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		JSONObject jo = HttpUtils.get("v1.0/hypervisors", account, this.env, token);
		if (jo.has("exception")) {
			return jo.toString();
		}
		JSONArray hosts = jo.getJSONObject("hypervisors").getJSONArray("hostnames");
		for (int i = 0; i < hosts.size(); i++) {
			JSONObject host = hosts.getJSONObject(i);
			if (host.containsKey("hypervisor"))
				continue;
			JSONObject ho = HttpUtils.get("v1.0/hypervisor/" + host.getString("hypervisor_hostname"), account,
					this.env, token);
			host.put("hypervisor", ho.getJSONObject("hypervisor"));
		}
		return jo.toString();
	}

	@RequestMapping(value = { "/dash2/resourses" }, method = { RequestMethod.GET })
	@ResponseBody
	public String resourses(@RequestParam("start") String start, @RequestParam("end") String end, HttpSession session)
			throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		return HttpUtils.get("v1.0/usages?start=" + start + "&end=" + end, account, this.env, token).toString();
	}
}