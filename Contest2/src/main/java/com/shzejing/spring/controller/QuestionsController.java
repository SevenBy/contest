package com.shzejing.spring.controller;

import com.shzejing.spring.common.HttpUtils;
import com.shzejing.spring.common.LogTypeEnum;
import com.shzejing.spring.common.UserTypeEnum;
import com.shzejing.spring.dao.CommentDao;
import com.shzejing.spring.entities.Account;
import com.shzejing.spring.entities.Comment;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.ClientProtocolException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.util.*;

@Controller
@Transactional(readOnly = true)
@RequestMapping("/services/questions")
public class QuestionsController {

	@Autowired
	private Environment env;

	@Autowired
	private LogController logController;

	@Autowired
	private CommentDao commentDao;

	@RequestMapping(value = "/listall", method = RequestMethod.GET)
	@ResponseBody
	public String listall(HttpSession session) throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		JSONObject jo = HttpUtils.get("v1.0/questions", account, env, token);
		if (jo.has("exception")) {
			return jo.toString();
		}
		JSONArray jarr = jo.getJSONArray("questions");
		for (int i = 0; i < jarr.size(); i++) {
			JSONObject joo = jarr.getJSONObject(i);
			String id = joo.getString("id");
			if (joo.has("timeout")) {
				joo.put("timeout", joo.getInt("timeout") / 3600.0f);
			}
			File f = new File(env.getProperty("file.upload.path") + "questions/" + id + "/answerfile");
			if (f.exists() && f.list() != null && f.list().length > 0) {
				JSONObject tempjson = (JSONObject) jo.get("question");
				joo.put("answerfile", env.getProperty("file.upload.context") + "questions/" + id + "/answerfile/" + FilenameUtils.getName(f.list()[0]));
			}
		}
		return jo.toString();
	}

	@RequestMapping(value = { "/{id}" }, method = RequestMethod.GET)
	@ResponseBody
	public String question(@PathVariable("id") String questionid, HttpSession session)
			throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		JSONObject jo = HttpUtils.get("v1.0/question/" + questionid, account, this.env, token);
		File f = new File(env.getProperty("file.upload.path") + "questions/" + questionid + "/answerfile/");
		if (f.exists() && f.list() != null && f.list().length > 0) {
			JSONObject tempjson = (JSONObject) jo.get("question");
			tempjson.put("answerfile", env.getProperty("file.upload.context") + "questions/" + questionid + "/answerfile/" + FilenameUtils.getName(f.list()[0]));
			jo.put("question", tempjson);
		}
		return jo.toString();
	}

	@RequestMapping(value = "/list2all", method = RequestMethod.POST)
	@ResponseBody
	public String cates(@RequestBody JSONObject data, HttpSession session)
			throws ClientProtocolException, IOException {
		String cate = data.optString("category", "");
		if ("null".equals(cate)) {
			return listall(session);
		}
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		return HttpUtils.get("v1.0/questions?category=" + cate, account, env, token).toString();
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@ResponseBody
	@Transactional
	public String saveQustion(@RequestBody JSONObject question, HttpSession session, HttpServletRequest request)
			throws ClientProtocolException, IOException {
		int timeout = (int) (question.getDouble("timeout") * 3600);
		question.put("timeout", timeout);
		String name = question.getString("summary");
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		JSONArray cates = question.getJSONArray("categories");
		for (int i = 0; i < cates.size(); i++) {
			addCategory(cates.getString(i), account, token);
		}
		String flag_score = question.optString("flag_score");
		if (!StringUtils.isEmpty(flag_score)) {
			question.put("flag_score", Integer.parseInt(flag_score));
		}
		JSONObject result = HttpUtils.post("v1.0/questions", question.toString(), account, env, token);
		if (result.has("exception")) {
			return result.toString();
		} else {
			String questionId = result.getJSONObject("question").getString("id");

			if (StringUtils.isNotEmpty(question.getString("answerfile"))) {
				FileUtils.deleteQuietly(new File(env.getProperty("file.upload.path") + "questions/" + questionId + "/answerfile"));
				FileUtils.copyFile(new File(env.getProperty("file.upload.path") + question.getString("answerfile")),
						new File(env.getProperty("file.upload.path") + "questions/" + questionId + "/answerfile/" + question.getString("originalName")));
			}
			logController.log(request, LogTypeEnum.Type20, new String[] { " ", questionId, "1", "0", " ", name });
			return result.toString();
		}
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseBody
	public String updateQustion(@RequestBody JSONObject question, HttpSession session)
			throws ClientProtocolException, IOException {
		int timeout = (int) (question.getDouble("timeout") * 3600);
		question.put("timeout", timeout);
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		JSONArray cates = question.getJSONArray("categories");
		for (int i = 0; i < cates.size(); i++) {
			addCategory(cates.getString(i), account, token);
		}
		JSONObject jo = JSONObject.fromObject(question);
		String flag_score = jo.optString("flag_score");
		if (!StringUtils.isEmpty(flag_score)) {
			jo.put("flag_score", Integer.parseInt(flag_score));
		}
		String questionId = question.getString("id");
		JSONObject result = HttpUtils.put("v1.0/question/" + questionId, jo.toString(), account, env, token);
		if (result.has("exception")) {
			return result.toString();
		} else {
			if (question.has("answerfile") && StringUtils.isNotEmpty(question.getString("answerfile"))) {
				try {
					FileUtils.deleteQuietly(new File(env.getProperty("file.upload.path") + "questions/" + questionId + "/answerfile"));
					FileUtils.copyFile(new File(env.getProperty("file.upload.path") + question.getString("answerfile")),
							new File(env.getProperty("file.upload.path") + "questions/" + questionId + "/answerfile/" + question.getString("originalName")));
				} catch (Exception e) {
				}
			}
			return result.toString();
		}
	}

	@RequestMapping(value = "/category/list", method = RequestMethod.GET)
	@ResponseBody
	public String categories(HttpSession session) throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		return HttpUtils.get("v1.0/question_categories", account, env, token).toString();
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ResponseBody
	@Transactional
	public String deleteQuestion(@RequestBody JSONObject jsonObj, HttpSession session, HttpServletRequest request)
			throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		String questionId = jsonObj.getString("id");
		JSONObject jq = HttpUtils.get("v1.0/question/" + questionId, account, env, token);
		String name = jq.has("question") ? ((JSONObject)jq.get("question")).getString("summary") : "";
		JSONObject jo = HttpUtils.delete("v1.0/question/" + questionId, account, env, token);
		
		if (!jo.has("exception")) {
			logController.log(request, LogTypeEnum.Type21, new String[] { " ", questionId, "1", "0", " ", name});
		}
		
		return jo.toString();
	}

	@RequestMapping(value = "/category/delete", method = RequestMethod.POST)
	@ResponseBody
	public String deleteCategory(@RequestBody JSONObject jsonObj, HttpSession session)
			throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		String cate = jsonObj.getString("label");
		return HttpUtils.delete("v1.0/question_category/" + cate, account, env, token).toString();
	}

	@RequestMapping(value = "/{id}/follows", method = RequestMethod.GET)
	@ResponseBody
	public String follows(@PathVariable("id") String questionid, HttpSession session)
			throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		return HttpUtils.get("v1.0/question/" + questionid + "/followers", account, env, token).toString();
	}

	@RequestMapping(value = "/{id}/follows", method = RequestMethod.POST)
	@ResponseBody
	public String publish(@PathVariable("id") String questionid, @RequestBody JSONObject jsonObj, HttpSession session)
			throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		return HttpUtils.post("v1.0/question/" + questionid + "/followers", jsonObj.toString(), account, env, token)
				.toString();
	}

	@RequestMapping(value = "/networks/{id}", method = RequestMethod.GET)
	@ResponseBody
	public String networksGet(@PathVariable("id") String id, HttpSession session)
			throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		return HttpUtils.get("v1.0/networks?question_id=" + id, account, env, token).toString();
	}

	@RequestMapping(value = "/networks", method = RequestMethod.POST)
	@ResponseBody
	public String networksSave(@RequestBody JSONObject jsonObj, HttpSession session)
			throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		return HttpUtils.post("v1.0/networks", jsonObj.toString(), account, env, token).toString();
	}

	@RequestMapping(value = "/network/{id}", method = RequestMethod.POST)
	@ResponseBody
	public String networksModify(@PathVariable("id") String id, @RequestBody JSONObject jsonObj, HttpSession session)
			throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		return HttpUtils.put("v1.0/network/" + id, jsonObj.toString(), account, env, token).toString();
	}

	@RequestMapping(value = "/network/{networkid}/delete", method = RequestMethod.POST)
	@ResponseBody
	public String networksDelete(@PathVariable("networkid") String networkid, HttpSession session)
			throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		return HttpUtils.delete("v1.0/network/" + networkid, account, env, token).toString();
	}

	@RequestMapping(value = "/routers/{id}", method = RequestMethod.GET)
	@ResponseBody
	public String routers(@PathVariable("id") String id, HttpSession session)
			throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		return HttpUtils.get("v1.0/routers?question_id=" + id, account, env, token).toString();
	}

	@RequestMapping(value = "/routers", method = RequestMethod.POST)
	@ResponseBody
	public String routersSave(@RequestBody JSONObject jsonObj, HttpSession session)
			throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		return HttpUtils.post("v1.0/routers", jsonObj.toString(), account, env, token).toString();
	}

	@RequestMapping(value = "/router/{id}", method = RequestMethod.POST)
	@ResponseBody
	public String routerModify(@PathVariable("id") String id, @RequestBody JSONObject jsonObj, HttpSession session)
			throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		return HttpUtils.put("v1.0/router/" + id, jsonObj.toString(), account, env, token).toString();
	}

	@RequestMapping(value = "/router/{routerid}/delete", method = RequestMethod.POST)
	@ResponseBody
	public String routerDelete(@PathVariable("routerid") String routerid, HttpSession session)
			throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		return HttpUtils.delete("v1.0/router/" + routerid, account, env, token).toString();
	}

	@RequestMapping(value = "/templates/{id}", method = RequestMethod.GET)
	@ResponseBody
	public String templates(@PathVariable("id") String id, HttpSession session)
			throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		JSONObject json = HttpUtils.get("v1.0/templates?question_id=" + id, account, env, token);
		if (json.has("exception")) {
			return json.toString();
		} else {
			JSONArray jarr = json.getJSONArray("templates");
			for (int i = 0; i < jarr.size(); i++) {
				String image = jarr.getJSONObject(i).getString("imageRef");
				JSONObject imgJSON = HttpUtils.get("v1.0/image/" + image, account, env, token);
				jarr.getJSONObject(i).put("image", imgJSON.getJSONObject("image"));
			}
		}
		return json.toString();
	}

	@RequestMapping(value = "/templates", method = RequestMethod.POST)
	@ResponseBody
	public String templatesSave(@RequestBody JSONObject jsonObj, HttpSession session)
			throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		jsonObj.put("disk", Integer.parseInt(jsonObj.optString("disk")));
		return HttpUtils.post("v1.0/templates", jsonObj.toString(), account, env, token).toString();
	}

	@RequestMapping(value = "/template/{id}", method = RequestMethod.POST)
	@ResponseBody
	public String templateModify(@PathVariable("id") String id, @RequestBody JSONObject jsonObj, HttpSession session)
			throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		jsonObj.put("disk", Integer.parseInt(jsonObj.optString("disk")));
		return HttpUtils.put("v1.0/template/" + id, jsonObj.toString(), account, env, token).toString();
	}

	@RequestMapping(value = "/template/{templateid}/delete", method = RequestMethod.POST)
	@ResponseBody
	public String templateDelete(@PathVariable("templateid") String templateid, HttpSession session)
			throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		return HttpUtils.delete("v1.0/template/" + templateid, account, env, token).toString();
	}

	@RequestMapping(value = "/{questionid}/servers/{username}", method = RequestMethod.GET)
	@ResponseBody
	public String servers2(@PathVariable("questionid") String questionid, @PathVariable("username") String username,
			HttpSession session) throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		JSONObject json = HttpUtils.get("v1.0/servers?question_id=" + questionid + "&username=" + username, account,
				env, token);
		return json.toString();
	}

	public void addCategory(String cate, Account account, String token) throws ClientProtocolException, IOException {
		JSONObject jo = new JSONObject();
		jo.put("category", cate);
		HttpUtils.post("v1.0/question_categories", jo.toString(), account, env, token);
	}

	@RequestMapping(value = { "/lesson/{id}/learn" }, method = RequestMethod.POST)
	@ResponseBody
	public String learnLesson(@PathVariable("id") String id, @RequestBody JSONObject jo, HttpSession session)
			throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		return HttpUtils.post("v1.0/lesson/" + id + "/learn", jo.toString(), account, this.env, token).toString();
	}

	@RequestMapping(value = "/flavors", method = RequestMethod.GET)
	@ResponseBody
	public String flavors(HttpSession session) throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		return HttpUtils.get("v1.0/flavors", account, env, token).toString();
	}

	public JSONObject singleImage(HttpSession session, String id) throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		JSONObject jo = HttpUtils.get("v1.0/image/" + id, account, this.env, token);
		return jo;
	}

	@RequestMapping(value = { "/servers/{username}/{question}" }, method = RequestMethod.GET)
	@ResponseBody
	public String servers(@PathVariable("username") String username, @PathVariable("question") String question,
			HttpSession session) throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		JSONObject jo = HttpUtils.get("v1.0/servers?question_id=" + question + "&username=" + username, account,
				this.env, token);
		if (!jo.containsKey("servers")){
			return new JSONObject().toString();
		}
		JSONArray servers = jo.getJSONArray("servers");
		for (int i = 0; i < servers.size(); i++) {
			String imageid = servers.getJSONObject(i).getString("id");
			JSONObject vm = singleServer(session, imageid);
			if (vm != null) {
				servers.getJSONObject(i).put("server", vm);
			} else {
				servers.remove(i);
			}
		}
		return jo.toString();
	}

	@RequestMapping(value = { "/server/{id}/alluser" }, method = RequestMethod.GET)
	@ResponseBody
	public String alluser(@PathVariable("id") String id, HttpSession session)
			throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		JSONObject jo = HttpUtils.get("v1.0/servers?question_id=" + id, account, this.env, token);
		JSONArray jarr = jo.getJSONArray("servers");
		Set<String> users = new HashSet<String>();
		for (int i = 0; i < jarr.size(); i++) {
			JSONObject json = jarr.getJSONObject(i);
			JSONObject vm = singleServer(session, json.getString("id"));
			if (vm != null) {
				json.put("server", vm);
				users.add(json.getString("username"));
			}
		}
		jo.put("users", users);
		return jo.toString();
	}

	public JSONObject singleServer(HttpSession session, String id) throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		JSONObject jo = HttpUtils.get("v1.0/server/" + id, account, this.env, token);
		if (jo.containsKey("error")){
			return null;
		}
		return jo;
	}

	@RequestMapping(value = { "/serverbyuser/{username}" }, method = RequestMethod.GET)
	@ResponseBody
	public String serverbyuser(@PathVariable("username") String username, HttpSession session)
			throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		JSONObject jo = HttpUtils.get("v1.0/servers?username=" + username, account, this.env, token);
		return jo.toString();
	}
	
	@RequestMapping(value = { "/servers" }, method = RequestMethod.GET)
	@ResponseBody
	public String servers(HttpSession session)
			throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		JSONObject jo = HttpUtils.get("v1.0/servers", account, this.env, token);
		return jo.toString();
	}

	@RequestMapping(value = { "/byuser/{username}" }, method = RequestMethod.GET)
	@ResponseBody
	public String byuser(@PathVariable("username") String username, HttpSession session)
			throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		JSONObject jo = HttpUtils.get("v1.0/servers?username=" + username, account, this.env, token);
		if (jo.has("exception")) {
			return jo.toString();
		}
		Set<String> questions = new HashSet<String>();
		JSONArray jarr = jo.getJSONArray("servers");
		JSONArray result = new JSONArray();

		for (int i = 0; i < jarr.size(); i++) {
			JSONObject joo = jarr.getJSONObject(i);
			String qid = joo.getString("question_id");
			if (!questions.contains(qid)) {
				questions.add(qid);
				result.add(joo);
			}
		}


		return result.toString();
	}

	@RequestMapping(value = "/server/{id}", method = RequestMethod.GET)
	@ResponseBody
	public String singleServers(HttpSession session, @PathVariable("id") String id)
			throws ClientProtocolException, IOException {
		return singleServer(session, id).toString();
	}

	@RequestMapping(value = "/start", method = RequestMethod.POST)
	@ResponseBody
	@Transactional
	public String start(@RequestBody JSONObject jsonObj, HttpServletRequest request, HttpSession session)
			throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		JSONObject jo = HttpUtils.post("v1.0/servers", jsonObj.toString(), account, env, token);
		if (jo.has("exception")) {
			logController.log(request, LogTypeEnum.Type3,
					new String[] { jo.getString("exception"), jsonObj.getString("question_id"), "-1" });
		} else {
			logController.log(request, LogTypeEnum.Type3, new String[] { " ", jsonObj.getString("question_id"), "1" });
		}
		return jo.toString();
	}

	@RequestMapping(value = "/stop", method = RequestMethod.POST)
	@ResponseBody
	public String stop(@RequestBody JSONObject jsonObj, HttpServletRequest request, HttpSession session)
			throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		String question = jsonObj.getString("question");
		String username = jsonObj.getString("username");
		System.out.println(jsonObj.toString());
		JSONObject jo = HttpUtils.delete("v1.0/servers?question_id=" + question + "&username=" + username, 
				account, env, token);
		if (jo.has("exception")) {
			logController.log(request, LogTypeEnum.Type4,
					new String[] { jo.getString("exception"), question, "-1" });
		} else {
			logController.log(request, LogTypeEnum.Type4, new String[] { " ", question, "1" });
		}
		return jo.toString();
	}

	@RequestMapping(value = "/stopall", method = RequestMethod.POST)
	@ResponseBody
	@Transactional
	public String stopall(@RequestBody JSONObject jo, HttpSession session, HttpServletRequest request)
			throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		String question = jo.optString("question");
		String username = jo.optString("username");
		JSONArray questions = jo.optJSONArray("questions");
		
		//销毁单个用户的多道题目
		if ((questions != null) && (questions.size() > 0)) {
			for (int i = 0; i < questions.size(); i++) {
				JSONObject result = HttpUtils.delete(
						"v1.0/servers?question_id=" + questions.getString(i) + "&username=" + username, account,
						this.env, token);
				if (result.has("exception"))
					logController.log(request, LogTypeEnum.Type4,
							new String[] { result.getString("exception"), questions.getString(i), "-1" });
				else {
					logController.log(request, LogTypeEnum.Type4, new String[] { "", questions.getString(i) });
				}
			}
			HttpUtils.delete("v1.0/residuals?username=" + username, account, this.env, token);
			return new JSONObject().toString();
		}
		//销毁单个用户的单道题目
		else if (StringUtils.isNotEmpty(username) && StringUtils.isNotEmpty(question)) {
			JSONObject result = HttpUtils.delete("v1.0/servers?question_id=" + question + "&username=" + username,
					account, this.env, token);
			if (result.has("exception"))
				logController.log(request, LogTypeEnum.Type4,
						new String[] { result.getString("exception"), question, "-1" });
			else {
				logController.log(request, LogTypeEnum.Type4, new String[] { "", question });
			}
			return result.toString();
		}
		//销毁所有用户的同一道题目
		else if (StringUtils.isNotEmpty(question)) {
			JSONObject result = HttpUtils.delete("v1.0/servers?question_id=" + question, account, this.env, token);
			if (result.has("exception"))
				logController.log(request, LogTypeEnum.Type4,
						new String[] { result.getString("exception"), question, "-1" });
			else {
				logController.log(request, LogTypeEnum.Type4, new String[] { "", question });
			}
			return result.toString();
		} 
		//销毁用户所有的题目
		else {
			HttpUtils.delete("v1.0/residuals", account, this.env, token);
			return new JSONObject().toString();
		}
	}

	@RequestMapping(value = { "/attackvm/{qid}/{username}" }, method = RequestMethod.GET)
	@ResponseBody
	@Transactional
	public String attackvm(@PathVariable("qid") String qid, @PathVariable("username") String username, HttpServletRequest request, HttpSession session)
			throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		return HttpUtils.get("v1.0/attackvm?question_id=" + qid + "&username=" + username, account, this.env, token).toString();
	}

	@RequestMapping(value = { "/images/{name}" }, method = RequestMethod.GET)
	@ResponseBody
	public String images(@PathVariable("name") String name, HttpSession session)
			throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		return HttpUtils.get("v1.0/images?name=" + name, account, this.env, token).toString();
	}

	@RequestMapping(value = { "/images2/{name}" }, method = RequestMethod.GET)
	@ResponseBody
	public String images2(@PathVariable("name") String name, HttpSession session)
			throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		return HttpUtils.get("v1.0/images?category=" + name, account, this.env, token).toString();
	}

	@RequestMapping(value = { "/answer/{questionId}" }, method = { RequestMethod.POST })
	@ResponseBody
	@Transactional
	public String answer(@PathVariable("questionId") String questionId, @RequestBody JSONObject jo,
			HttpServletRequest request, HttpSession session) throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		JSONObject result = HttpUtils.post("v1.0/question/" + questionId + "/ctf", jo.toString(), account, this.env, token);
		if (result.has("exception")) {
			System.out.println(result.getString("exception"));
			logController.log(request, LogTypeEnum.Type9, new String[] { result.getString("exception"), questionId, "-1" });
		} else {
			if (result.has("score") && result.getInt("score")>0)
				logController.log(request, LogTypeEnum.Type9, new String[] { " ", questionId, "1", result.getString("score") });
			else {
				logController.log(request, LogTypeEnum.Type9, new String[] { " ", questionId, "-1" });
			}
		}
		return result.toString();
	}

	@RequestMapping(value = { "/answerattack/{questionId}/{imageid}/{username}" }, method = RequestMethod.GET)
	@ResponseBody
	@Transactional
	public String answerattack(@PathVariable("questionId") String questionId, @PathVariable("imageid") String imageId,
			@PathVariable("username") String username,
			HttpServletRequest request, HttpSession session) throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		String url = "v1.0/question/" + questionId + "/answer";
		if (!StringUtils.isEmpty(username))
			url += "?username=" + username;
		JSONObject result = HttpUtils.post(url,
				"{\"image_id\":\"" + imageId + "\"}", account, this.env, token);
		if (result.has("exception")){
			logController.log(request, LogTypeEnum.Type5, new String[] { result.getString("exception"), questionId, "-1" });
		} else {
			logController.log(request, LogTypeEnum.Type5, new String[] { "", questionId });
		}
		return result.toString();
	}

	@RequestMapping(value = "/comments/{id}", method = RequestMethod.GET)
	@ResponseBody
	public List<Comment> byQuestion(@PathVariable("id") Long id, HttpSession session)
			throws ClientProtocolException, IOException {
		List<Comment> comments = commentDao.findByQidAndTypeOrderByLogtimeAsc(id, Comment.TYPE_COMMENT);
		for (Comment comment : comments) {
			List<Comment> replys = commentDao.findByParentidOrderByLogtimeAsc(comment.getId());
			comment.setReplys(replys);
		}
		return comments;
	}

	@RequestMapping(value = "/comment", method = RequestMethod.POST)
	@ResponseBody
	@Transactional
	public Comment saveComment(@RequestBody Comment comment, HttpServletRequest request, HttpSession session) {
		Account obj = (Account) session.getAttribute("user");
		comment.setId(UUID.randomUUID().toString());
		comment.setUsername(obj.getUsername());
		comment.setLogtime(new Date());
		commentDao.save(comment);
		logController.log(request, LogTypeEnum.Type8,
				new String[] { " ", comment.getQid() + "", "1", "0", comment.getContent() });
		return comment;
	}

	@RequestMapping(value = { "/{id}/entries" }, method = RequestMethod.GET)
	@ResponseBody
	public String entries(@PathVariable("id") String id, HttpSession session, HttpServletRequest request)
			throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		int usertype = (Integer)session.getAttribute("role");
		String token = (String) session.getAttribute("token");
		if (account.getUsername().equals(env.getProperty("vm.admin.name")) || usertype == UserTypeEnum.GroupUser.getUserType()) {
			String username = request.getParameter("username");
			return HttpUtils.get("v1.0/question/" + id + "/entries?username=" + username, account, this.env, token).toString();
		} else {
			return HttpUtils.get("v1.0/question/" + id + "/entries", account, this.env, token).toString();
		}
	}

}
