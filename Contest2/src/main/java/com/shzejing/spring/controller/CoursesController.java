package com.shzejing.spring.controller;

import java.io.File;
import java.io.IOException;

import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.ClientProtocolException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shzejing.spring.common.HttpUtils;
import com.shzejing.spring.entities.Account;
import com.shzejing.spring.entities.Course;

import net.sf.json.JSONObject;

@Controller
@Transactional(readOnly = true)
@RequestMapping("/services/courses")
public class CoursesController {

	@Autowired
	private Environment env;

	@RequestMapping(value = "/listall", method = RequestMethod.GET)
	@ResponseBody
	public String listall(@RequestParam(required = false, value = "difficult", defaultValue = "") String difficult,
			HttpSession session) throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		return HttpUtils.get("v1.0/lessons?difficult=" + difficult, account, env, token).toString();
	}

	@RequestMapping(value = "/lesson/category/list", method = RequestMethod.GET)
	@ResponseBody
	public String lessonCategory(HttpSession session) throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		return HttpUtils.get("v1.0/question_categories", account, env, token).toString();
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ResponseBody
	public String deleteCourse(@RequestParam(value = "id") String id, HttpSession session)
			throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		return HttpUtils.delete("v1.0/lesson/" + id, account, env, token).toString();
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ResponseBody
	public String get(@PathVariable("id") String id, HttpSession session) throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		JSONObject jo = HttpUtils.get("v1.0/lesson/" + id, account, env, token);
		File path = new File(env.getProperty("file.upload.path") + "courses/" + id + "/video/");
		if (path != null && path.list() != null && path.list().length > 0) {
			String filename = path.list()[0];
			jo.put("video", "courses/" + id + "/video/" + FilenameUtils.getName(filename));
		}
		File picture = new File(env.getProperty("file.upload.path") + "courses/" + id + "/index");
		if (picture.exists()) {
			jo.put("picture", "courses/" + id + "/index");
		}
		return jo.toString();
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@ResponseBody
	public String saveCourse(@RequestBody Course course, HttpSession session)
			throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		JSONObject jo = new JSONObject();
		jo.put("title", course.getTitle());
		jo.put("difficult", course.getDifficult());
		jo.put("introduction", course.getIntroduction());
		jo.put("questions", course.getQuestions());

		JSONObject result = HttpUtils.post("v1.0/lessons", jo.toString(), account, env, token);
		if (result.has("exception")) {
			return result.toString();
		} else {
			String lessonId = result.getJSONObject("lession").getString("id");
			if (StringUtils.isNotEmpty(course.getPic())) {
				FileUtils.copyFile(new File(env.getProperty("file.upload.path") + course.getPic()),
						new File(env.getProperty("file.upload.path") + "courses/" + lessonId + "/index"));
			}
			if (StringUtils.isNotEmpty(course.getVideo())) {
				FileUtils.deleteQuietly(new File(env.getProperty("file.upload.path") + "courses/" + lessonId + "/video/"));
				FileUtils.copyFile(new File(env.getProperty("file.upload.path") + course.getVideo()),
						new File(env.getProperty("file.upload.path") + "courses/" + lessonId + "/video/"
								+ FilenameUtils.getName(course.getVideo())));
			}
			return result.toString();
		}

	}

	@RequestMapping(value = "/{id}", method = RequestMethod.POST)
	@ResponseBody
	public String updateCourse(@PathVariable("id") String id, @RequestBody Course course, HttpSession session)
			throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		JSONObject jo = new JSONObject();
		jo.put("title", course.getTitle());
		jo.put("difficult", course.getDifficult());
		jo.put("introduction", course.getIntroduction());
		jo.put("questions", course.getQuestions());

		JSONObject result = HttpUtils.put("v1.0/lesson/" + id, jo.toString(), account, env, token);
		if (result.has("exception")) {
			return result.toString();
		} else {
			if (StringUtils.isNotEmpty(course.getPic()) && !course.getPic().equals("courses/" + id + "/index")) {
				FileUtils.copyFile(new File(env.getProperty("file.upload.path") + course.getPic()),
						new File(env.getProperty("file.upload.path") + "courses/" + id + "/index"));
			}
			if (StringUtils.isNotEmpty(course.getVideo()) && !course.getVideo()
					.equals("courses/" + id + "/video/" + FilenameUtils.getName(course.getVideo()))) {
				FileUtils.deleteQuietly(new File(env.getProperty("file.upload.path") + "courses/" + id + "/video/"));
				FileUtils.copyFile(new File(env.getProperty("file.upload.path") + course.getVideo()),
						new File(env.getProperty("file.upload.path") + "courses/" + id + "/video/"
								+ FilenameUtils.getName(course.getVideo())));
			}
			return result.toString();
		}
	}

}
