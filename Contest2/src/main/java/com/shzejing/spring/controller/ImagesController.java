package com.shzejing.spring.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.ClientProtocolException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shzejing.spring.common.HttpUtils;
import com.shzejing.spring.common.LogTypeEnum;
import com.shzejing.spring.entities.Account;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONException;

@Controller
@Transactional(readOnly = true)
@RequestMapping("/services/images")
public class ImagesController {

	@Autowired
	private Environment env;

	@Autowired
	private LogController logController;

	@RequestMapping(value = "/listall", method = RequestMethod.GET)
	@ResponseBody
	public String listall(@RequestParam(required = false, value = "category", defaultValue = "") String category,
			HttpSession session) throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		if (StringUtils.isEmpty(category)) {
			return HttpUtils.get("v1.0/images", account, env, token).toString();
		} else {
			return HttpUtils.get("v1.0/images?category=" + category, account, env, token).toString();
		}
	}
	
	@RequestMapping(value = "/list2all", method = RequestMethod.POST)
	@ResponseBody
	public String cateslist(@RequestBody JSONObject data, HttpSession session)
			throws ClientProtocolException, IOException {
		String cate = data.optString("category", "");
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		return HttpUtils.get("v1.0/images?category=" + cate, account, env, token).toString();
	}

	@RequestMapping(value = "/categories", method = RequestMethod.GET)
	@ResponseBody
	public String categories(HttpSession session) throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		return HttpUtils.get("v1.0/image_categories", account, env, token).toString();
	}

	@RequestMapping(value = "/category/delete", method = RequestMethod.POST)
	@ResponseBody
	public String categoryDelete(@RequestBody JSONObject json, HttpSession session)
			throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		String label = json.getString("label");
		return HttpUtils.delete("v1.0/image_category/" + label, account, env, token).toString();
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ResponseBody
	public String single(@PathVariable("id") String id, HttpSession session)
			throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		return HttpUtils.get("v1.0/image/" + id, account, env, token).toString();
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.POST)
	@ResponseBody
	@Transactional
	public String update(@PathVariable("id") String id, @RequestBody JSONObject image, HttpSession session)
			throws ClientProtocolException, IOException, InterruptedException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		JSONArray cates = image.getJSONArray("categories");
		for (int i = 0; i < cates.size(); i++) {
			addCategory(cates.getString(i), account, token);
		}
		return HttpUtils.post("v1.0/image/" + id, image.toString(), account, env, token).toString();
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ResponseBody
	public String deleteImage(@RequestParam(value = "id") String id, HttpSession session)
			throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		return HttpUtils.delete("v1.0/image/" + id, account, env, token).toString();
	}

	public void addCategory(String cate, Account account, String token) throws ClientProtocolException, IOException {
		JSONObject jo = new JSONObject();
		jo.put("category", cate);
		HttpUtils.post("v1.0/image_categories", jo.toString(), account, env, token);
	}

	@RequestMapping(value = "/vm/list/{id}", method = RequestMethod.GET)
	@ResponseBody
	public String vmlist(@PathVariable("id") String id, HttpSession session)
			throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		JSONObject result = HttpUtils.get("v1.0/servers?image_id=" + id, account, env, token);
		JSONArray arr = result.getJSONArray("servers");
		for (int i = 0; i < arr.size(); i++) {
			JSONObject jo = arr.getJSONObject(i);
			String vmid = jo.getString("id");
			JSONObject vmjo = HttpUtils.get("v1.0/server/" + vmid, account, env, token);
			if (vmjo.has("server")) {
				JSONObject serverjo = vmjo.getJSONObject("server");
				if (serverjo != null) {
					jo.put("created", serverjo.get("created"));
					jo.put("created_ts", serverjo.get("created_ts"));
					jo.put("vmname", serverjo.get("name"));
					jo.put("power_state", serverjo.get("power_state"));
					jo.put("status", serverjo.get("status"));
				}
			} else {
				arr.remove(i);
			}

		}
		return result.toString();
	}

	@RequestMapping(value = "/vm/vnc/{id}", method = RequestMethod.GET)
	@ResponseBody
	public String vnc(@PathVariable("id") String id, HttpSession session) throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		return HttpUtils.post("v1.0/server/" + id + "/action", "{\"action\":\"vnc-console\"}", account, env, token).toString();
	}

	@RequestMapping(value = "/vm/log/{id}", method = RequestMethod.GET)
	@ResponseBody
	public String vmlog(@PathVariable("id") String id, HttpSession session)
			throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		return HttpUtils.post("v1.0/server/" + id + "/action", "{\"action\":\"console-log\"}", account, env, token).toString();
	}

	@RequestMapping(value = "/vm/operate/{id}", method = RequestMethod.GET)
	@ResponseBody
	public String vmoperate(@PathVariable("id") String id, @RequestParam("action") String action, HttpSession session)
			throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		return HttpUtils.post("v1.0/server/" + id + "/action", "{\"action\":\"" + action + "\"}", account, env, token)
				.toString();
	}

	@RequestMapping(value = "/{id}/verify", method = RequestMethod.POST)
	@ResponseBody
	@Transactional
	public String verify(@PathVariable("id") String id, @RequestBody JSONObject image, HttpServletRequest request,
			HttpSession session) throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		JSONObject jo = HttpUtils.get("v1.0/image/" + id, account, env, token);
		String imageName = jo.getJSONObject("image").getString("name");
		
		JSONObject result = HttpUtils.post("v1.0/image/" + id + "/verify", image.toString(), account, env, token);
		if (result.has("exception"))
			logController.log(request, LogTypeEnum.Type6,
					new String[] { result.getString("exception"), "0", "-1", "0", imageName });
		else {
			logController.log(request, LogTypeEnum.Type6, new String[] { " ", "0", "0", "0", imageName });
		}
		return result.toString();
	}

	@RequestMapping(value = "/vm/delete/{id}", method = RequestMethod.POST)
	@ResponseBody
	@Transactional
	public String deleteVM(@PathVariable("id") String id, HttpServletRequest request, HttpSession session)
			throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		JSONObject jo = HttpUtils.get("v1.0/server/" + id, account, env, token);
		String imageName = "";
		try {
			imageName = jo.getJSONObject("server").getString("name");
		} catch (JSONException e){
		}
		
		JSONObject result = HttpUtils.delete("v1.0/server/" + id, account, env, token);
		if (result.has("exception"))
			logController.log(request, LogTypeEnum.Type7,
					new String[] { result.getString("exception"), "0", "-1", "0", imageName });
		else {
			logController.log(request, LogTypeEnum.Type7, new String[] { " ", "0", "0", "0", imageName });
		}
		return result.toString();
	}
}
