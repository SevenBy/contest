package com.shzejing.spring.controller;

import com.shzejing.spring.common.HttpUtils;
import com.shzejing.spring.common.LogTypeEnum;
import com.shzejing.spring.common.RequestPage;
import com.shzejing.spring.common.UserTypeEnum;
import com.shzejing.spring.dao.LogDao;
import com.shzejing.spring.entities.Account;
import com.shzejing.spring.entities.Log;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.apache.http.client.ClientProtocolException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.*;

@Controller
@Transactional(readOnly = true)
@RequestMapping("/services")
public class LogController {

	@Autowired
	private LogDao logDao;
	@Autowired
	Environment env;

	@RequestMapping(value = "/log/list", method = RequestMethod.POST)
	@ResponseBody
	public Page<Log> loglist(@RequestBody RequestPage page, HttpServletRequest request, HttpSession session)
			throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		int usertype = (Integer)session.getAttribute("role");
		Page<Log> result;
		String select_username;
		List<String> usernames = new ArrayList<String>();

		if (usertype == UserTypeEnum.AdminUser.getUserType()) {  //管理员
			if (StringUtils.isNotEmpty(request.getParameter("username"))) {
				select_username = request.getParameter("username");
			} else{
				select_username = "";
			}
			if (page.getLogtype() == 0) {
				result = logDao.findByUsernameLike("%" + select_username + "%", page.toPageable());
			} else {
				result = logDao.findByUsernameLikeAndType("%" + select_username + "%", page.getLogtype(), page.toPageable());
			}
		} else if(usertype == UserTypeEnum.GroupUser.getUserType()){  //组用户
			if (StringUtils.isNotEmpty(request.getParameter("username"))) {
				select_username = request.getParameter("username");
			} else{
				select_username = "";
			}
			String token = (String) session.getAttribute("token");
			JSONArray users = HttpUtils.get("/v1.0/user/" + account.getUsername() + "/groupusers", account, this.env, token).optJSONArray("groupusers");
			for (Object user: users) {
				JSONObject u = JSONObject.fromObject(user);
				usernames.add(u.get("username").toString());
			}
			usernames.add(account.getUsername());
			if (page.getLogtype() == 0) {
				result = logDao.findByUsernameInAndUsernameLike(usernames, "%" + select_username + "%", page.toPageable());
			} else {
				result = logDao.findByUsernameInAndUsernameLikeAndType(usernames, "%" + select_username + "%", page.getLogtype(), page.toPageable());
			}
		} else{  //其他（地方）用户
			select_username = account.getUsername();
			usernames.add(select_username);
			if (page.getLogtype() == 0) {
				result = logDao.findByUsernameInAndUsernameLike(usernames, "%" + select_username + "%", page.toPageable());
			} else {
				result = logDao.findByUsernameInAndUsernameLikeAndType(usernames, "%" + select_username + "%", page.getLogtype(), page.toPageable());
			}
		}
		Map<Long, String> questionMap = new HashMap<Long, String>();

		List<Log> logs = result.getContent();
		for (Log log : logs) {
			Long qid = log.getQid();
			if (qid != null && qid > 0) {
				if (questionMap.containsKey(qid)) {
					log.setQstName(questionMap.get(qid));
				} else {
					questionMap.put(qid, question(qid + "", session));
					log.setQstName(questionMap.get(qid));
				}
			}
		}
		return result;
	}

	private String question(String questionid, HttpSession session) throws ClientProtocolException, IOException {
		Account account = new Account();
		account.setUsername(env.getProperty("vm.admin.name"));
		account.setUserpass(env.getProperty("vm.admin.pass"));
		String token = (String) session.getAttribute("token");
		JSONObject jo = HttpUtils.get("v1.0/question/" + questionid, account, this.env, token);
		try{
			return jo.getJSONObject("question").getString("summary");
		}catch (Exception e) {
		}
		return "";
	}

	@RequestMapping(value = "/log/answer/{qid}", method = RequestMethod.GET)
	@ResponseBody
	public List<Log> answerLog(@PathVariable("qid") Long qid, HttpServletRequest request) {
		return logDao.findAnswerLog(qid);
	}

	@RequestMapping(value = "/logtype/list", method = RequestMethod.GET)
	@ResponseBody
	public String logtypelist(HttpServletRequest request, HttpSession session) {
		JSONArray jarr = new JSONArray();
		Account account = (Account)session.getAttribute("user");
		jarr.add(jo("登录", 1));
		jarr.add(jo("登出", 2));
		jarr.add(jo("登录比赛", 10));
		jarr.add(jo("登出比赛", 11));
		jarr.add(jo("启动题目", 3));
		jarr.add(jo("销毁题目", 4));
		jarr.add(jo("启动攻击实例", 5));
		jarr.add(jo("启动实例", 6));
		jarr.add(jo("销毁实例", 7));
		jarr.add(jo("提交旗帜文件", 9));
		jarr.add(jo("评论", 8));
		
		if (account.getUsername().equals(env.getProperty("vm.admin.name"))) {
			jarr.add(jo("创建题目", 20));
			jarr.add(jo("删除题目", 21));
			jarr.add(jo("创建用户", 30));
			jarr.add(jo("删除用户", 31));
		}
		
		return jarr.toString();
	}

	public JSONObject jo(String name, int value) {
		JSONObject j = new JSONObject();
		j.put("name", name);
		j.put("value", value);
		return j;
	}

	/**
	 * 1.errorinfo, 2.Qid, 3.state, 4.score, 5.servername
	 * 
	 * @param request
	 * @param account
	 * @param type
	 * @param state
	 */
	@Transactional
	public void log(HttpServletRequest request, LogTypeEnum type, Account account, String[] state) {
		String ip = request.getHeader("X-real-ip");
		if (StringUtils.isEmpty(ip)) {
			ip = request.getHeader("x-forwarded-for");
		}
		if (StringUtils.isEmpty(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (StringUtils.isEmpty(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (StringUtils.isEmpty(ip))
			ip = request.getRemoteAddr();
		try {
			Log log = new Log();
			log.setId(UUID.randomUUID().toString());
			log.setIp(ip);
			log.setLogtime(new Date());
			log.setType(type.getCode().intValue());
			log.setUsername(account.getUsername());
			if ((state != null) && (state.length > 0)) {
				log.setErrorinfo(state[0]);
			}
			if ((state != null) && (state.length > 1)) {
				log.setQid(Long.valueOf(Long.parseLong(state[1])));
			}
			if ((state != null) && (state.length > 2)) {
				log.setState(Integer.parseInt(state[2]));
			}
			if ((state != null) && (state.length > 3)) {
				log.setScore(Integer.parseInt(state[3]));
			}
			if ((state != null) && (state.length > 4)) {
				log.setServername(state[4]);
			}
			if ((state != null) && (state.length > 5)) {
				log.setExtra(state[5]);
			}
			this.logDao.save(log);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 1.errorinfo, 2.Qid, 3.state, 4.score, 5.servername
	 * 
	 * @param request
	 * @param type
	 * @param state
	 */
	@Transactional
	public void log(HttpServletRequest request, LogTypeEnum type, String[] state) {
		try {
			HttpSession session = request.getSession();
			Account account = (Account) session.getAttribute("user");
			this.log(request, type, account, state);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
