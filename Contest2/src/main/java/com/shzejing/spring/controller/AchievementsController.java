package com.shzejing.spring.controller;

import com.shzejing.spring.common.HttpUtils;
import com.shzejing.spring.entities.Account;
import org.apache.http.client.ClientProtocolException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.io.IOException;

@Controller
@Transactional(readOnly = true)
@RequestMapping("/services/achievements")
public class AchievementsController {

	@Autowired
	private Environment env;

	@RequestMapping(value = "/availableusers", method = RequestMethod.GET)
	@ResponseBody
	public String availableUsers(@RequestParam(required = false, value = "password", defaultValue = "") String username,
								 HttpSession session) throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		return HttpUtils.get("v1.0/achievements/availableusers", account, env, token).toString();
	}

	@RequestMapping(value = "/userallsituation", method = RequestMethod.GET)
	@ResponseBody
		public String userAllSituation(@RequestParam(required = false, value = "username", defaultValue = "") String username,
									   HttpSession session) throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		return HttpUtils.get("v1.0/achievements/userallsituation?username=" + username, account, env, token).toString();
	}

	@RequestMapping(value = "/alluserallsituation", method = RequestMethod.GET)
	@ResponseBody
	public String userAllSituation(HttpSession session) throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		return HttpUtils.get("v1.0/achievements/alluserallsituation", account, env, token).toString();
	}

	@RequestMapping(value = "/questionsusersolved", method = RequestMethod.GET)
	@ResponseBody
		public String questionsUserSolved(@RequestParam(required = false, value = "id", defaultValue = "") String questionid,
										  @RequestParam(required = false, value = "username", defaultValue = "") String username,
										  @RequestParam(required = false, value = "category", defaultValue = "") String category,
										  @RequestParam(required = false, value = "difficult", defaultValue = "") String difficult,
										  HttpSession session) throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		return HttpUtils.get("v1.0/achievements/questionsusersolved?username=" + username +
				"&category=" + category + "&difficult=" + difficult + "&id=" + questionid, account, env, token).toString();
	}

	@RequestMapping(value = "/userquestionssolved", method = RequestMethod.GET)
	@ResponseBody
	public String userQuestionsSolved(@RequestParam(required = false, value = "username", defaultValue = "") String username,
									  HttpSession session) throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		return HttpUtils.get("v1.0/achievements/userquestionssolved?username=" + username, account, env, token).toString();
	}

	@RequestMapping(value = "/lessonsuserprogress", method = RequestMethod.GET)
	@ResponseBody
		public String lessonsUserProgress(@RequestParam(required = false, value = "username", defaultValue = "") String username,
										  @RequestParam(required = false, value = "difficult", defaultValue = "") String difficult,
										  HttpSession session) throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		return HttpUtils.get("v1.0/achievements/lessonsuserprogress?username=" + username +
				"&difficult=" + difficult, account, env, token).toString();
	}

	@RequestMapping(value = "/contestsusersituation", method = RequestMethod.GET)
	@ResponseBody
		public String contestsUserSituation(@RequestParam(required = false, value = "username", defaultValue = "") String username,
											 HttpSession session) throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		return HttpUtils.get("v1.0/achievements/contestsusersituation?username=" + username,
				account, env, token).toString();
	}

}
