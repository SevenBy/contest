package com.shzejing.spring.controller;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpSession;

import org.apache.http.client.ClientProtocolException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shzejing.spring.common.HttpUtils;
import com.shzejing.spring.entities.Account;
import com.shzejing.spring.entities.Contest;

import net.sf.json.JSONObject;
import net.sf.json.JSONArray;

@Controller
@Transactional(readOnly = true)
@RequestMapping("/services/contests")
public class ContestsController {

	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	@Autowired
	private Environment env;

	@RequestMapping(value = "/listall", method = RequestMethod.GET)
	@ResponseBody
	public String listall(HttpSession session) throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		return HttpUtils.get("v1.0/contests", account, env, token).toString();
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ResponseBody
	public String one(@PathVariable("id") int id, HttpSession session) throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		JSONObject contest = HttpUtils.get("v1.0/contest/" + id, account, env, token);
		if (contest.has("contest")) {
			JSONObject jocontest = contest.getJSONObject("contest");
			long start = jocontest.getLong("start") * 1000;
			long end = jocontest.getLong("end") * 1000;
			JSONArray question_scores = jocontest.getJSONArray("question_scores");
			JSONArray questions = jocontest.getJSONArray("questions");
			jocontest.put("startstr", sdf.format(new Date(start)));
			jocontest.put("endstr", sdf.format(new Date(end)));			
			if (questions != null && question_scores != null) {
				for(int i = 0; i < questions.size(); i++) {
					JSONObject joques = questions.getJSONObject(i);
					Object score = question_scores.get(i);
					joques.put("flag_score", score);
				}
			}
			return contest.toString();
		} else {
			return contest.toString();
		}
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@ResponseBody
	public String saveContest(@RequestBody Contest contest, HttpSession session)
			throws ClientProtocolException, IOException, ParseException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");

		JSONObject jo = JSONObject.fromObject(contest);
		if (jo.has("startstr")) {
			jo.put("start", sdf.parse(jo.getString("startstr")).getTime() / 1000);
			jo.remove("startstr");
		}
		if (jo.has("endstr")) {
			jo.put("end", sdf.parse(jo.getString("endstr")).getTime() / 1000);
			jo.remove("endstr");
		}
		JSONObject result = HttpUtils.post("v1.0/contests", jo.toString(), account, env, token);
		return result.toString();
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseBody
	public String updateContest(@RequestBody Contest contest, HttpSession session)
			throws ClientProtocolException, IOException, ParseException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");

		JSONObject jo = JSONObject.fromObject(contest);
		if (jo.has("startstr")) {
			jo.put("start", sdf.parse(jo.getString("startstr")).getTime() / 1000);
			jo.remove("startstr");
		}
		if (jo.has("endstr")) {
			jo.put("end", sdf.parse(jo.getString("endstr")).getTime() / 1000);
			jo.remove("endstr");
		}
		JSONObject result = HttpUtils.put("v1.0/contest/" + jo.getInt("id"), jo.toString(), account, env, token);
		return result.toString();
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ResponseBody
	public String deleteContest(@RequestParam(value = "id") String id, HttpSession session)
			throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		return HttpUtils.delete("v1.0/contest/" + id, account, env, token).toString();
	}

}
