package com.shzejing.spring.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.http.client.ClientProtocolException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shzejing.spring.common.HttpUtils;
import com.shzejing.spring.common.LogTypeEnum;
import com.shzejing.spring.entities.Account;
import com.shzejing.spring.entities.User;

import net.sf.json.JSONObject;

@Controller
@Transactional(readOnly = true)
@RequestMapping("/services/accounts")
public class AccountsController {

	@Autowired
	private Environment env;
	
	@Autowired
	private LogController logController;
	
	@RequestMapping(value = "/current", method = RequestMethod.GET)
	@ResponseBody
	public String current(HttpSession session) throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		return HttpUtils.get("v1.0/self", account, env, token).toString();
	}

	@RequestMapping(value = "/availableusers", method = RequestMethod.GET)
	@ResponseBody
	public String availableUsers(HttpSession session) throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		return HttpUtils.get("v1.0/achievements/availableusers", account, env, token).toString();
	}

	@RequestMapping(value = "/listall", method = RequestMethod.GET)
	@ResponseBody
	public String listall(HttpSession session) throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		return HttpUtils.get("v1.0/users", account, env, token).toString();
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@ResponseBody
	@Transactional
	public String saveUser(@RequestBody User user, HttpSession session, HttpServletRequest request) 
			throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		JSONObject jo = JSONObject.fromObject(user);
		JSONObject result = HttpUtils.post("v1.0/users", jo.toString(), account, env, token);
		if (!result.has("exception")) {
			logController.log(request, LogTypeEnum.Type30, new String[] { " ", "-1", "1", "0", " ", 
					String.format("%s (%s)", user.getRealname(), user.getUsername())});
		}
		
		return result.toString();
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseBody
	public String updateUser(@RequestBody JSONObject user, HttpSession session) throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		System.out.println(user.toString());;
		return HttpUtils.put("v1.0/user/" + user.getString("username"), user.toString(), account, env, token).toString();
	}

	@RequestMapping(value = "/password/{username}", method = RequestMethod.POST)
	@ResponseBody
	public String updatePass(@RequestBody JSONObject json, @PathVariable("username") String username,
			HttpSession session) throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		return HttpUtils.put("v1.0/user/" + username, json.toString(), account, env, token).toString();
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ResponseBody
	@Transactional
	public String deleteUser(@RequestBody JSONObject jsonObj, HttpSession session, HttpServletRequest request)
			throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		String userId = jsonObj.getString("id");
		JSONObject jo = HttpUtils.delete("v1.0/user/" + userId, account, env, token);
		
		if (!jo.has("exception")) {
			logController.log(request, LogTypeEnum.Type31, new String[] { " ", "-1", "1", "0", " ", userId});
		}
		return jo.toString();
	}

	@RequestMapping(value = "/{id}/questions", method = RequestMethod.POST)
	@ResponseBody
	public String questions(@PathVariable(value = "id") String id, @RequestBody JSONObject questions,
			HttpSession session) throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		return HttpUtils.post("v1.0/user/" + id + "/questions", questions.toString(), account, env, token).toString();
	}

	@RequestMapping(value = "/{id}/questions", method = RequestMethod.GET)
	@ResponseBody
	public String questions(@PathVariable(value = "id") String id, HttpSession session)
			throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		return HttpUtils.get("v1.0/user/" + id + "/questions", account, env, token).toString();
	}

	@RequestMapping(value = "/{id}/groupusers", method = RequestMethod.POST)
	@ResponseBody
	public String groupusers(@PathVariable(value = "id") String id, @RequestBody JSONObject userGroup,
			HttpSession session) throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		return HttpUtils.post("v1.0/user/" + id + "/groupusers", userGroup.toString(), account, env, token).toString();
	}

	@RequestMapping(value = "/{id}/groupusers", method = RequestMethod.GET)
	@ResponseBody
	public String groupusers(@PathVariable(value = "id") String id, HttpSession session)
			throws ClientProtocolException, IOException {
		Account account = (Account) session.getAttribute("user");
		String token = (String) session.getAttribute("token");
		return HttpUtils.get("v1.0/user/" + id + "/groupusers", account, env, token).toString();
	}
}
