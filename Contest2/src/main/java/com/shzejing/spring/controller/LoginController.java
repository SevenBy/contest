package com.shzejing.spring.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shzejing.spring.common.HttpUtils;
import com.shzejing.spring.common.LogTypeEnum;
import com.shzejing.spring.common.RES;
import com.shzejing.spring.common.UserTypeEnum;
import com.shzejing.spring.entities.Account;

import net.sf.json.JSONObject;

@Controller
@Transactional(readOnly = true)
@RequestMapping("/services")
public class LoginController {

	@Autowired
	private LogController loginController;

	@Autowired
	private Environment env;

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	@ResponseBody
	@Transactional
	public RES login(@RequestBody Account account, HttpSession session, HttpServletRequest request)
			throws ClientProtocolException, IOException {
		JSONObject json = post("v1.0/login", null, account, env);
		if (json.containsKey("token") && StringUtils.isNotEmpty(json.getString("token"))) {
			int type = json.containsKey("type") ? json.getInt("type") : UserTypeEnum.NormalUser.getUserType();
			session.setAttribute("token", json.getString("token"));
			session.setAttribute("user", account);
			if (json.containsKey("contest"))
				session.setAttribute("login_contest", true);
			else
				session.setAttribute("login_contest", false);
			session.setAttribute("role", type);
			
			this.loginController.log(request, LogTypeEnum.Type1, new String[0]);
			return RES.OK(json);
		} else {
			String error = json.getString("exception");
			this.loginController.log(request, LogTypeEnum.Type1, account, new String[]{ error, "0", "-1" });
			return RES.ERROR(error);
		}
	}
	
	private JSONObject post(String url, String body, Account account, Environment env)
			throws ClientProtocolException, IOException {
		DefaultHttpClient httpclient = new DefaultHttpClient();
		UsernamePasswordCredentials credent;
		credent = new UsernamePasswordCredentials(account.getUsername(), account.getUserpass());
		HttpPost httpPost = new HttpPost(env.getProperty("vm.user.url") + url);
		httpclient.getCredentialsProvider().setCredentials(AuthScope.ANY, credent);
		if (StringUtils.isNotEmpty(body)) {
			StringEntity entity = new StringEntity(body, "utf-8");// 解决中文乱码问题
			entity.setContentEncoding("UTF-8");
			entity.setContentType("application/json");
			httpPost.setEntity(entity);
		}

		HttpResponse res = httpclient.execute(httpPost);
		if (res.getStatusLine().getStatusCode() == 200) {
			JSONObject o = JSONObject.fromObject(EntityUtils.toString(res.getEntity()));
			return o;
		} else {
			JSONObject o = new JSONObject();
			o.put("exception", "错误的用户名密码");
			return o;
		}
	}
	
	@RequestMapping(value = "/login2", method = RequestMethod.POST)
	@ResponseBody
	@Transactional
	public RES login2(@RequestBody Account account, HttpSession session, HttpServletRequest request)
			throws ClientProtocolException, IOException {
		JSONObject json = HttpUtils.get("v1.0/contest/current", account, env, "");
		if (json.containsKey("contest")){
			String contest = json.getString("contest");
			if(StringUtils.isNotEmpty(contest) && contest != "null") {
				session.setAttribute("user", account);
				session.setAttribute("login_contest", true);
				session.setAttribute("role", UserTypeEnum.NormalUser.getUserType());
				this.loginController.log(request, LogTypeEnum.Type10, new String[0]);
				return RES.OK(json);
			} else {
				this.loginController.log(request, LogTypeEnum.Type10, account, new String[]{ "当前用户没有比赛", "0", "-1" });
				return RES.ERROR("当前用户没有比赛");
			}
		} else {
			String error = json.getString("exception");
			this.loginController.log(request, LogTypeEnum.Type10, account, new String[]{ error, "0", "-1" });
			return RES.ERROR(error);
		}
	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	@ResponseBody
	@Transactional
	public RES logout(HttpSession session, HttpServletRequest request)
			throws ClientProtocolException, IOException {
		boolean login_contest = (Boolean)session.getAttribute("login_contest");
		removeToken((String)session.getAttribute("token"));
		loginController.log(request, login_contest ? LogTypeEnum.Type11 : LogTypeEnum.Type2, new String[0]);
		session.removeAttribute("user");
		session.removeAttribute("token");
		return RES.OK();
	}
	
	private void removeToken(String token) 
			throws ClientProtocolException, IOException {
		DefaultHttpClient httpclient = new DefaultHttpClient();
		HttpPost httpPost = new HttpPost(env.getProperty("vm.user.url") + "v1.0/logout");
		httpPost.setHeader("X-Auth-Token", token);
		httpclient.execute(httpPost);
	}
	
	public static void main(String[] args) {
		char[] rtn = "\r\n".toCharArray();
        StringBuffer strBuffer = new StringBuffer();
        for (int i=0;i< rtn.length;i++){
            strBuffer.append(Integer.toHexString(rtn[i]).toUpperCase());
        }
        System.out.println(strBuffer);
	}
}
