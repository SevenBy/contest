package com.shzejing.spring.controller;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.http.client.ClientProtocolException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import net.sf.json.JSONObject;

@Controller
@Transactional(readOnly = true)
@RequestMapping("/services/fileupload")
public class FileuploadController {

	@Autowired
	private Environment env;

	@RequestMapping(value = "", method = RequestMethod.POST)
	@ResponseBody
	public String upload(@RequestParam("upfile") MultipartFile multipartFile)
			throws ClientProtocolException, IOException {
		String originalFilename = multipartFile.getOriginalFilename(); // 文件全名
		String suffix = StringUtils.substringAfter(originalFilename, "."); // 后缀
		String file = new Date().getTime() + "." + suffix;
		SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd");
		File path = new File(env.getProperty("file.upload.path") + fmt.format(new Date()));
		if (!path.exists()) {
			path.mkdirs();
		}
		FileUtils.copyInputStreamToFile(multipartFile.getInputStream(),
				new File(env.getProperty("file.upload.path") + fmt.format(new Date()) + "/" + file));
		JSONObject jo = new JSONObject();
		jo.put("originalName", originalFilename);
		jo.put("name", file);
		jo.put("type", suffix);
		jo.put("state", "SUCCESS");
		jo.put("url", fmt.format(new Date()) + "/" + file);
		return jo.toString();
	}

}
