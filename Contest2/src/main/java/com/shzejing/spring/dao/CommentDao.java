package com.shzejing.spring.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.shzejing.spring.entities.Comment;

public interface CommentDao extends JpaRepository<Comment, String> {

	List<Comment> findByQidAndTypeOrderByLogtimeAsc(Long id, int typeComment);

	List<Comment> findByParentidOrderByLogtimeAsc(String id);


}
