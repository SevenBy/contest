package com.shzejing.spring.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.shzejing.spring.entities.Log;

public interface LogDao extends JpaRepository<Log, String> {

	Page<Log> findByUsernameLike(String username, Pageable pageable);

	Page<Log> findByUsernameInAndUsernameLike(List<String> users, String username, Pageable pageable);

	Page<Log> findByUsernameInAndUsernameLikeAndType(List<String> users, String username, int logtype, Pageable pageable);

	Page<Log> findByUsernameLikeAndType(String string, int logtype, Pageable pageable);

	@Query(nativeQuery = true, value = "select w.* from ws_log w where w.question_id = :qid and w.type in (3,4,5,6,7,8,9,20,21) order by w.timestamp desc")
	List<Log> findAnswerLog(@Param("qid") Long qid);

}
