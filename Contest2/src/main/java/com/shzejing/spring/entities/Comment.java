package com.shzejing.spring.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name = "ws_comment")
public class Comment implements Serializable {

	private static final long serialVersionUID = 2L;
	
	public static final int TYPE_COMMENT = 0;
	public static final int TYPE_REPLT = 1;

	@Id
	private String id;

	@Column(name = "timestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date logtime;

	@Column(name = "username")
	private String username;

	@Column(name = "replayname")
	private String replayname;

	@Column(name = "type")
	private int type;

	@Column(name = "question_id")
	private Long qid;

	@Column(name = "content", length = 65535)
	private String content;
	
	@Column(name = "parentid")
	private String parentid;
	
	@Transient
	private List<Comment> replys;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getLogtime() {
		return logtime;
	}

	public void setLogtime(Date logtime) {
		this.logtime = logtime;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getReplayname() {
		return replayname;
	}

	public void setReplayname(String replayname) {
		this.replayname = replayname;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public Long getQid() {
		return qid;
	}

	public void setQid(Long qid) {
		this.qid = qid;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		if (content.length() > 65535) {
			content = content.substring(0, 65535);
		}
		this.content = content;
	}

	public List<Comment> getReplys() {
		return replys;
	}

	public void setReplys(List<Comment> replys) {
		this.replys = replys;
	}

	public String getParentid() {
		return parentid;
	}

	public void setParentid(String parentid) {
		this.parentid = parentid;
	}

}
