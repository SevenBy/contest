package com.shzejing.spring.entities;

public class UserQuestion {

	private int[] questions;

	public int[] getQuestions() {
		return questions;
	}

	public void setQuestions(int[] questions) {
		this.questions = questions;
	}

}
