package com.shzejing.spring.entities;

public class QuestionScore {

	private int question_id;
	private int question_score;

	public int getQuestion_id() {
		return question_id;
	}

	public void setQuestion_id(int question_id) {
		this.question_id = question_id;
	}

	public int getQuestion_score() {
		return question_score;
	}

	public void setQuestion_score(int question_score) {
		this.question_score = question_score;
	}

}
