package com.shzejing.spring.entities;

public class Question {

	private String summary;
	private String[] categories;
	private String introduction;
	private String guide;
	private int timeout;
	private int flag_score;
	private String flag;
	private int difficult;
	private String id;
	
	private String answerfile;

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String[] getCategories() {
		return categories;
	}

	public void setCategories(String[] categories) {
		this.categories = categories;
	}

	public String getIntroduction() {
		return introduction;
	}

	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}

	public String getGuide() {
		return guide;
	}

	public void setGuide(String guide) {
		this.guide = guide;
	}

	public int getTimeout() {
		return timeout;
	}

	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}

	public int getFlag_score() {
		return flag_score;
	}

	public void setFlag_score(int flag_score) {
		this.flag_score = flag_score;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public int getDifficult() {
		return difficult;
	}

	public void setDifficult(int difficult) {
		this.difficult = difficult;
	}

	public String getAnswerfile() {
		return answerfile;
	}

	public void setAnswerfile(String answerfile) {
		this.answerfile = answerfile;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
