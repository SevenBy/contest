package com.shzejing.spring.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 * 用户操作日志记录，包括操作时间、用户ip地址、用户名、 行为（1 登录 、2 登出 、3 启动题目 、4 销毁题目、5 进入题目、6 启动单个虚拟机、 7
 * 删除单个虚拟机、 8 评分）、 题目ID、虚拟机名、用户评分、操作结果（0 成功、 1 错误）、失败详细信息
 *
 */
@Entity
@Table(name = "ws_log")
public class Log implements Serializable {

	private static final long serialVersionUID = 2L;

	@Id
	private String id;

	@Column(name = "timestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date logtime;

	@Column(name = "ip")
	private String ip;

	@Column(name = "username")
	private String username;

	@Column(name = "type")
	private int type;

	@Column(name = "question_id")
	private Long qid;

	@Column(name = "server_name")
	private String servername;

	@Column(name = "score")
	private int score;

	@Column(name = "state")
	private int state;

	@Column(name = "error", length = 65535)
	private String errorinfo;
	
	@Column(name = "extra")
	private String extra;

//	@Formula("(select a.qst_title from tc_question a where a.id = question_id)")
	@Transient
	private String qstName;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getLogtime() {
		return logtime;
	}

	public void setLogtime(Date logtime) {
		this.logtime = logtime;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public Long getQid() {
		return qid;
	}

	public void setQid(Long qid) {
		this.qid = qid;
	}

	public String getServername() {
		return servername;
	}

	public void setServername(String servername) {
		this.servername = servername;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public String getErrorinfo() {
		return errorinfo;
	}

	public void setErrorinfo(String errorinfo) {
		if (errorinfo.length() > 65535) {
			errorinfo = errorinfo.substring(0, 65535);
		}
		this.errorinfo = errorinfo;
	}
	
	public String getExtra() {
		return extra;
	}

	public void setExtra(String extra) {
		this.extra = extra;
	}

	public String getQstName() {
		return qstName;
	}

	public void setQstName(String qstName) {
		this.qstName = qstName;
	}

}
