package com.shzejing.spring.entities;

public class Contest {

	private int id;
	private String title;
	private QuestionScore[] questions;
	private String[] users;
	private long start;
	private long end;
	private String startstr;
	private String endstr;

	public String getTitle() {
		return title;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public QuestionScore[] getQuestions() {
		return questions;
	}

	public void setQuestions(QuestionScore[] questions) {
		this.questions = questions;
	}

	public String[] getUsers() {
		return users;
	}

	public void setUsers(String[] users) {
		this.users = users;
	}

	public long getStart() {
		return start;
	}

	public void setStart(long start) {
		this.start = start;
	}

	public long getEnd() {
		return end;
	}

	public void setEnd(long end) {
		this.end = end;
	}

	public String getStartstr() {
		return startstr;
	}

	public void setStartstr(String startstr) {
		this.startstr = startstr;
	}

	public String getEndstr() {
		return endstr;
	}

	public void setEndstr(String endstr) {
		this.endstr = endstr;
	}

}
