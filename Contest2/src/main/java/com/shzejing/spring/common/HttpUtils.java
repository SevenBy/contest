package com.shzejing.spring.common;

import java.io.IOException;

import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.springframework.core.env.Environment;

import com.shzejing.spring.entities.Account;

import net.sf.json.JSONObject;

public class HttpUtils {

	public static JSONObject post(String url, String body, Account account, Environment env, String token)
			throws ClientProtocolException, IOException {
		DefaultHttpClient httpclient = new DefaultHttpClient();
		String username = env.getProperty("vm.admin.name");
		String pass = env.getProperty("vm.admin.pass");
		UsernamePasswordCredentials credent;
		if (account.getUsername().equals(username)) {
			credent = new UsernamePasswordCredentials(username, pass);
		} else {
			credent = new UsernamePasswordCredentials(account.getUsername(), account.getUserpass());
		}
		HttpPost httpPost = new HttpPost(env.getProperty("vm.user.url") + url);
		httpclient.getCredentialsProvider().setCredentials(AuthScope.ANY, credent);
		if (StringUtils.isNotEmpty(token))
			httpPost.setHeader("X-Auth-Token", token);
		System.out.println("user:" + account.getUsername() + ", url:" + url);
		if (StringUtils.isNotEmpty(body)) {
			System.out.println("body:" + body);
			StringEntity entity = new StringEntity(body, "utf-8");// 解决中文乱码问题
			entity.setContentEncoding("UTF-8");
			entity.setContentType("application/json");
			httpPost.setEntity(entity);
		}

		HttpResponse res = httpclient.execute(httpPost);
		JSONObject o = JSONObject.fromObject(EntityUtils.toString(res.getEntity()));
		System.out.println("response:" + o.toString());
		return o;
	}

	public static JSONObject get(String url, Account account, Environment env, String token)
			throws ClientProtocolException, IOException {
		DefaultHttpClient httpclient = new DefaultHttpClient();
		String username = env.getProperty("vm.admin.name");
		String pass = env.getProperty("vm.admin.pass");
		HttpGet httpGet = new HttpGet(env.getProperty("vm.user.url") + url);
		System.out.println("user:" + account.getUsername() + ", url:" + url);
		UsernamePasswordCredentials credent;
		if (account.getUsername().equals(username)) {
			credent = new UsernamePasswordCredentials(username, pass);
		} else {
			credent = new UsernamePasswordCredentials(account.getUsername(), account.getUserpass());
		}
		httpclient.getCredentialsProvider().setCredentials(AuthScope.ANY, credent);
		if (StringUtils.isNotEmpty(token))
			httpGet.setHeader("X-Auth-Token", token);
		HttpResponse res = httpclient.execute(httpGet);
		int code = res.getStatusLine().getStatusCode();
		JSONObject o = null;
		if (code == 200) {
			o = JSONObject.fromObject(EntityUtils.toString((HttpEntity) res.getEntity()));
		} else {
			o = new JSONObject();
			if(code == 401) {
				o.put("exception", "错误的用户名密码");
			} else if(code >= 500){
				o.put("exception", "内部错误");
			} else {
				o = JSONObject.fromObject(EntityUtils.toString(res.getEntity()));
			}
		}
		httpGet.releaseConnection();
		return o;
	}

	public static JSONObject delete(String url, Account account, Environment env, String token)
			throws ClientProtocolException, IOException {
		DefaultHttpClient httpclient = new DefaultHttpClient();
		String username = env.getProperty("vm.admin.name");
		String pass = env.getProperty("vm.admin.pass");
		HttpDelete httpDelete = new HttpDelete(env.getProperty("vm.user.url") + url);
		UsernamePasswordCredentials credent;
		if (account.getUsername().equals(username)) {
			credent = new UsernamePasswordCredentials(username, pass);
		} else {
			credent = new UsernamePasswordCredentials(account.getUsername(), account.getUserpass());
		}
		System.out.println("user:" + account.getUsername() + ", url:" + url);
		httpclient.getCredentialsProvider().setCredentials(AuthScope.ANY, credent);
		if (StringUtils.isNotEmpty(token))
			httpDelete.setHeader("X-Auth-Token", token);
		HttpResponse res = httpclient.execute(httpDelete);
		int code = res.getStatusLine().getStatusCode();
		JSONObject o = new JSONObject();
		if (code >= 300) {
			if(code == 401) {
				o.put("exception", "错误的用户名密码");
			} else if(code >= 500){
				o.put("exception", "内部错误");
			} else {
				o = JSONObject.fromObject(EntityUtils.toString(res.getEntity()));
			}
		}
		httpDelete.releaseConnection();
		return o;
	}
	
	public static JSONObject put(String url, String body, Account account, Environment env, String token)
			throws ClientProtocolException, IOException {
		DefaultHttpClient httpclient = new DefaultHttpClient();
		String username = env.getProperty("vm.admin.name");
		String pass = env.getProperty("vm.admin.pass");
		UsernamePasswordCredentials credent;
		if (account.getUsername().equals(username)) {
			credent = new UsernamePasswordCredentials(username, pass);
		} else {
			credent = new UsernamePasswordCredentials(account.getUsername(), account.getUserpass());
		}
		System.out.println("user:" + account.getUsername() + ", url:" + url);
		HttpPut httpPut = new HttpPut(env.getProperty("vm.user.url") + url);
		httpclient.getCredentialsProvider().setCredentials(AuthScope.ANY, credent);
		if (StringUtils.isNotEmpty(body)) {
			StringEntity entity = new StringEntity(body, "utf-8");// 解决中文乱码问题
			entity.setContentEncoding("UTF-8");
			entity.setContentType("application/json");
			httpPut.setEntity(entity);
		}
		if (StringUtils.isNotEmpty(token))
			httpPut.setHeader("X-Auth-Token", token);

		HttpResponse res = httpclient.execute(httpPut);
		JSONObject o = JSONObject.fromObject(EntityUtils.toString(res.getEntity()));
		return o;
	}
}
