package com.shzejing.spring.common;

public enum UserTypeEnum {
	AdminUser(0, "超级管理员"),
	GroupUser(1, "组用户"),
	NormalUser(2, "普通用户");
	
	private Integer code;
	private String name;
	
	UserTypeEnum(Integer code, String name) {
		this.code = code;
		this.name = name;
	}
	
	public Integer getUserType() {
		return code;
	}

	public void setUserType(Integer code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
