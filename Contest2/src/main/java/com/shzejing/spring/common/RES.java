package com.shzejing.spring.common;

public class RES {

	private int status;

	private Object message;

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Object getMessage() {
		return message;
	}

	public void setMessage(Object message) {
		this.message = message;
	}

	public static RES OK() {
		RES r = new RES();
		r.setStatus(1);
		return r;
	}

	public static RES OK(Object message) {
		RES r = new RES();
		r.setStatus(1);
		r.setMessage(message);
		return r;
	}

	public static RES ERROR(Object message) {
		RES r = new RES();
		r.setStatus(0);
		r.setMessage(message);
		return r;
	}

	public static RES ERROR() {
		RES r = new RES();
		r.setStatus(0);
		return r;
	}
}
