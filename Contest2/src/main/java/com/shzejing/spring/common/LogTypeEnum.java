package com.shzejing.spring.common;

/**
 * 用户日志记录的行为
 *
 */
public enum LogTypeEnum {

	Type1(1, "登录"),
	Type2(2, "登出"),
	Type3(3, "启动题目"),
	Type4(4, "销毁题目"),
	Type5(5, "进入题目"),
	Type6(6, "启动单个虚拟机"),
	Type7(7, "删除单个虚拟机"),
	Type8(8, "发表评论"),
	Type9(9, "提交旗帜文件"),
	Type10(10, "登录比赛系统"),
	Type11(11, "登出比赛系统"),
	Type20(20, "创建题目"),
	Type21(21, "删除题目"),
	Type30(30, "创建用户"),
	Type31(31, "删除用户");
	
	private Integer code;
	private String name;
	
	LogTypeEnum(Integer code, String name) {
		this.code = code;
		this.name = name;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public static String enumName(Integer code) {
		for(LogTypeEnum item : values()) {
			if(item.getCode()==code) {
				return item.getName();
			}
 		}
		return "";
	}
}
